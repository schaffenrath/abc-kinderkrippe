package at.qe.sepm.skeleton.tests;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.model.Message;
import at.qe.sepm.skeleton.services.CaregiverService;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.DayPlanService;
import at.qe.sepm.skeleton.services.MessageService;
import at.qe.sepm.skeleton.ui.beans.MessageScheduleBean;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Set;

/**
 * Testing for {@link MessageScheduleBean}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class MessageScheduleBeanTest {

    @Autowired
    private MessageScheduleBean messageScheduleBean;
    
    @Autowired
    private MessageService messageService;
    
    @Autowired
    private ChildService childService;
    
    @Autowired
    private CaregiverService caregiverService;
    
    @Autowired
    private DayPlanService dayPlanService;
    
    @DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"ADMIN", "EMPLOYEE"})
    public void testDailyScheduledMessages() {
        Collection<Message> messages = messageService.loadAllMessages();
        messageScheduleBean.dailyScheduledMessages();
        
        int counter = 0;
        Calendar today = Calendar.getInstance();

        // Count how many children should have been deregistered
        for (Child c : childService.loadAllChildren()) {
            if (c != null && c.isEnabled() && c.getDeregistrationDate() != null && c.getDeregistrationDate().before(today)) {
                counter++;
            }
        }
        
        // Count how many caregiver are unconfirmed
        counter += caregiverService.loadAllUnconfirmedCaregivers().size();
        
        // Count how many children have birthday that are attending today
        DayPlan dayPlan = dayPlanService.loadDayPlan(today);
        today = removeTimeFromDate(today);

        if (dayPlan != null) {
            Set<Attendance> attendanceSet = dayPlan.getAttendances();
            for (Attendance a : attendanceSet) {
                Child child = a.getChild();
                if (child != null && child.getBirthdate().get(Calendar.MONTH) == today.get(Calendar.MONTH)
                        && child.getBirthdate().get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH)) {
                    counter++;
                }
            }
        }
                
        Assert.assertEquals(messageService.loadAllMessages().size(), messages.size() + counter);
    }
    
    private Calendar removeTimeFromDate(Calendar date) {
        Calendar dateWithoutTime = date;
        dateWithoutTime.set(Calendar.HOUR_OF_DAY, 0);
        dateWithoutTime.set(Calendar.MINUTE, 0);
        dateWithoutTime.set(Calendar.SECOND, 0);
        dateWithoutTime.set(Calendar.MILLISECOND, 0);
        return dateWithoutTime;
    }
}

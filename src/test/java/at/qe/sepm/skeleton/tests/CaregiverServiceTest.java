package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Caregiver;
import at.qe.sepm.skeleton.services.CaregiverService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Collection;

/**
 * Testing for {@link CaregiverService}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class CaregiverServiceTest {

    @Autowired
    CaregiverService caregiverService;

    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void testDatainitialization() {
        Assert.assertTrue("Insufficient amount of caregivers initialized for test data source", 1 <= caregiverService.loadAllCaregivers().size());

        for (Caregiver caregiver : caregiverService.loadAllCaregivers()) {
            if ("4".equals(caregiver.getId())) {
                Assert.assertNotNull("Caregiver \"caregiver1\" does not have a creationDate defined", caregiver.getCreationDate());
            }
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void testDeleteCaregiver() {
        Caregiver toBeDeletedCaregiver = caregiverService.loadCaregiver("4");
        int oldSize = caregiverService.loadAllCaregivers().size();

        Assert.assertNotNull("Caregiver could not be loaded from test data source", toBeDeletedCaregiver);

        caregiverService.deleteCaregiver(toBeDeletedCaregiver);

        Assert.assertTrue("No user has been deleted after calling CaregiverService.deleteCaregiver", (oldSize - 1) == caregiverService.loadAllCaregivers().size());
        Caregiver deletedCaregiver = caregiverService.loadCaregiver("4");
        Assert.assertNull("Deleted Caregiver could still be loaded from test data source via CaregiverService.getAllCaregivers", deletedCaregiver);

        for (Caregiver remainingCaregiver : caregiverService.loadAllCaregivers()) {
            Assert.assertNotEquals("Deleted Caregiver could still be loaded from test data source via CaregiverService.getAllCaregivers", toBeDeletedCaregiver.getId(), remainingCaregiver.getId());
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void testUpdateCaregiver() {
        Caregiver toBeSavedCaregiver = caregiverService.loadCaregiver("4");

        toBeSavedCaregiver.setEmail("changed-email@whatever.wherever");
        caregiverService.saveCaregiver(toBeSavedCaregiver);

        Caregiver freshlyLoadedCaregiver = caregiverService.loadCaregiver("4");
        Assert.assertNotNull("Caregiver could not be loaded from test data source after being saved", freshlyLoadedCaregiver);
        Assert.assertEquals("Caregiver \"caregiver\" does not have a the correct email attribute stored being saved", "changed-email@whatever.wherever", freshlyLoadedCaregiver.getEmail());
    }

    @DirtiesContext
    @Test
    @WithMockUser(authorities = {"EMPLOYEE"})
    public void testCreateCaregiver() {

    	Caregiver toBeCreatedCaregiver = new Caregiver();
        toBeCreatedCaregiver.setConfirmed(true);
        toBeCreatedCaregiver.setFirstName("New");
        toBeCreatedCaregiver.setLastName("Caregiver");
        toBeCreatedCaregiver.setEmail("new-email@whatever.wherever");
        toBeCreatedCaregiver.setPrivatePhone("+12 345 67890");
        toBeCreatedCaregiver.setBusinessPhone("+12 345 67892");
        caregiverService.saveCaregiver(toBeCreatedCaregiver);

        Caregiver freshlyCreatedCaregiver = caregiverService.loadCaregiver(toBeCreatedCaregiver.getId());
        Assert.assertNotNull("New caregiver could not be loaded from test data source after being saved", freshlyCreatedCaregiver);
        Assert.assertEquals("New caregiver does not have a the correct firstName attribute stored being saved", "New", freshlyCreatedCaregiver.getFirstName());
        Assert.assertEquals("New caregiver does not have a the correct lastName attribute stored being saved", "Caregiver", freshlyCreatedCaregiver.getLastName());
        Assert.assertEquals("New caregiver does not have a the correct email attribute stored being saved", "new-email@whatever.wherever", freshlyCreatedCaregiver.getEmail());
        Assert.assertEquals("New caregiver does not have a the correct phone attribute stored being saved", "+12 345 67890", freshlyCreatedCaregiver.getPrivatePhone());
        Assert.assertEquals("New caregiver does not have a the correct phone attribute stored being saved", "+12 345 67892", freshlyCreatedCaregiver.getBusinessPhone());
        Assert.assertNotNull("New caregiver does not have a createDate defined after being saved", freshlyCreatedCaregiver.getCreationDate());
    }

    @Test(expected = org.springframework.security.authentication.AuthenticationCredentialsNotFoundException.class)
    public void testUnauthenticateddLoadCaregivers() {
        for (@SuppressWarnings("unused") Caregiver caregiver : caregiverService.loadAllCaregivers()) {
            Assert.fail("Call to CaregiverService.getAllCaregivers should not work without proper authorization");
        }
    }

    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser(authorities = {"PARENT"})
    public void testUnauthorizedLoadCaregivers() {
        for (@SuppressWarnings("unused") Caregiver caregiver : caregiverService.loadAllCaregivers()) {
            Assert.fail("Call to CaregiverService.getAllCaregivers should not work without proper authorization");
        }
    }

    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser
    public void testUnauthorizedLoadCaregiver() {
        @SuppressWarnings("unused")
		Caregiver caregiver = caregiverService.loadCaregiver("4");
        Assert.fail("Call to CaregiverService.loadCaregiver should not work without proper authorization for other users than the authenticated ones");
    }

    @WithMockUser(authorities = {"EMPLOYEE"})
    public void testAuthorizedLoadCaregiver() {
        Caregiver caregiver = caregiverService.loadCaregiver("4");
        Assert.assertEquals("Call to CaregiverService.loadCaregiver returned wrong caregiver", "4", caregiver.getId());
    }

    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser
    public void testUnauthorizedSaveCaregiver() {
        Caregiver caregiver = caregiverService.loadCaregiver("4");
        Assert.assertEquals("Call to CaregiverService.loadCaregiver returned wrong caregiver", "4", caregiver.getId());
        caregiverService.saveCaregiver(caregiver);
    }

    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser(authorities = {"EMPLOYEE"})
    public void testUnauthorizedDeleteCaregiver() {
        Caregiver caregiver = caregiverService.loadCaregiver("4");
        Assert.assertEquals("Call to CaregiverService.loadCaregiver returned wrong caregiver", "4", caregiver.getId());
        caregiverService.deleteCaregiver(caregiver);
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadConfirmedCaregivers() {
        Collection<Caregiver> caregivers = caregiverService.loadAllConfirmedCaregivers();

        Assert.assertNotNull(caregiverService.loadCaregiver("Caregiver_Id_1"));

        Assert.assertNotNull(caregivers);
        Assert.assertTrue(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_1")));
        Assert.assertTrue(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_2")));
        Assert.assertTrue(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_3")));
        Assert.assertTrue(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_4")));
        Assert.assertTrue(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_5")));
        Assert.assertTrue(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_6")));
        Assert.assertTrue(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_7")));
        Assert.assertTrue(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_8")));
        Assert.assertFalse(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_9")));
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadUnconfirmedCaregivers() {
        Collection<Caregiver> caregivers = caregiverService.loadAllUnconfirmedCaregivers();

        Assert.assertNotNull(caregiverService.loadCaregiver("Caregiver_Id_1"));

        Assert.assertNotNull(caregivers);
        Assert.assertFalse(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_1")));
        Assert.assertFalse(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_2")));
        Assert.assertFalse(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_3")));
        Assert.assertFalse(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_4")));
        Assert.assertFalse(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_5")));
        Assert.assertFalse(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_6")));
        Assert.assertFalse(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_7")));
        Assert.assertFalse(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_8")));
        Assert.assertTrue(caregivers.contains(caregiverService.loadCaregiver("Caregiver_Id_9")));
    }
}

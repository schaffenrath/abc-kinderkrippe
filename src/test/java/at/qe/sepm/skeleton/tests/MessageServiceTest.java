package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Collection;

/**
 * Testing for {@link MessageService}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class MessageServiceTest {

    @Autowired
    private MessageService messageService;

    @Autowired
    private UserService userService;

    @Autowired
    private CaregiverService caregiverService;

    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"EMPLOYEE", "ADMIN"})
    public void testDatainitialization() {
        Assert.assertTrue("Insufficient amount of attendances initialized for test data source", 6 <= messageService.loadAllMessages().size());

        for (Message message : messageService.loadAllMessages()) {
            Assert.assertNotNull("message " + message.getId() + " does not have a creationDate defined", message.getCreationDate());
            Assert.assertNotNull("message " + message.getId() + " does not have a subject defined", message.getSubject());
            Assert.assertNotNull("message " + message.getId() + " does not have content defined", message.getContent());
            Assert.assertNotNull("message " + message.getId() + " does not have hideMessage defined", message.isEmployeeMessage());
            Assert.assertNotNull("message " + message.getId() + " does not have employeeMessage defined", message.isEmployeeMessage());
            Assert.assertNotNull("message " + message.getId() + " does not have publicMessage defined", message.isPublicMessage());

            if (null != message.getId()) switch (message.getId().toString()) {
                case "1":
                    Assert.assertTrue(message.getId() + " wrong subject", message.getSubject().equals("Employee Message Subject 1"));
                    Assert.assertTrue(message.getId() + " wrong content",message.getContent().equals("Employee Message Content 1"));
                    Assert.assertTrue(message.getId() + " wrong hideMessage",message.isHideMessage() == false);
                    Assert.assertTrue(message.getId() + " wrong employeeMessage", message.isEmployeeMessage() == true);
                    Assert.assertTrue(message.getId() + " wrong publicMessage", message.isPublicMessage() == false);
                    Assert.assertNull(message.getId() + " wrong sender", message.getSender());
                    Assert.assertNull(message.getId() + " wrong recipient", message.getRecipient());
                    break;
                case "2":
                    Assert.assertTrue(message.getId() + " wrong subject", message.getSubject().equals("Employee Message Subject 2"));
                    Assert.assertTrue(message.getId() + " wrong content",message.getContent().equals("Employee Message Content 2 - hidden"));
                    Assert.assertTrue(message.getId() + " wrong hideMessage",message.isHideMessage() == true);
                    Assert.assertTrue(message.getId() + " wrong employeeMessage", message.isEmployeeMessage() == true);
                    Assert.assertTrue(message.getId() + " wrong publicMessage", message.isPublicMessage() == false);
                    Assert.assertNull(message.getId() + " wrong sender", message.getSender());
                    Assert.assertNull(message.getId() + " wrong recipient", message.getRecipient());
                    break;
                case "3":
                    Assert.assertTrue(message.getId() + " wrong subject", message.getSubject().equals("Public Message Subject 1"));
                    Assert.assertTrue(message.getId() + " wrong content", message.getContent().equals("Public Message Content 1"));
                    Assert.assertTrue(message.getId() + " wrong hideMessage",message.isHideMessage() == false);
                    Assert.assertTrue(message.getId() + " wrong employeeMessage", message.isEmployeeMessage() == false);
                    Assert.assertTrue(message.getId() + " wrong publicMessage", message.isPublicMessage() == true);
                    Assert.assertNull(message.getId() + " wrong sender", message.getSender());
                    Assert.assertNull(message.getId() + " wrong recipient", message.getRecipient());
                    break;
                case "4":
                    Assert.assertTrue(message.getId() + " wrong subject", message.getSubject().equals("Public Message Subject 2"));
                    Assert.assertTrue(message.getId() + " wrong content",message.getContent().equals("Public Message Content 2 - hidden"));
                    Assert.assertTrue(message.getId() + " wrong hideMessage",message.isHideMessage() == true);
                    Assert.assertTrue(message.getId() + " wrong employeeMessage", message.isEmployeeMessage() == false);
                    Assert.assertTrue(message.getId() + " wrong publicMessage", message.isPublicMessage() == true);
                    Assert.assertNull(message.getId() + " wrong sender", message.getSender());
                    Assert.assertNull(message.getId() + " wrong recipient", message.getRecipient());
                    break;
                case "5":
                    Assert.assertTrue(message.getId() + " wrong subject", message.getSubject().equals("Personal Message Subject 1"));
                    Assert.assertTrue(message.getId() + " wrong content",message.getContent().equals("Personal Message Content 1 sender:Parent_Id_1; recipient:Parent_Id_2"));
                    Assert.assertTrue(message.getId() + " wrong hideMessage",message.isHideMessage() == false);
                    Assert.assertTrue(message.getId() + " wrong employeeMessage", message.isEmployeeMessage() == false);
                    Assert.assertTrue(message.getId() + " wrong publicMessage", message.isPublicMessage() == true);
                    Assert.assertTrue(message.getId() + " wrong sender", message.getSender().equals(userService.loadUser("Parent_Id_1")));
                    Assert.assertTrue(message.getId() + " wrong recipient", message.getRecipient().equals(userService.loadUser("Parent_Id_2")));
                    break;
                case "6":
                    Assert.assertTrue(message.getId() + " wrong subject", message.getSubject().equals("Personal Message Subject 2"));
                    Assert.assertTrue(message.getId() + " wrong content", message.getContent().equals("Personal Message Content 2 sender:Parent_Id_3; recipient:Parent_Id_4"));
                    Assert.assertTrue(message.getId() + " wrong hideMessage",message.isHideMessage() == false);
                    Assert.assertTrue(message.getId() + " wrong employeeMessage",message.isEmployeeMessage() == false);
                    Assert.assertTrue(message.getId() + " wrong publicMessage", message.isPublicMessage() == true);
                    Assert.assertTrue(message.getId() + " wrong sender", message.getSender().equals(userService.loadUser("Parent_Id_3")));
                    Assert.assertTrue(message.getId() + " wrong recipient", message.getRecipient().equals(userService.loadUser("Parent_Id_4")));
                    break;
                default:
                    break;
            }
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testCreateEmployeeMessage() {
        Message message = messageService.createEmployeeMessage("Test Subject", "Test Content");
        Message newMessage = messageService.loadMessage(message.getId());
        Assert.assertNotNull(newMessage + " wasn't saved", newMessage);
        Assert.assertTrue(newMessage + " wrong subject", newMessage.getSubject().equals("Test Subject"));
        Assert.assertTrue(newMessage + " wrong content",newMessage.getContent().equals("Test Content"));
        Assert.assertTrue(newMessage + " wrong hideMessage",newMessage.isHideMessage() == false);
        Assert.assertTrue(newMessage + " wrong employeeMessage", newMessage.isEmployeeMessage() == true);
        Assert.assertTrue(newMessage + " wrong publicMessage", newMessage.isPublicMessage() == false);
        Assert.assertNull(newMessage + " wrong sender", newMessage.getSender());
        Assert.assertNull(newMessage + " wrong recipient", newMessage.getRecipient());
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testCreateCaregiverConfirmationMessage() {
        Message message = messageService.createCaregiverConfirmationMessage("Test Subject", "Test Content", caregiverService.loadCaregiver("Caregiver_Id_9").getId());
        Message newMessage = messageService.loadMessage(message.getId());
        Assert.assertNotNull(newMessage + " wasn't saved", newMessage);
        Assert.assertTrue(newMessage + " wrong subject", newMessage.getSubject().equals("Test Subject"));
        Assert.assertTrue(newMessage + " wrong content",newMessage.getContent().equals("Test Content"));
        Assert.assertTrue(newMessage + " wrong hideMessage",newMessage.isHideMessage() == false);
        Assert.assertTrue(newMessage + " wrong employeeMessage", newMessage.isEmployeeMessage() == true);
        Assert.assertTrue(newMessage + " wrong publicMessage", newMessage.isPublicMessage() == false);
        Assert.assertNull(newMessage + " wrong sender", newMessage.getSender());
        Assert.assertNull(newMessage + " wrong recipient", newMessage.getRecipient());
        Assert.assertTrue(newMessage + " wrong confirmCaregiverMessage", newMessage.isConfirmCaregiverMessage());
        Assert.assertEquals(newMessage + " wrong additionalId", newMessage.getAdditionalId(), caregiverService.loadCaregiver("Caregiver_Id_9").getId());
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testCreatePublicMessage() {
        Message message = messageService.createPublicMessage("Test Subject", "Test Content");
        Message newMessage = messageService.loadMessage(message.getId());
        Assert.assertNotNull(newMessage + " wasn't saved", newMessage);
        Assert.assertTrue(newMessage + " wrong subject", newMessage.getSubject().equals("Test Subject"));
        Assert.assertTrue(newMessage + " wrong content",newMessage.getContent().equals("Test Content"));
        Assert.assertTrue(newMessage + " wrong hideMessage",newMessage.isHideMessage() == false);
        Assert.assertTrue(newMessage + " wrong employeeMessage", newMessage.isEmployeeMessage() == false);
        Assert.assertTrue(newMessage + " wrong publicMessage", newMessage.isPublicMessage() == true);
        Assert.assertNull(newMessage + " wrong sender", newMessage.getSender());
        Assert.assertNull(newMessage + " wrong recipient", newMessage.getRecipient());
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
    public void testCreateUserMessage() {
        Message message = messageService.createUserMessage("Test Subject", "Test Content", userService.loadUser("Parent_Id_8"));
        Message newMessage = messageService.loadMessage(message.getId());
        Assert.assertNotNull(newMessage + " wasn't saved", newMessage);
        Assert.assertTrue(newMessage + " wrong subject", newMessage.getSubject().equals("Test Subject"));
        Assert.assertTrue(newMessage + " wrong content",newMessage.getContent().equals("Test Content"));
        Assert.assertTrue(newMessage + " wrong hideMessage",newMessage.isHideMessage() == false);
        Assert.assertTrue(newMessage + " wrong employeeMessage", newMessage.isEmployeeMessage() == false);
        Assert.assertTrue(newMessage + " wrong publicMessage", newMessage.isPublicMessage() == false);
        Assert.assertTrue(newMessage + " wrong sender", newMessage.getSender().equals(userService.loadUser("Parent_Id_1")));
        Assert.assertTrue(newMessage + " wrong recipient", newMessage.getRecipient().equals(userService.loadUser("Parent_Id_8")));
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadAllMessagesSubjectContains() {
        Collection<Message> messages = messageService.loadAllMessagesSubjectContains("1");

        Assert.assertNotNull("loadAllMessagesSubjectContains returned null", messages);
        Assert.assertTrue("loadAllMessagesSubjectContains didn't return all messages", messages.contains(messageService.loadMessage(new Long(1))));
        Assert.assertTrue("loadAllMessagesSubjectContains didn't return all messages", messages.contains(messageService.loadMessage(new Long(3))));
        Assert.assertTrue("loadAllMessagesSubjectContains didn't return all messages", messages.contains(messageService.loadMessage(new Long(5))));
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadAllEmployeeMessages() {
        Collection<Message> messages = messageService.loadAllEmployeeMessages();

        Assert.assertNotNull("loadAllEmployeeMessages returned null", messages);
        for (Message m : messages) {
            Assert.assertTrue("loadAllEmployeeMessages returned a non employee message", m.isEmployeeMessage());
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadAllVisibleEmployeeMessages() {
        Collection<Message> messages = messageService.loadAllVisibleEmployeeMessages();

        Assert.assertNotNull("loadAllVisibleEmployeeMessages returned null", messages);
        for (Message m : messages) {
            Assert.assertTrue("loadAllVisibleEmployeeMessages returned a non visible message", m.isHideMessage() == false);
            Assert.assertTrue("loadAllVisibleEmployeeMessages returned a non employee message", m.isEmployeeMessage());
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadAllPublicMessages() {
        Collection<Message> messages = messageService.loadAllPublicMessages();

        Assert.assertNotNull("loadAllPublicMessages returned null", messages);
        for (Message m : messages) {
            Assert.assertTrue("loadAllPublicMessages returned a non public message", m.isPublicMessage());
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadAllVisiblePublicMessages() {
        Collection<Message> messages = messageService.loadAllVisiblePublicMessages();

        Assert.assertNotNull("loadAllVisiblePublicMessages returned null", messages);
        for (Message m : messages) {
            Assert.assertTrue("loadAllVisiblePublicMessages returned a non visible message", m.isHideMessage() == false);
            Assert.assertTrue("loadAllVisiblePublicMessages returned a non public message", m.isPublicMessage());
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"ADMIN", "EMPLOYEE"})
    public void testDeleteMessage() {
        Message message = messageService.loadMessage(new Long(1));
        messageService.deleteMessage(message);

        Assert.assertNull("Message could still be loaded after deleting it", messageService.loadMessage(message.getId()));
        Assert.assertFalse("Message could still be found in loadAllMessages after deleting it", messageService.loadAllMessages().contains(message));
    }
}

package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.services.TaskService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.controllers.TaskListController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Calendar;

/**
 * Testing for {@link TaskListController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class TaskListControllerTest {

	@Autowired
	UserService userService;

	@Autowired
	TaskService taskService;

	@Autowired
	TaskListController taskListController;

	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testTasksOfParent() {
		Assert.assertEquals("Could not load tasks of parent", 3, taskListController.getTasksOfParent("Parent_Id_1").size());
		Assert.assertNotNull("Could not load active tasks of parent", taskListController.getActiveTasksOfParent("Parent_Id_1"));

		Calendar deadline = Calendar.getInstance();
		deadline.set(2018, Calendar.JULY, 10);
		Assert.assertEquals("Could not load tasks of parent and deadline",
				0, taskListController.getTasksOfParentAndDeadline("Parent_Id_1", deadline).size());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testUncompletedTasks() {
		int numberUncompletedTasks = taskListController.getUncompletedTasks().size();
		taskListController.setUncompletedTasks(taskService.loadUncompletedTasks());
		Assert.assertEquals("Could not load all uncompleted tasks",
				numberUncompletedTasks,
				taskListController.getUncompletedTasks().size());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testFilteredTasks() {
		taskListController.setFilteredTasks(taskService.loadActiveTasks());
		Assert.assertEquals("Could not filter active tasks tasks",
				taskService.loadActiveTasks().size(),
				taskListController.getFilteredTasks().size());
	}

}

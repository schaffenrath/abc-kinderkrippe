package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;

import at.qe.sepm.skeleton.ui.controllers.UserDetailController;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link UserDetailController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class UserDetailControllerTest {
    
    @Autowired
    private UserDetailController userDetailController;
    
    @Autowired
    private ChildService childService;
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
    public void testCreateParent() {
	    userDetailController.createUser();
	    User newUser = userDetailController.getUser();
	    newUser.setEmail("test@test.tt");
	    newUser.setFirstName("Test_FN");
	    newUser.setLastName("Test_LN");
	    newUser.setPrivatePhone("testnr");
	    userDetailController.setUser(newUser);
	    userDetailController.setNewPassword("testpw");
	    userDetailController.setUserRegistrated(true);
	    try {
	        userDetailController.doSaveParent();
	    } catch (NullPointerException e) {}
	    userDetailController.setChildById("Child_Id_7");
	    try {
	        userDetailController.addChildToParent();
	    } catch (NullPointerException e) {}
	    String id = userDetailController.getUser().getId();
	    
	    userDetailController.setUser(null);
	    userDetailController.setUserById(id);
	    Assert.assertEquals(userDetailController.getFirstName(), "Test_FN");
	    User createdUser = userDetailController.getUserById();
	    Assert.assertEquals(createdUser.getEmail(), "test@test.tt");
	    Assert.assertEquals(createdUser.getChildren().size(), 1);
        Assert.assertFalse(createdUser.getRoles().contains(UserRole.ADMIN));
        Assert.assertFalse(createdUser.getRoles().contains(UserRole.EMPLOYEE));
        Assert.assertTrue(createdUser.getRoles().contains(UserRole.PARENT));
	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
    public void testUpdateParent() {
	    userDetailController.setUserById("Parent_Id_1");
	    userDetailController.setChild(childService.loadChild("Child_Id_7"));
	    try {
	        userDetailController.addChildToParent();
	    } catch (NullPointerException e) {}
	    
	    userDetailController.setUserById("Parent_Id_1");
	    userDetailController.setFirstName("OtherName");
	    try {
	        userDetailController.doSaveUser();
	    } catch (NullPointerException e) {}
	    
	    userDetailController.setUser(null);
	    userDetailController.setUserById("Parent_Id_1");
	    User user = userDetailController.getUser();
	    Assert.assertTrue(user.getChildren().contains(userDetailController.getChild()));
	    Assert.assertEquals(user.getFirstName(), "OtherName");
	}
	
	@DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
    public void testCreateEmployee() {
        userDetailController.createUser();
        User newUser = userDetailController.getUser();
        newUser.setEmail("test@test.tt");
        newUser.setFirstName("Test_FN");
        newUser.setLastName("Test_LN");
        newUser.setPrivatePhone("testnr");
        userDetailController.setUser(newUser);
        userDetailController.setUserRegistrated(true);
        try{
            userDetailController.doSaveEmployee();
        } catch (NullPointerException e) {}
        
        String id = userDetailController.getUser().getId();
        userDetailController.setUser(null);
        userDetailController.setUserById(id);
        Assert.assertEquals(userDetailController.getFirstName(), "Test_FN");
        User createdUser = userDetailController.getUserById();
        Assert.assertEquals(createdUser.getEmail(), "test@test.tt");
        Assert.assertFalse(createdUser.getRoles().contains(UserRole.ADMIN));
        Assert.assertTrue(createdUser.getRoles().contains(UserRole.EMPLOYEE));
        Assert.assertFalse(createdUser.getRoles().contains(UserRole.PARENT));
    }
	
	@DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testSendBill() {
	    userDetailController.setUserById("Parent_Id_1");
	    userDetailController.sendBill("test");
	}
}

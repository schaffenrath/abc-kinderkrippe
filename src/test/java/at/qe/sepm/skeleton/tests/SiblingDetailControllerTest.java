package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Sibling;
import at.qe.sepm.skeleton.services.SiblingService;
import at.qe.sepm.skeleton.ui.controllers.SiblingDetailController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link SiblingDetailController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class SiblingDetailControllerTest {

	@Autowired
	SiblingService siblingService;

	@Autowired
	SiblingDetailController siblingDetailController;


	@Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"EMPLOYEE", "ADMIN"})
    public void testSibling() {
		Sibling testSibling = siblingService.loadSibling("Sibling_Id_1");
		siblingDetailController.set(testSibling);
		Assert.assertNotNull("Could not load sibling", siblingDetailController.get());
		Assert.assertEquals("Could not save right sibling",
				siblingService.loadSibling("Sibling_Id_1"),
				siblingDetailController.get());
	}

	@Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"EMPLOYEE", "ADMIN"})
    public void testSiblingById() {
		siblingDetailController.setById("Sibling_Id_1");
		Assert.assertNotNull("Could not load sibling", siblingDetailController.get());
		Assert.assertEquals("Could not save right sibling",
				siblingService.loadSibling("Sibling_Id_1"),
				siblingDetailController.getById("Sibling_Id_1"));
	}
	

	@DirtiesContext
	@Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"EMPLOYEE", "ADMIN"})
    public void testSaveSibling() {
		Sibling testSibling = siblingService.loadSibling("Sibling_Id_1");
		siblingDetailController.set(testSibling);
		Assert.assertNotNull("Could not load sibling", siblingDetailController.get());
		siblingDetailController.save();
		Assert.assertEquals("Could not save sibling", siblingDetailController.get(),  testSibling);
	}

	@DirtiesContext
	@Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"EMPLOYEE", "ADMIN"})
    public void testDeleteSibling() {
		Sibling testSibling = siblingService.loadSibling("Sibling_Id_1");
		siblingDetailController.set(testSibling);
		Assert.assertNotNull("Could not load sibling", siblingDetailController.get());
		siblingDetailController.delete();
		Assert.assertNull("Could not delete sibling", siblingService.loadSibling("Sibling_Id_1"));
	}


}

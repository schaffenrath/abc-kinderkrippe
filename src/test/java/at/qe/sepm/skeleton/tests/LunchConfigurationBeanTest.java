package at.qe.sepm.skeleton.tests;

import java.util.Calendar;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.LunchConfigurationBean;

/**
 * Testing for {@link LunchConfigurationBean}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class LunchConfigurationBeanTest {
    
    @Autowired
    private LunchConfigurationBean lunchConfigurationBean;
    
    @Autowired
    private UserService userService;
    
    @DirtiesContext
    @Test
    public void testGetDayOfWeek() {
        Calendar now = Calendar.getInstance();
        lunchConfigurationBean.setToday();
        Assert.assertEquals(lunchConfigurationBean.getDayOfWeekAsString((now.get(Calendar.DAY_OF_WEEK) + 5) % 7 + 1), lunchConfigurationBean.convertToString(now));
    }
    
    @DirtiesContext
    @Test
    public void testSwitchWeek() {
        lunchConfigurationBean.setToday();
        Calendar nextWeek = Calendar.getInstance();
        Calendar prevWeek = Calendar.getInstance();
        
        nextWeek.add(Calendar.WEEK_OF_YEAR, 1);
        lunchConfigurationBean.setNextWeek();
        Assert.assertEquals(lunchConfigurationBean.getDayOfWeekAsString((nextWeek.get(Calendar.DAY_OF_WEEK) + 5) % 7 + 1), lunchConfigurationBean.convertToString(nextWeek));
    
        lunchConfigurationBean.setPrevWeek();
        Assert.assertEquals(lunchConfigurationBean.getDayOfWeekAsString((prevWeek.get(Calendar.DAY_OF_WEEK) + 5) % 7 + 1), lunchConfigurationBean.convertToString(prevWeek));
    }
    
    @DirtiesContext
    @Test
    public void testSavedDay() {
        Calendar now = Calendar.getInstance();
        lunchConfigurationBean.setToday();
        lunchConfigurationBean.setSavedDay((now.get(Calendar.DAY_OF_WEEK) + 5) % 7 + 1);
        
        Assert.assertEquals(lunchConfigurationBean.convertToString(lunchConfigurationBean.getSavedDay()), lunchConfigurationBean.convertToString(now));
        Assert.assertEquals(lunchConfigurationBean.getSavedDayAsString(), lunchConfigurationBean.convertToString(now));
    }
    
    @DirtiesContext
    @Test
    @WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
    public void testGetPossibleChildren() {
        Set<Child> children = userService.loadUser("Parent_Id_1").getChildren();
        Assert.assertEquals(lunchConfigurationBean.getPossibleChildren().size(), children.size());
    }
}

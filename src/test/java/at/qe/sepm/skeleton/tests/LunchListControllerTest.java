package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.services.DayPlanService;
import at.qe.sepm.skeleton.services.LunchService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.controllers.LunchListController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Calendar;

/**
 * Testing for {@link LunchListController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class LunchListControllerTest {

	@Autowired
	LunchService lunchService;

	@Autowired
	DayPlanService dayPlanService;

	@Autowired
	UserService userService;

	@Autowired
	LunchListController lunchListController;


	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testLunchesByParentInTimespan() {
		Calendar testFrom = Calendar.getInstance();
		testFrom.set(2017, Calendar.OCTOBER, 2);

		Calendar testTo = Calendar.getInstance();
		testTo.set(2017, Calendar.OCTOBER, 7);

		Assert.assertEquals("Not correct number of lunches", 2,
				lunchListController.getLunchesByParentInTimespan(userService.loadUser("Parent_Id_1"), testFrom, testTo).size());
	}

}

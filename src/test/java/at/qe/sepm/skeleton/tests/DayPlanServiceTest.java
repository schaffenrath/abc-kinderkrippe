package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.configs.DayPlanServiceConfig;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.model.Gender;
import at.qe.sepm.skeleton.model.MonthPlan;
import at.qe.sepm.skeleton.services.*;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link DayPlanService}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class DayPlanServiceTest {

	@Autowired
	DayPlanService dayPlanService;

	@Autowired
	AttendanceService attendanceService;

	@Autowired
	LunchService lunchService;

	@Autowired
	DayPlanServiceConfig dayPlanServiceConfig;

	@Autowired
	ChildService childService;

	@Autowired
	CaregiverService caregiverService;

	@DirtiesContext
	@Test
	public void testAutoinitDayPlans() {
		dayPlanServiceConfig.autoinitDayPlans();
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DATE, 7 * 52 + 4);
		DayPlan dayPlan = dayPlanService.loadDayPlan(date);
		Calendar time = Calendar.getInstance();
		time.setTimeInMillis(Long.parseLong(dayPlanServiceConfig.getDPconfig("friday.earliestArrival")));
		time = dayPlanServiceConfig.adjustTimeOfCalendar(date, time);
		Assert.assertEquals(dayPlan.getEarliestArrivalDate().getTime(), time.getTime());
		time.setTimeInMillis(Long.parseLong(dayPlanServiceConfig.getDPconfig("friday.latestArrival")));
		time = dayPlanServiceConfig.adjustTimeOfCalendar(date, time);
		Assert.assertEquals(dayPlan.getLatestArrivalDate().getTime(), time.getTime());
		time.setTimeInMillis(Long.parseLong(dayPlanServiceConfig.getDPconfig("friday.earliestDeparture")));
		time = dayPlanServiceConfig.adjustTimeOfCalendar(date, time);
		Assert.assertEquals(dayPlan.getEarliestDepartureDate().getTime(), time.getTime());
		time.setTimeInMillis(Long.parseLong(dayPlanServiceConfig.getDPconfig("friday.latestDeparture")));
		time = dayPlanServiceConfig.adjustTimeOfCalendar(date, time);
		Assert.assertEquals(dayPlan.getLatestDepartureDate().getTime(), time.getTime());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testAutoinitAttendances() {
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DATE, 5 * 7);
		dayPlanService.createDayPlan(date);
		boolean[] attendanceDays = { true, true, true, true, true, true, true };
		for (Child child : childService.loadAllChildren()) {
			child.setAttendanceDays(attendanceDays);
			childService.saveChild(child);
		}
		dayPlanServiceConfig.autoinitAttendances();
		DayPlan dayPlan = dayPlanService.loadDayPlan(date);
		Assert.assertEquals(5, dayPlan.getAttendances().size());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
    public void testInitAttendancesForChild() {
		Child child = childService.createChild("firstName", "lastName", Gender.OTHER, Calendar.getInstance(),
				Calendar.getInstance());
		boolean array[] = new boolean[7];
		Arrays.fill(array, true);
		child.setAttendanceDays(array);
		child = childService.saveChild(child);
		Calendar endDate = Calendar.getInstance();
		endDate.add(Calendar.DATE, 1 + 3 * 7);
		endDate.set(Calendar.HOUR_OF_DAY, 0);
		endDate.set(Calendar.MINUTE, 0);
		endDate.set(Calendar.SECOND, 0);
		endDate.set(Calendar.MILLISECOND, 0);
		while (endDate.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
			endDate.add(Calendar.DATE, 1);
		}
		Calendar dummyDate = Calendar.getInstance();
		dummyDate.add(Calendar.DATE, 5);
		dayPlanService.createDayPlan(dummyDate);
		dummyDate.add(Calendar.DATE, 7);
		dayPlanService.createDayPlan(dummyDate);
		dummyDate.add(Calendar.DATE, 4);
		dayPlanService.createDayPlan(dummyDate);
		
		dayPlanServiceConfig.initAttendancesForChild(child);
		
		Child child2 = childService.loadChildWhileForcingAttendances(child.getId());
		List<DayPlan> dayPlans = dayPlanService.loadDayPlansInTimeSpan(child.getRegistrationDate(), endDate);
		System.out.println(dayPlans.size());
		Assert.assertEquals(dayPlans.size(), child2.getAttendances().size());

	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testManualInit() {
		int initCount = dayPlanService.loadAllDayPlans().size();
		dayPlanServiceConfig.manualInit();
		Assert.assertTrue(initCount < dayPlanService.loadAllDayPlans().size());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testDatainitialization() {
		Calendar date1 = Calendar.getInstance();
		date1.set(Calendar.YEAR, 2017);
		date1.set(Calendar.MONTH, Calendar.OCTOBER);
		date1.set(Calendar.DAY_OF_MONTH, 1);
		date1 = removeTimeFromDate(date1);

		Calendar date2 = Calendar.getInstance();
		date2.set(Calendar.YEAR, 2017);
		date2.set(Calendar.MONTH, Calendar.OCTOBER);
		date2.set(Calendar.DAY_OF_MONTH, 2);
		date2 = removeTimeFromDate(date2);

		Calendar date3 = Calendar.getInstance();
		date3.set(Calendar.YEAR, 2017);
		date3.set(Calendar.MONTH, Calendar.OCTOBER);
		date3.set(Calendar.DAY_OF_MONTH, 3);
		date3 = removeTimeFromDate(date3);

		Calendar date4 = Calendar.getInstance();
		date4.set(Calendar.YEAR, 2017);
		date4.set(Calendar.MONTH, Calendar.OCTOBER);
		date4.set(Calendar.DAY_OF_MONTH, 4);
		date4 = removeTimeFromDate(date4);

		Assert.assertTrue("Insufficient amount of dayplans initialized for test data source",
				4 <= dayPlanService.loadAllDayPlans().size());

		for (DayPlan dayPlan : dayPlanService.loadAllDayPlans()) {
			if (dayPlan.equals(dayPlanService.loadDayPlan(date1))) {
				Assert.assertTrue(dayPlan.getMaxAttendances() == 20);
				Assert.assertTrue(dayPlan.getAttendances().isEmpty());
				Assert.assertTrue(dayPlan.getLunches().isEmpty());
			}

			else if (dayPlan.equals(dayPlanService.loadDayPlan(date2))) {
				Assert.assertTrue(dayPlan.getMaxAttendances() == 20);
				Assert.assertTrue(
						dayPlan.getAttendances().contains(attendanceService.loadAttendance("Attendance_Id_1")));
				Assert.assertTrue(
						dayPlan.getAttendances().contains(attendanceService.loadAttendance("Attendance_Id_2")));
				Assert.assertTrue(dayPlan.getLunches().contains(lunchService.loadLunch("Lunch_Id_1")));
				Assert.assertTrue(dayPlan.getLunches().contains(lunchService.loadLunch("Lunch_Id_2")));
			}

			else if (dayPlan.equals(dayPlanService.loadDayPlan(date3))) {
				Assert.assertTrue(dayPlan.getMaxAttendances() == 20);
				Assert.assertTrue(
						dayPlan.getAttendances().contains(attendanceService.loadAttendance("Attendance_Id_3")));
				Assert.assertTrue(dayPlan.getLunches().contains(lunchService.loadLunch("Lunch_Id_3")));
				Assert.assertTrue(dayPlan.getLunches().contains(lunchService.loadLunch("Lunch_Id_4")));
			}

			else if (dayPlan.equals(dayPlanService.loadDayPlan(date4))) {
				Assert.assertTrue(dayPlan.getMaxAttendances() == 20);
				Assert.assertTrue(
						dayPlan.getAttendances().contains(attendanceService.loadAttendance("Attendance_Id_4")));
				Assert.assertTrue(
						dayPlan.getAttendances().contains(attendanceService.loadAttendance("Attendance_Id_5")));
				Assert.assertTrue(
						dayPlan.getAttendances().contains(attendanceService.loadAttendance("Attendance_Id_6")));
				Assert.assertTrue(dayPlan.getLunches().contains(lunchService.loadLunch("Lunch_Id_5")));
				Assert.assertTrue(dayPlan.getLunches().contains(lunchService.loadLunch("Lunch_Id_6")));
			}
		}
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testCreateDayPlan() {
		Calendar date1 = Calendar.getInstance();
		date1.set(Calendar.YEAR, 2017);
		date1.set(Calendar.MONTH, Calendar.OCTOBER);
		date1.set(Calendar.DAY_OF_MONTH, 9);
		date1 = removeTimeFromDate(date1);

		DayPlan freshlyCreatedDayPlan = dayPlanService.createDayPlan(date1, date1, date1, date1, date1);

		dayPlanService.addAttendance(freshlyCreatedDayPlan, childService.loadChild("Child_Id_1"), date1, date1);
		dayPlanService.addAttendance(freshlyCreatedDayPlan, childService.loadChild("Child_Id_2"), date1, date1,
				caregiverService.loadCaregiver("Caregiver_Id_1"));
		dayPlanService.addLunch(freshlyCreatedDayPlan, "test menu", (float) 0.42);

		DayPlan reloadedCreatedDayPlan = dayPlanService.loadDayPlan(freshlyCreatedDayPlan.getId());
		Assert.assertNotNull("New dayplan could not be loaded from test data source after being saved",
				reloadedCreatedDayPlan);
		Assert.assertEquals(reloadedCreatedDayPlan.getDayDate(), date1);
		Assert.assertEquals(reloadedCreatedDayPlan.getEarliestArrivalDate(), date1);
		Assert.assertEquals(reloadedCreatedDayPlan.getLatestArrivalDate(), date1);
		Assert.assertEquals(reloadedCreatedDayPlan.getEarliestDepartureDate(), date1);
		Assert.assertEquals(reloadedCreatedDayPlan.getLatestDepartureDate(), date1);
		Assert.assertTrue(reloadedCreatedDayPlan.getAttendances().size() == 2);
		Assert.assertTrue(reloadedCreatedDayPlan.getLunches().size() == 1);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testloadDayPlansInTimeSpan() {
		Calendar date1 = Calendar.getInstance();
		date1.set(Calendar.YEAR, 2017);
		date1.set(Calendar.MONTH, Calendar.OCTOBER);
		date1.set(Calendar.DAY_OF_MONTH, 1);
		date1 = removeTimeFromDate(date1);

		Calendar date2 = Calendar.getInstance();
		date2.set(Calendar.YEAR, 2017);
		date2.set(Calendar.MONTH, Calendar.OCTOBER);
		date2.set(Calendar.DAY_OF_MONTH, 3);
		date2 = removeTimeFromDate(date2);

		Calendar date3 = Calendar.getInstance();
		date3.set(Calendar.YEAR, 2017);
		date3.set(Calendar.MONTH, Calendar.SEPTEMBER);
		date3.set(Calendar.DAY_OF_MONTH, 1);
		date3 = removeTimeFromDate(date3);

		for (DayPlan d : dayPlanService.loadDayPlansInTimeSpan(date1, date2)) {
			Assert.assertTrue(d.getId().after(date3));
			Assert.assertTrue(d.getId().before(date2));
		}
	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
    public void testLoadMonthPlansByYear() {
	    Calendar year = Calendar.getInstance();
	    year.set(Calendar.YEAR, 2017);
	    
	    List<MonthPlan> months = dayPlanService.loadMonthPlansByYear(year);
	    
	    for(MonthPlan month : months) {
	        if(month.getMonthId() == 9) {
	            Assert.assertEquals(month.getOvrOccupancy(), 6);
	            Assert.assertEquals(month.getAvrOccupancy(), 1);
	            Assert.assertEquals(month.getOpen(), 4);
	        } else {
	            Assert.assertEquals(month.getOvrOccupancy(), 0);
                Assert.assertEquals(month.getAvrOccupancy(), 0);
                Assert.assertEquals(month.getOpen(), 0);
	        }
	    }
	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
    public void testLoadCurrentLunchDeadline() {
	    Calendar date = Calendar.getInstance();
        Calendar latestDate = Calendar.getInstance();
        latestDate.setTimeInMillis(date.getTimeInMillis());
	    Calendar deadline = dayPlanService.loadCurrentLunchDeadline(date);
	    latestDate.add(Calendar.DATE, 7);
	    
	    Assert.assertNotNull(deadline);
	    Assert.assertNotNull(latestDate);
	    Assert.assertTrue("The loaded deadline is in the wrong timespan", latestDate.after(deadline));
	}

	private Calendar removeTimeFromDate(Calendar date) {
		Calendar dateWithoutTime = date;
		dateWithoutTime.set(Calendar.HOUR_OF_DAY, 0);
		dateWithoutTime.set(Calendar.MINUTE, 0);
		dateWithoutTime.set(Calendar.SECOND, 0);
		dateWithoutTime.set(Calendar.MILLISECOND, 0);
		return dateWithoutTime;
	}
}

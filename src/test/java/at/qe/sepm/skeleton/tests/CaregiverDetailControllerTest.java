package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Caregiver;
import at.qe.sepm.skeleton.services.CaregiverService;
import at.qe.sepm.skeleton.ui.controllers.CaregiverDetailController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link CaregiverDetailController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class CaregiverDetailControllerTest {

	@Autowired
	CaregiverService caregiverService;

	@Autowired
	CaregiverDetailController caregiverDetailController;


	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testCaregiver() {
		Caregiver testCaregiver = caregiverService.loadCaregiver("Caregiver_Id_1");
		caregiverDetailController.setCaregiver(testCaregiver);
		Assert.assertNotNull("Could not load caregiver", caregiverDetailController.getCaregiver());
		Assert.assertEquals("Not correct numbers of children", caregiverDetailController.getSelectedChildren().size(), 1);
	}

	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testCaregiverById() {
		caregiverDetailController.setCaregiverById("Caregiver_Id_1");
		Assert.assertNotNull("Could not load caregiver", caregiverDetailController.getCaregiver());
		Assert.assertEquals("Could not save right caregiver",
				caregiverService.loadCaregiver("Caregiver_Id_1"),
				caregiverDetailController.getCaregiverById());
	}
	

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testSaveCaregiver() {
		Caregiver testCaregiver = caregiverService.loadCaregiver("Caregiver_Id_1");
		caregiverDetailController.setCaregiver(testCaregiver);
		Assert.assertNotNull("Could not load caregiver", caregiverDetailController.getCaregiver());
		caregiverDetailController.save();
		caregiverDetailController.reload();
		Assert.assertEquals("Could not save caregiver", caregiverDetailController.getCaregiver(),  testCaregiver);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testDeleteCaregiver() {
		Caregiver testCaregiver = caregiverService.loadCaregiver("Caregiver_Id_1");
		caregiverDetailController.setCaregiver(testCaregiver);
		Assert.assertNotNull("Could not load caregiver", caregiverDetailController.getCaregiver());
		caregiverDetailController.delete();
		Assert.assertNull("Could not delete caregiver", caregiverService.loadCaregiver("Caregiver_Id_1"));
	}


}

package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Message;
import at.qe.sepm.skeleton.services.CaregiverService;
import at.qe.sepm.skeleton.services.MessageService;
import at.qe.sepm.skeleton.ui.controllers.MessageDetailController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link MessageDetailController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class MessageDetailControllerTest {

	@Autowired
	CaregiverService caregiverService;

	@Autowired
	MessageDetailController messageDetailController;

	@Autowired
	MessageService messageService;


	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = {"EMPLOYEE", "ADMIN"})
	public void testCreateMassage() {
		messageDetailController.createMessage();
		Assert.assertNotNull("Could not create Message", messageDetailController.getMessage());
	}
	

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = {"EMPLOYEE", "ADMIN"})
	public void testConfirmCaregiver() {
		for (Message testMessage : messageService.loadAllMessagesSubjectContains("Confirm Caregiver")) {
			messageDetailController.setMessage(testMessage);
			Assert.assertEquals("Could not set message", testMessage, messageDetailController.getMessage());

			messageDetailController.confirmCaregiver();
			Assert.assertTrue("Could not confirm caregiver", messageDetailController.getMessage().isHideMessage());

			messageDetailController.saveMessage();
			Assert.assertEquals("Could not save message", testMessage, messageDetailController.getMessage());
		}
	}


	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = {"EMPLOYEE", "ADMIN"})
	public void testDeleteMessages() {
		for (Message testMessage : messageService.loadAllMessagesSubjectContains("Confirm Caregiver")) {
			messageDetailController.setMessage(testMessage);
			Assert.assertEquals("Could not set message", testMessage, messageDetailController.getMessage());

			messageDetailController.deleteMessage();
		}
		Assert.assertEquals("Could not delete messages", 0, messageService.loadAllMessagesSubjectContains("Confirm Caregiver").size());
	}


}

package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Caregiver;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.Sibling;
import at.qe.sepm.skeleton.services.CaregiverService;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.SiblingService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.controllers.ChildCreateController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Testing for {@link ChildCreateController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class ChildCreateControllerTest {

	@Autowired
	UserService userService;

	@Autowired
    CaregiverService caregiverService;
	
	@Autowired
	ChildService childService;

	@Autowired
    SiblingService siblingService;

	@Autowired
	ChildCreateController childCreateController;

	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testChildCreate() {

	    Assert.assertNotNull("Could not create new child", childCreateController.getChild());

	    childCreateController.setChild(childService.loadChild("Child_Id_1"));
        Assert.assertNotNull("Could not set child", childCreateController.getChild());

        childCreateController.setFirstName("Test");
        Assert.assertEquals("Could not set first name", "Test", childCreateController.getFirstName());

        childCreateController.setLastName("This");
        Assert.assertEquals("Could not set last name", "This", childCreateController.getLastName());

        childCreateController.setParentId("Parent_Id_1");
        Assert.assertTrue("Could not set parent", childCreateController.isParentSet());
        Assert.assertEquals("Could not set parent id", "Parent_Id_1", childCreateController.getParentId());

        childCreateController.createEmergencyPerson();
        Assert.assertNotNull("Could not create new emergency person", childCreateController.getEmergencyPerson());

        childCreateController.setEmergencyId("Parent_Id_1");
        Assert.assertEquals("Could not set emergency id", "Parent_Id_1", childCreateController.getEmergencyId());

        childCreateController.setEmergencyPersonById("Parent_Id_1");
        Assert.assertTrue("Could not set emergency person", childCreateController.isEmergencyPersonSet());
        Assert.assertEquals("Could not set emergency person id",
                "Parent_1_FN Parent_1_LN", childCreateController.getEmergencyPersonName());

        childCreateController.setEmergencyPerson(userService.loadUser("Parent_Id_1"));
        Assert.assertEquals("Could not set emergency id",
                userService.loadUser("Parent_Id_1"),
                childCreateController.getEmergencyPerson());

        Date birthday = new Date();
        childCreateController.setBirthdate(birthday);
        Assert.assertEquals("Could not set birthdate", birthday, childCreateController.getChild().getBirthdate().getTime());

        childCreateController.setGender("MALE");
        Assert.assertEquals("Could not set gender", "MALE", childCreateController.getGender());

        Calendar day = Calendar.getInstance();
        childCreateController.setDeregistrationDate(day.getTime());
        Assert.assertEquals("Could not set deregistration date", day.getTime(), childCreateController.getDeregistrationDate());

        day.add(Calendar.DATE, -1);
        childCreateController.setRegistrationDate(day.getTime());
        Assert.assertEquals("Could not set registration date", day.getTime(), childCreateController.getRegistrationDate());

        try {
            childCreateController.saveChild();
            childCreateController.redirect();
        } catch (IOException | NullPointerException e) {}


    }

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testSiblings() {

	    childCreateController.setChild(childService.loadChild("Child_Id_1"));

        try {
            Set<Child> internals = new HashSet<>(childService.loadAllChildren());
            internals.remove(childService.loadChild("Child_Id_2"));
            childCreateController.setInternals(internals);
            Assert.assertEquals("Could not set internals", internals, childCreateController.getInternals());

            Child internal = new Child();
            childCreateController.setInternal(internal);
            Assert.assertEquals("Could not set internal", internal, childCreateController.getInternal());

            childCreateController.saveInternalSibling();

            Set<Sibling> externals = new HashSet<>(siblingService.getAllSiblings());
            childCreateController.setExternals(externals);
            Assert.assertEquals("Could not set externals", externals, childCreateController.getExternals());

            int numberOfInternalSiblings = childCreateController.getInternals().size();
            childCreateController.setInternal(childService.loadChild("Child_Id_2"));
            childCreateController.addInternalSiblings();
            Assert.assertEquals("Could not add internal sibling",
                    numberOfInternalSiblings+1, childCreateController.getInternals().size());

            childCreateController.createSibling();
            int numberOfExternalSiblings = childCreateController.getExternals().size();
            childCreateController.addExternalSiblings();
            Assert.assertEquals("Could not add external sibling",
                    numberOfExternalSiblings+1, childCreateController.getExternals().size());

            childCreateController.createSibling();
            childCreateController.saveExternSibling();

            childCreateController.setSibling(siblingService.loadSibling("Sibling_Id_1"));
            Assert.assertEquals("Could not set external",
                    siblingService.loadSibling("Sibling_Id_1").getBirthdate().getTime(),
                    childCreateController.getSiblingBirthdate());
        } catch(NullPointerException e) { }
	}

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
    public void testCaregiver() {

        childCreateController.setChild(childService.loadChild("Child_Id_1"));

        childCreateController.createCaregiver();
        Assert.assertTrue("Could not create caregiver", childCreateController.isCaregiverSet());
        Assert.assertNotNull("Could not create caregiver", childCreateController.getCaregiver());

        childCreateController.setCaregiver(caregiverService.loadCaregiver("Caregiver_Id_1"));
        childCreateController.addCaregiver();
        Assert.assertTrue("Could not add caregiver",
                childCreateController.getCaregivers().contains(caregiverService.loadCaregiver("Caregiver_Id_1")));

        childCreateController.setCaregivers(new HashSet<Caregiver>(caregiverService.loadAllCaregivers()));
        Assert.assertEquals("Could not add caregivers",
                caregiverService.loadAllCaregivers().size(), childCreateController.getCaregivers().size());

        childCreateController.setFilteredCaregiver(caregiverService.loadAllConfirmedCaregivers());
        Assert.assertEquals("Could not filter caregivers",
                childCreateController.getFilteredCaregiver(), caregiverService.loadAllConfirmedCaregivers());

    }
}

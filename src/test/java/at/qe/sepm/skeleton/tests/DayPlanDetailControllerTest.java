package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.services.DayPlanService;
import at.qe.sepm.skeleton.ui.controllers.DayPlanDetailController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Calendar;

/**
 * Testing for {@link DayPlanDetailController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class DayPlanDetailControllerTest {

	@Autowired
	DayPlanService dayPlanService;

	@Autowired
	DayPlanDetailController dayPlanDetailController;


	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testSetDayPlan() {
		Calendar day = Calendar.getInstance();
		day.set(2017, Calendar.OCTOBER, 2);
		DayPlan testDayPlan = dayPlanService.loadDayPlan(day);
		dayPlanDetailController.setDayPlan(testDayPlan);
		Assert.assertEquals("Could not load dayPlan", dayPlanDetailController.getDayPlan(), testDayPlan);
		dayPlanDetailController.setDayPlan(Calendar.getInstance());
		Assert.assertNotNull("Could not load new dayPlan", dayPlanDetailController.getDayPlan());
	}


    @DirtiesContext
    @Test
    @WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
    public void testDepartureTimeOfToday() {
        Assert.assertEquals("Got wrong departure times", dayPlanDetailController.getDepartureTimeOfToday(), "");
    }


	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testArrivalTimeOfToday() {
		Assert.assertEquals("Got wrong arrival times", dayPlanDetailController.getArrivalTimeOfToday(), "Heute geschlossen!");
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testCurrentWeeksMonday() {
		Calendar testDay = Calendar.getInstance();
		Calendar monday = dayPlanDetailController.getCurrentWeeksMonday();
		Assert.assertEquals("Got wrong day of week", monday.get(Calendar.DAY_OF_WEEK), 2);
		monday.add(Calendar.DATE, 7);
		Assert.assertTrue("Not current week", monday.after(testDay));
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testCurrentWeeksSunday() {
		Calendar testDay = Calendar.getInstance();
		Calendar sunday = dayPlanDetailController.getCurrentWeeksSunday();
		Assert.assertEquals("Got wrong day of week", sunday.get(Calendar.DAY_OF_WEEK), 1);
		testDay.add(Calendar.DATE, 8);
		Assert.assertTrue("Not current week", sunday.before(testDay));
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testArrivalTime() {
		Calendar day = Calendar.getInstance();
		Calendar test = Calendar.getInstance();
		day.set(2017, Calendar.OCTOBER, 2);
		DayPlan testDayPlan = dayPlanService.loadDayPlan(day);
		dayPlanDetailController.setDayPlan(testDayPlan);
		Assert.assertEquals("Could not load dayPlan", dayPlanDetailController.getDayPlan(), testDayPlan);

		Assert.assertEquals("Wrong default earliest arrival time", dayPlanDetailController.getEarliestArrivalTime(), testDayPlan.getEarliestArrivalDate().getTime());
		Assert.assertEquals("Wrong default latest arrival time", dayPlanDetailController.getLatestArrivalTime(), testDayPlan.getLatestArrivalDate().getTime());

		day.set(Calendar.HOUR, 2);
		dayPlanDetailController.setEarliestArrivalTime(day.getTime());
		test.setTime(dayPlanDetailController.getEarliestArrivalTime());
		Assert.assertEquals("Wrong earliest arrival time", test.get(Calendar.HOUR), day.get(Calendar.HOUR));

		day.set(Calendar.HOUR, 3);
		dayPlanDetailController.setLatestArrivalTime(day.getTime());
		test.setTime(dayPlanDetailController.getLatestArrivalTime());
		Assert.assertEquals("Wrong latest arrival time", test.get(Calendar.HOUR), day.get(Calendar.HOUR));

	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testDepartureTime() {
		Calendar day = Calendar.getInstance();
		Calendar test = Calendar.getInstance();
		day.set(2017, Calendar.OCTOBER, 2);
		DayPlan testDayPlan = dayPlanService.loadDayPlan(day);
		dayPlanDetailController.setDayPlan(testDayPlan);
		Assert.assertEquals("Could not load dayPlan", dayPlanDetailController.getDayPlan(), testDayPlan);

		Assert.assertEquals("Wrong default earliest departure time", dayPlanDetailController.getEarliestDepartureTime(), testDayPlan.getEarliestDepartureDate().getTime());
		Assert.assertEquals("Wrong default latest departure time", dayPlanDetailController.getLatestDepartureTime(), testDayPlan.getLatestDepartureDate().getTime());

		day.set(Calendar.HOUR, 5);
		dayPlanDetailController.setEarliestDepartureTime(day.getTime());
		test.setTime(dayPlanDetailController.getEarliestDepartureTime());
		Assert.assertEquals("Wrong earliest departure time", test.get(Calendar.HOUR), day.get(Calendar.HOUR));

		day.set(Calendar.HOUR, 6);
        dayPlanDetailController.setLatestDepartureTime(day.getTime());
        test.setTime(dayPlanDetailController.getLatestDepartureTime());
		Assert.assertEquals("Wrong latest departure time", test.get(Calendar.HOUR), day.get(Calendar.HOUR));
	}
	
	/*@DirtiesContext
    @Test
    @WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testCurrentDayStats() {
	    
	    Calendar today = Calendar.getInstance();
        if(dayPlanDetailController.getCurrentDayPlan() == null) {
            dayPlanDetailController.createDayPlan(today);
        } else {
            dayPlanDetailController.setDayPlan(today);
        }
        today.set(Calendar.MILLISECOND, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.HOUR, 8);
        dayPlanDetailController.setEarliestArrivalTime(today.getTime());
        today.set(Calendar.HOUR, 10);
        dayPlanDetailController.setLatestArrivalTime(today.getTime());
        today.set(Calendar.HOUR, 19);
        dayPlanDetailController.setEarliestDepartureTime(today.getTime());
        today.set(Calendar.HOUR, 21);
        today.set(Calendar.MINUTE, 34);
        dayPlanDetailController.setLatestDepartureTime(today.getTime());
        
        dayPlanService.saveDayPlan(dayPlanDetailController.getDayPlan());
        
        Assert.assertFalse(dayPlanDetailController.getCurrentDayPlan() == null);
        Assert.assertEquals(dayPlanDetailController.getArrivalTimeOfToday(), "8:00 - 10:00");
        Assert.assertEquals(dayPlanDetailController.getDepartureTimeOfToday(), "19:00 - 21:34");
	}*/

}

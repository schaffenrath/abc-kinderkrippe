package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import java.util.Calendar;
import java.util.List;

import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link AttendanceService}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class AttendanceServiceTest {

    @Autowired
    AttendanceService attendanceService;

    @Autowired
    DayPlanService dayPlanService;

    @Autowired
    ChildService childService;

    @Autowired
    UserService userService;

    @Autowired
    LunchService lunchService;

    @Autowired
    CaregiverService caregiverService;

    @Autowired
    SessionInfoBean sessionInfoBean;

    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"EMPLOYEE", "ADMIN"})
    public void testDatainitialization() {
        Assert.assertTrue("Insufficient amount of attendances initialized for test data source", 6 <= attendanceService.loadAllAttendances().size());

        Calendar dayPlanDate1 = Calendar.getInstance();
        dayPlanDate1.set(2017, Calendar.OCTOBER, 2, 0, 0, 0);
        Calendar dayPlanDate2 = Calendar.getInstance();
        dayPlanDate2.set(2017, Calendar.OCTOBER, 3, 0, 0, 0);
        Calendar dayPlanDate3 = Calendar.getInstance();
        dayPlanDate3.set(2017, Calendar.OCTOBER, 4, 0, 0, 0);

        for (Attendance attendance : attendanceService.loadAllAttendances()) {
            Assert.assertNotNull("attendance " + attendance.getId() + " does not have a creationDate defined", attendance.getCreationDate());
            Assert.assertNotNull("attendance " + attendance.getId() + " does not have a dayPlan defined", attendance.getDayPlan());
            Assert.assertNotNull("attendance " + attendance.getId() + " does not have a child defined", attendance.getChild());

            if (null != attendance.getId()) switch (attendance.getId()) {
                case "Attendance_Id_1":
                    Assert.assertTrue(attendance.getDayPlan().equals(dayPlanService.loadDayPlan(dayPlanDate1)));
                    Assert.assertTrue(attendance.getChild().equals(childService.loadChild("Child_Id_1")));
                    Assert.assertTrue(attendance.getLunch().equals(lunchService.loadLunch("Lunch_Id_1")));
                    Assert.assertTrue(attendance.getPickUpPerson().equals(userService.loadUser("Parent_Id_1")));
                    break;
                case "Attendance_Id_2":
                    Assert.assertTrue(attendance.getDayPlan().equals(dayPlanService.loadDayPlan(dayPlanDate1)));
                    Assert.assertTrue(attendance.getChild().equals(childService.loadChild("Child_Id_5")));
                    Assert.assertNull(attendance.getLunch());
                    Assert.assertTrue(attendance.getPickUpPerson().equals(userService.loadUser("Parent_Id_5")));
                    break;
                case "Attendance_Id_3":
                    Assert.assertTrue(attendance.getDayPlan().equals(dayPlanService.loadDayPlan(dayPlanDate2)));
                    Assert.assertTrue(attendance.getChild().equals(childService.loadChild("Child_Id_2")));
                    Assert.assertTrue(attendance.getLunch().equals(lunchService.loadLunch("Lunch_Id_3")));
                    Assert.assertTrue(attendance.getPickUpPerson().equals(userService.loadUser("Parent_Id_1")));
                    break;
                case "Attendance_Id_4":
                    Assert.assertTrue(attendance.getDayPlan().equals(dayPlanService.loadDayPlan(dayPlanDate3)));
                    Assert.assertTrue(attendance.getChild().equals(childService.loadChild("Child_Id_3")));
                    Assert.assertTrue(attendance.getLunch().equals(lunchService.loadLunch("Lunch_Id_5")));
                    Assert.assertTrue(attendance.getPickUpPerson().equals(userService.loadUser("Parent_Id_4")));
                    break;
                default:
                    break;
            }
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testCreateAttendance() {
        Calendar date = Calendar.getInstance();
        date.set(Calendar.YEAR, 2017);
        date.set(Calendar.MONTH, Calendar.OCTOBER);
        date.set(Calendar.DAY_OF_MONTH, 3);

        Calendar time = Calendar.getInstance();

        DayPlan dayPlan = dayPlanService.loadDayPlan(date);
        Child child = childService.loadChild("Child_Id_5");
        Lunch lunch = lunchService.loadLunch("Lunch_Id_6");
        Person pickUpPerson = caregiverService.loadCaregiver("Caregiver_Id_7");
        Assert.assertNotNull("Couldn't load dayplan", dayPlan);
        Assert.assertNotNull("Couldn't load child", child);
        Assert.assertNotNull("Couldn't load lunch", lunch);
        Assert.assertNotNull("Couldn't load pickupperson", pickUpPerson);

        Attendance toBeCreatedAttendance = new Attendance(dayPlan, child);
        toBeCreatedAttendance.setArrivalTime(time);
        toBeCreatedAttendance.setDepartureTime(time);
        toBeCreatedAttendance.setLunch(lunch);
        toBeCreatedAttendance.setPickUpPerson(pickUpPerson);

        attendanceService.saveAttendance(toBeCreatedAttendance);

        Attendance freshlyCreatedAttendance = attendanceService.loadAttendance(toBeCreatedAttendance.getId());
        Assert.assertNotNull("Couldn't load freshly saved attendance", freshlyCreatedAttendance);
        Assert.assertEquals("New attendance does not have the correct id saved", toBeCreatedAttendance.getId(), freshlyCreatedAttendance.getId());
        Assert.assertEquals("New attendance does not have the correct child saved", child, freshlyCreatedAttendance.getChild());
        Assert.assertEquals("New attendance does not have the correct dayplan saved", dayPlan, freshlyCreatedAttendance.getDayPlan());
        Assert.assertEquals("New attendance does not have the correct arrivaltime saved", time, freshlyCreatedAttendance.getArrivalTime());
        Assert.assertEquals("New attendance does not have the correct departuretime saved", time, freshlyCreatedAttendance.getDepartureTime());
        Assert.assertEquals("New attendance does not have the correct lunch saved", lunch, freshlyCreatedAttendance.getLunch());
        Assert.assertEquals("New attendance does not hace the correct pickupperson saved", pickUpPerson, freshlyCreatedAttendance.getPickUpPerson());
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testUpdateAttendance() {
        Attendance toBeUpdatedAttendance = attendanceService.loadAttendance("Attendance_Id_1");
        Assert.assertNotNull("Couldn't load toBeUpdatedAttendance", toBeUpdatedAttendance);

        Calendar date = Calendar.getInstance();
        date.set(Calendar.YEAR, 2017);
        date.set(Calendar.MONTH, Calendar.OCTOBER);
        date.set(Calendar.DAY_OF_MONTH, 3);

        Calendar time = Calendar.getInstance();

        DayPlan dayPlan = dayPlanService.loadDayPlan(date);
        Child child = childService.loadChild("Child_Id_5");
        Lunch lunch = lunchService.loadLunch("Lunch_Id_6");
        Person pickUpPerson = caregiverService.loadCaregiver("Caregiver_Id_7");
        Assert.assertNotNull("Couldn't load dayplan", dayPlan);
        Assert.assertNotNull("Couldn't load child", child);
        Assert.assertNotNull("Couldn't load lunch", lunch);
        Assert.assertNotNull("Couldn't load pickupperson", pickUpPerson);

        toBeUpdatedAttendance.setPickUpPerson(pickUpPerson);
        toBeUpdatedAttendance.setLunch(lunch);
        toBeUpdatedAttendance.setDepartureTime(time);
        toBeUpdatedAttendance.setArrivalTime(time);
        toBeUpdatedAttendance.setDayPlan(dayPlan);
        toBeUpdatedAttendance.setChild(child);

        attendanceService.saveAttendance(toBeUpdatedAttendance);

        Attendance freshlyCreatedAttendance = attendanceService.loadAttendance(toBeUpdatedAttendance.getId());
        Assert.assertNotNull("Couldn't load updated attendance", freshlyCreatedAttendance);
        Assert.assertEquals("Updated attendance does not have the correct id saved", toBeUpdatedAttendance.getId(), freshlyCreatedAttendance.getId());
        Assert.assertEquals("Updated attendance does not have the correct child saved", child, freshlyCreatedAttendance.getChild());
        Assert.assertEquals("Updated attendance does not have the correct dayplan saved", dayPlan, freshlyCreatedAttendance.getDayPlan());
        Assert.assertEquals("Updated attendance does not have the correct arrivaltime saved", time, freshlyCreatedAttendance.getArrivalTime());
        Assert.assertEquals("Updated attendance does not have the correct departuretime saved", time, freshlyCreatedAttendance.getDepartureTime());
        Assert.assertEquals("Updated attendance does not have the correct lunch saved", lunch, freshlyCreatedAttendance.getLunch());
        Assert.assertEquals("Updated attendance does not hace the correct pickupperson saved", pickUpPerson, freshlyCreatedAttendance.getPickUpPerson());
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadAttendancesByChild() {
        List<Attendance> attendances = attendanceService.loadAttendancesByChild(childService.loadChild("Child_Id_5"));

        Assert.assertNotNull("Couldn't load list of attendances", attendances);
        Assert.assertFalse("List of attendances is empty", attendances.isEmpty());
        Assert.assertTrue("List of attendances doesn't contain attendance 2", attendances.contains(attendanceService.loadAttendance("Attendance_Id_2")));
        Assert.assertTrue("List of attendances doesn't contain attendance 6", attendances.contains(attendanceService.loadAttendance("Attendance_Id_6")));

        for(Attendance a : attendances) {
            switch (a.getId()) {
                case "Attendance_Id_2":
                    Assert.assertEquals(childService.loadChild("Child_Id_5"), a.getChild());
                    Assert.assertNull(a.getLunch());
                    Assert.assertEquals(userService.loadUser("Parent_Id_5"), a.getPickUpPerson());
                    break;
                case "Attendance_Id_6":
                    Assert.assertEquals(childService.loadChild("Child_Id_5"), a.getChild());
                    Assert.assertEquals(lunchService.loadLunch("Lunch_Id_6"), a.getLunch());
                    Assert.assertEquals(userService.loadUser("Parent_Id_5"), a.getPickUpPerson());
                    break;
            }
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
    public void testLoadAttendancesOfChildrenByParentAuthorizedAsParent() {
        List<Attendance> attendances = attendanceService.loadAttendancesOfChildrenByParent(sessionInfoBean.getCurrentUser());
        Assert.assertTrue("getAttendancesOfChildrenByParent(" + sessionInfoBean.getCurrentUser().getId() + ") does not contain all attendances of all children of " + sessionInfoBean.getCurrentUser().getId(), attendances.contains(attendanceService.loadAttendance("Attendance_Id_1")));
        Assert.assertTrue("getAttendancesOfChildrenByParent(" + sessionInfoBean.getCurrentUser().getId() + ") does not contain all attendances of all children of " + sessionInfoBean.getCurrentUser().getId(), attendances.contains(attendanceService.loadAttendance("Attendance_Id_3")));
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadAttendancesOfChildrenByParentAuthorizedAsEmployee() {
        List<Attendance> attendances = attendanceService.loadAttendancesOfChildrenByParent(userService.loadUser("Parent_Id_1"));
        Assert.assertTrue("getAttendancesOfChildrenByParent(" + sessionInfoBean.getCurrentUser().getId() + ") does not contain all attendances of all children of " + sessionInfoBean.getCurrentUser().getId(), attendances.contains(attendanceService.loadAttendance("Attendance_Id_1")));
        Assert.assertTrue("getAttendancesOfChildrenByParent(" + sessionInfoBean.getCurrentUser().getId() + ") does not contain all attendances of all children of " + sessionInfoBean.getCurrentUser().getId(), attendances.contains(attendanceService.loadAttendance("Attendance_Id_3")));
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"ADMIN"})
    public void testLoadAttendancesOfChildrenByParentAuthorizedAsAdmin() {
        List<Attendance> attendances = attendanceService.loadAttendancesOfChildrenByParent(userService.loadUser("Parent_Id_1"));
        Assert.assertTrue("getAttendancesOfChildrenByParent(" + sessionInfoBean.getCurrentUser().getId() + ") does not contain all attendances of all children of " + sessionInfoBean.getCurrentUser().getId(), attendances.contains(attendanceService.loadAttendance("Attendance_Id_1")));
        Assert.assertTrue("getAttendancesOfChildrenByParent(" + sessionInfoBean.getCurrentUser().getId() + ") does not contain all attendances of all children of " + sessionInfoBean.getCurrentUser().getId(), attendances.contains(attendanceService.loadAttendance("Attendance_Id_3")));
    }

    @DirtiesContext
    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser(username = "parent3@gmx.at", authorities = {"Parent"})
    public void testUnauthorizedLoadAttendancesOfChildrenByParent() {
        List<Attendance> attendances = attendanceService.loadAttendancesOfChildrenByParent(userService.loadUser("Parent_Id_1"));
    }

    @Test(expected = org.springframework.security.authentication.AuthenticationCredentialsNotFoundException.class)
    public void testUnauthenticatedLoadAttendancesOfChildrenByParent() {
        List<Attendance> attendances = attendanceService.loadAttendancesOfChildrenByParent(userService.loadUser("Parent_Id_1"));
        Assert.fail("Call to attendanceService.getAttendancesOfChildrenByParent should not work without proper authorization");
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
    public void testLoadAttendancesOfChildrenByParentAuthorizedAsParentFromDate() {
        Calendar date = Calendar.getInstance();
        date.set(Calendar.YEAR, 2017);
        date.set(Calendar.MONTH, Calendar.OCTOBER);
        date.set(Calendar.DAY_OF_MONTH, 3);

        List<Attendance> attendances = attendanceService.loadAttendancesOfChildrenByParentFromDate(sessionInfoBean.getCurrentUser(), date);
        Assert.assertFalse("getAttendancesOfChildrenByParentFromDate(" + sessionInfoBean.getCurrentUser().getId() + ") does contain attendances before the given date of all children of " + sessionInfoBean.getCurrentUser().getId(), attendances.contains(attendanceService.loadAttendance("Attendance_Id_1")));
        Assert.assertTrue("getAttendancesOfChildrenByParentFromDate(" + sessionInfoBean.getCurrentUser().getId() + ")  does not contain all attendances of all children of " + sessionInfoBean.getCurrentUser().getId(), attendances.contains(attendanceService.loadAttendance("Attendance_Id_3")));
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        for (Attendance a : attendances) {
            Assert.assertFalse("getAttendancesOfChildrenByParentFromDate returns older dates than the one given ", a.getDayPlan().getDayDate().before(date));
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"ADMIN"})
    public void testDeleteAttendance() {
        Attendance toBeDeletedAttendance = attendanceService.loadAttendance("Attendance_Id_1");
        Assert.assertNotNull("Attendance could not be loaded from test data source", toBeDeletedAttendance);
        int oldSize = attendanceService.loadAllAttendances().size();

        attendanceService.deleteAttendance(toBeDeletedAttendance);
        Assert.assertTrue("No attendance has been deleted after calling AttendanceService.deleteAttendance", (oldSize - 1) == attendanceService.loadAllAttendances().size());

        Attendance deletedAttendance = attendanceService.loadAttendance("Attendance_Id_1");
        Assert.assertNull("Deleted Attendance could still be loaded from test data source via AttendanceService.loadAttendance", deletedAttendance);

        for (Attendance remainingAttendance : attendanceService.loadAllAttendances()) {
            Assert.assertNotEquals("Deleted Attendance could still be loaded from test data source via AttendanceService.getAllAttendances", toBeDeletedAttendance.getId(), remainingAttendance.getId());
            Assert.assertNotEquals("Deleted Attendance could still be loaded from test data source via AttendanceService.getAllAttendances", toBeDeletedAttendance, remainingAttendance);

        }
    }
}

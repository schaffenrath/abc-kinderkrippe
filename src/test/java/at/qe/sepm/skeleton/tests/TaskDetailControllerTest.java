package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.services.TaskService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.controllers.TaskDetailController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Calendar;

/**
 * Testing for {@link TaskDetailController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class TaskDetailControllerTest {

	@Autowired
	UserService userService;

	@Autowired
	TaskService taskService;

	@Autowired
	TaskDetailController taskDetailController;

	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testTask() {
		Task testTask = taskService.loadTask("Task_Id_1");
		taskDetailController.setTask(testTask);
		Assert.assertNotNull("Could not load task", taskDetailController.getTask());
		Assert.assertEquals("Could not save right task", taskDetailController.getTask(), testTask);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testTaskById() {
		taskDetailController.setTaskById("Task_Id_1");
		Assert.assertNotNull("Could not load task", taskDetailController.getTask());
		Assert.assertEquals("Could not save right task",
                "at.qe.sepm.skeleton.model.Task[ id='Task_Id_1']",
                taskDetailController.getTaskById().toString());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testCreateTask() {
		taskDetailController.createTask();
		Assert.assertTrue("Could not load task", taskDetailController.isTaskSelected());

		Calendar day = Calendar.getInstance();
		taskDetailController.setInfoDate(day.getTime());
		Assert.assertEquals("Could not save info date", day.getTime(), taskDetailController.getInfoDate());

		day.setTime(taskDetailController.getEarliestEnd());
		day.add(Calendar.DATE, 1);
		taskDetailController.setDeadlineFrom(day.getTime());
		Assert.assertEquals("Could not save deadline date", day.getTime(), taskDetailController.getDeadlineFrom());

		day.add(Calendar.DATE, 1);
		taskDetailController.setDeadlineTo(day.getTime());
		Assert.assertEquals("Could not save info date", day.getTime(), taskDetailController.getDeadlineTo());

        taskDetailController.setParentById("Parent_Id_1");
        Assert.assertNotNull("Could not load parent", taskDetailController.getParentById());

		int numberOfTasks = taskService.loadUncompletedTasks().size();
		taskDetailController.save();
		Assert.assertEquals("Could not save task", numberOfTasks+1, taskService.loadUncompletedTasks().size());

		taskDetailController.sendCreationMail();
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testComplete() {
		taskDetailController.setTaskById("Task_Id_1");
		Assert.assertNotNull("Could not load task", taskDetailController.getTaskById());
		taskDetailController.setParentById("Parent_Id_1");
		Assert.assertNotNull("Could not load parent", taskDetailController.getParentById());
		try{
                taskDetailController.complete();
                } catch(NullPointerException e){}
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testEdited() {
		taskDetailController.setTaskById("Task_Id_1");
		Assert.assertNotNull("Could not load task", taskDetailController.getTaskById());
		taskDetailController.setParent(userService.loadUser("Parent_Id_1"));
		Assert.assertNotNull("Could not load parent", taskDetailController.getParent());
		taskDetailController.sendEditedMail();
	}


}

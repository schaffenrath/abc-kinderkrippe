package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Caregiver;
import at.qe.sepm.skeleton.services.*;
import at.qe.sepm.skeleton.ui.controllers.CaregiverCreateController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link ChildService}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class CaregiverCreateTest {

	@Autowired
	CaregiverService caregiverService;


	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testSaveCaregiver() {
		CaregiverCreateController caregiverCreateController = new CaregiverCreateController();
		Caregiver testCaregiver = caregiverService.loadCaregiver("Caregiver_Id_1");
		Assert.assertNotNull("Could not load caregiver", testCaregiver);
		caregiverCreateController.setCaregiver(testCaregiver);
		Assert.assertNotNull("Could not load caregiver", caregiverCreateController.getCaregiver());
		Assert.assertEquals("Wrong number of children", caregiverCreateController.getCaregiver().getChildren().size(), 1);
		try {
		    caregiverCreateController.save();
        } catch (NullPointerException e) {}
		Assert.assertNotEquals("Could not save caregiver", caregiverCreateController.getCaregiver(),  testCaregiver);
	}


}

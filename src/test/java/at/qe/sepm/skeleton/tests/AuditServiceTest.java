/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.services.AuditService;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link AuditService}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class AuditServiceTest {
    
    @Autowired
    private AuditService auditService;
    
    @Test
    public void testAuditMessage()
    {
        try
        {
            auditService.log(AuditService.Action.Info,    "TEST");
            auditService.log(AuditService.Action.Warning, "TEST");
            auditService.log(AuditService.Action.Create,  "TEST");
            auditService.log(AuditService.Action.Modify,  "TEST");
            auditService.log(AuditService.Action.Delete,  "TEST");
        }
        catch(Exception e)
        {
            fail("Exception thrown: " + e.getMessage());
        }
    }
}

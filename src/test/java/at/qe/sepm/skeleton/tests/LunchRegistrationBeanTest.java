package at.qe.sepm.skeleton.tests;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.services.AttendanceService;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.LunchService;
import at.qe.sepm.skeleton.ui.beans.LunchRegistrationBean;

/**
 * Testing for {@link LunchRegistrationBean}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class LunchRegistrationBeanTest {

    @Autowired
    private LunchRegistrationBean lunchRegistrationBean;
    
    @Autowired
    private LunchService lunchService;
    
    @Autowired
    private AttendanceService attendanceService;
    
    @Autowired
    private ChildService childService;

    @DirtiesContext
    @Test
    @WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
    public void testSaveRegistration() {
        lunchRegistrationBean.init();
        Collection<String> attendances = lunchRegistrationBean.getAttendances();
        if(attendances == null) {
            attendances = new HashSet<>();
        }
        attendances.add("Attendance_Id_1");
        lunchRegistrationBean.setAttendances(attendances);
        lunchRegistrationBean.setLunch("Lunch_Id_2");
        lunchRegistrationBean.setDayPlan(attendanceService.loadAttendance("Attendance_Id_1").getDayPlan());
        lunchRegistrationBean.saveRegistration();
        Assert.assertEquals(attendanceService.loadAttendance("Attendance_Id_1").getLunch().getId(), "Lunch_Id_2");
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
    public void testGetPossibleChildren() {
        lunchRegistrationBean.init();
        lunchRegistrationBean.setDayPlan(attendanceService.loadAttendance("Attendance_Id_1").getDayPlan());
        Assert.assertTrue(lunchRegistrationBean.getPossibleChildren().contains(attendanceService.loadAttendance("Attendance_Id_1")));
        Assert.assertFalse(lunchRegistrationBean.getPossibleChildren().contains(attendanceService.loadAttendance("Attendance_Id_2")));
        Assert.assertFalse(lunchRegistrationBean.getPossibleChildren().contains(attendanceService.loadAttendance("Attendance_Id_3")));
    }
    
    @DirtiesContext
    @Test
    @WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
    public void testGetRegisteredChildren() {
        lunchRegistrationBean.init();
        Collection<Child> children = lunchRegistrationBean.getRegisteredChildren(lunchService.loadLunch("Lunch_Id_1"));
        Assert.assertTrue(children.contains(childService.loadChild("Child_Id_1")));
    }
    
    
}

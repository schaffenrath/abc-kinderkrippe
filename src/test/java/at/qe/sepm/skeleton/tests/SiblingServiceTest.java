package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Sibling;
import at.qe.sepm.skeleton.services.SiblingService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link SiblingServiceTest}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class SiblingServiceTest {

    @Autowired
    SiblingService siblingService;

    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void testDatainitialization() {
        Assert.assertTrue("Insufficient amount of siblings initialized for test data source", 4 <= siblingService.getAllSiblings().size());
    }

    @DirtiesContext
    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void testDeleteSibling() {
        Sibling toBeDeletedSibling = siblingService.loadSibling("Sibling_Id_1");
        int oldSize = siblingService.getAllSiblings().size();

        Assert.assertNotNull("Sibling could not be loaded from test data source", toBeDeletedSibling);

        siblingService.deleteSibling(toBeDeletedSibling);

        Assert.assertTrue("No sibling has been deleted after calling SiblingService.deleteSibling", (oldSize - 1) == siblingService.getAllSiblings().size());
        Sibling deletedSibling = siblingService.loadSibling("Sibling_Id_1");
        Assert.assertNull("Deleted Sibling could still be loaded from test data source via SiblingService.loadSibling", deletedSibling);

        for (Sibling remainingSibling : siblingService.getAllSiblings()) {
            Assert.assertNotEquals("Deleted Sibling could still be loaded from test data source via SiblingService.getAllSiblings", toBeDeletedSibling.getId(), remainingSibling.getId());
        }
    }
}

package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.services.AttendanceService;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import at.qe.sepm.skeleton.ui.controllers.AttendanceDetailController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link AttendanceDetailController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class AttendanceDetailControllerTest {

	@Autowired
	ChildService childService;

	@Autowired
	AttendanceService attendanceService;

	@Autowired
	AttendanceDetailController attendanceDetailController;

	@Autowired
	SessionInfoBean sessionInfoBean;

	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testPickUpPerson() {
		Attendance testAttendance = attendanceService.loadAttendance("Attendance_Id_1");
		attendanceDetailController.setAttendance(testAttendance);
		Assert.assertNotNull("Could not load attendance", attendanceDetailController.getAttendance());

        attendanceDetailController.setPickUpPersonById("Parent_Id_1");
        Assert.assertEquals("Could not find right pickUpPerson (parent)", attendanceDetailController.getPickUpPersonById(),
				"Parent_Id_1");

		attendanceDetailController.setPickUpPersonById("Caregiver_Id_1");
		Assert.assertEquals("Could not find right pickUpPerson (caregiver)", attendanceDetailController.getPickUpPersonById(),
				"Caregiver_Id_1");
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testChangeAttending() {
		Attendance testAttendance = attendanceService.loadAttendance("Attendance_Id_1");
		attendanceDetailController.setAttendance(testAttendance);
		try{
		    attendanceDetailController.changeAttending();
		} catch (NullPointerException e) {}
		Assert.assertFalse("Could not save attendance", attendanceDetailController.getAttendance().isAttending());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testAttendanceDelete() {
		attendanceDetailController.setById("Attendance_Id_1");
		Assert.assertTrue("Could not load attendance", attendanceDetailController.isAttendanceSelected());
		attendanceDetailController.delete();
		Assert.assertNull("Could not delete attendance", attendanceService.loadAttendance("Attendance_Id_1"));
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testAttendanceById() {
		attendanceDetailController.setById("Attendance_Id_1");
		Assert.assertNotNull("Could not load attendance", attendanceDetailController.getAttendance());
		Assert.assertEquals("Wrong attendance", "at.qe.sepm.skeleton.model.Attendance[ id='Attendance_Id_1']", attendanceDetailController.getById("Attendance_Id_1").toString());
	}

}

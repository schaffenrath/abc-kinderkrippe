package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Calendar;
import java.util.List;

/**
 * Testing for {@link ChildService}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class LunchServiceTest {

    @Autowired
    LunchService lunchService;

    @Autowired
    DayPlanService dayPlanService;

    @Autowired
    AttendanceService attendanceService;

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
    public void testCreateLunch() {
        Calendar date1 = Calendar.getInstance();
        date1.set(Calendar.YEAR, 2017);
        date1.set(Calendar.MONTH, Calendar.OCTOBER);
        date1.set(Calendar.DAY_OF_MONTH, 1);

        Lunch lunch = new Lunch();
        lunch.setDayPlan(dayPlanService.loadDayPlan(date1));
        lunch.setMenu("test menu");
        lunch.setPrice((float) 0.42);
        lunchService.saveLunch(lunch);

        Lunch freshlyCreatedLunch = lunchService.loadLunch(lunch.getId());
        Assert.assertNotNull("New lunch could not be loaded from test data source after being saved",
                freshlyCreatedLunch);
        Assert.assertEquals("New lunch does not have the correct dayplan saved", lunch.getDayPlan(),
                freshlyCreatedLunch.getDayPlan());
        Assert.assertEquals("New lunch does not have the correct menu saved", lunch.getMenu(),
                freshlyCreatedLunch.getMenu());
        Assert.assertEquals("New lunch does not have the correct price saved", lunch.getPrice(),
                freshlyCreatedLunch.getPrice(), 0.000001);
        Assert.assertEquals("New lunch does not have the correct creationDate saved", lunch.getCreationDate(),
                freshlyCreatedLunch.getCreationDate());
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
    public void testLoadLunchesByConsumer() {
        List<Lunch> lunches = lunchService.loadLunchesByConsumer(attendanceService.loadAttendance("Attendance_Id_3"));
        Assert.assertEquals(1, lunches.size());
        for (Lunch lunch : lunches) {
            Assert.assertEquals("Lunch_Id_3", lunch.getId());
            Assert.assertEquals("Tagesteller", lunch.getMenu());
        }
    }
}

package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Lunch;
import at.qe.sepm.skeleton.services.DayPlanService;
import at.qe.sepm.skeleton.services.LunchService;
import at.qe.sepm.skeleton.ui.controllers.LunchDetailController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link LunchDetailController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class LunchDetailControllerTest {

	@Autowired
	LunchService lunchService;

	@Autowired
	DayPlanService dayPlanService;

	@Autowired
	LunchDetailController lunchDetailController;


	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testTodaysLunch() {
		Assert.assertNotNull("Could not load lunch message", lunchDetailController.getTodaysLunch1());
		Assert.assertEquals("Not correct lunch message", lunchDetailController.getTodaysLunch1(), "Kein Menü festgelegt");
	}
	

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testAddLunch() {
		lunchDetailController.setById("Lunch_Id_1");
		Assert.assertNotNull("Could not load lunch by id", lunchDetailController.getLunch());
		Assert.assertEquals("Not correct lunch loaded by id", lunchDetailController.getLunch().getId(), "Lunch_Id_1");

		Lunch testLunch = new Lunch();
		lunchDetailController.setLunch(testLunch);
		Assert.assertNotNull("Could not load lunch", lunchDetailController.getLunch());
		Assert.assertEquals("Not correct lunch loaded", lunchDetailController.getLunch(), testLunch);


		lunchDetailController.setMenu("Test");
		Assert.assertEquals("Not correct menu", "Test", lunchDetailController.getMenu());
		lunchDetailController.setPrice(1.00f);
		Assert.assertEquals("Not correct price", 1.00f, lunchDetailController.getPrice(), 0.01f);
	}


}

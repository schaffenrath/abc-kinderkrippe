package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import at.qe.sepm.skeleton.ui.controllers.AttendanceCreateController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;

/**
 * Testing for {@link AttendanceCreateController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class AttendanceCreateControllerTest {

	@Autowired
	ChildService childService;

	@Autowired
	AttendanceService attendanceService;

	@Autowired
	AttendanceCreateController attendanceCreateController;

	@Autowired
	SessionInfoBean sessionInfoBean;

	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testCustomTimes() {
		Attendance testAttendance = attendanceService.loadAttendance("Attendance_Id_1");
		attendanceCreateController.setAttendance(testAttendance);
		Assert.assertNotNull("Could not load attendance", attendanceCreateController.getAttendance());

		//test standard times
		Assert.assertEquals("Standard earliest arrival time not correct", attendanceCreateController.getArrivalTime(), testAttendance.getDayPlan().getEarliestArrivalDate().getTime());
		Assert.assertEquals("Standard latest departure time not correct", attendanceCreateController.getDepartureTime(), testAttendance.getDayPlan().getLatestDepartureDate().getTime());

		//test custom times
		attendanceCreateController.setCustomTimes(true);
		Calendar arrivalTime = Calendar.getInstance();
		arrivalTime.set(Calendar.YEAR, testAttendance.getDayPlan().getDayDate().get(Calendar.YEAR));
		arrivalTime.set(Calendar.MONTH, testAttendance.getDayPlan().getDayDate().get(Calendar.MONTH));
		arrivalTime.set(Calendar.DATE, testAttendance.getDayPlan().getDayDate().get(Calendar.DATE));
		attendanceCreateController.setArrivalTime(arrivalTime.getTime());
		Assert.assertEquals("Could not set custom arrival time", attendanceCreateController.getArrivalTime(), arrivalTime.getTime());
		Calendar departureTime = Calendar.getInstance();
		departureTime.set(Calendar.YEAR, testAttendance.getDayPlan().getDayDate().get(Calendar.YEAR));
		departureTime.set(Calendar.MONTH, testAttendance.getDayPlan().getDayDate().get(Calendar.MONTH));
		departureTime.set(Calendar.DATE, testAttendance.getDayPlan().getDayDate().get(Calendar.DATE));
		attendanceCreateController.setDepartureTime(departureTime.getTime());
		Assert.assertEquals("Could not set custom departure time", attendanceCreateController.getDepartureTime(), departureTime.getTime());

		//test illegal custom time arguments
		attendanceCreateController.setArrivalTime(departureTime.getTime());
		attendanceCreateController.setDepartureTime(arrivalTime.getTime());
		Assert.assertFalse("Custom times are not overlapping", attendanceCreateController.checkCustomTimes());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testChildrenToRegister() {
		Assert.assertEquals("Not same amount of children", attendanceCreateController.getParentsChildren(), childService.loadChildrenByParent(sessionInfoBean.getCurrentUser()));

		Attendance testAttendance = attendanceService.loadAttendance("Attendance_Id_1");
		attendanceCreateController.setAttendance(testAttendance);
		Assert.assertNotNull("Could not load attendance", attendanceCreateController.getAttendance());

		Assert.assertEquals("Could not ignore already registered child", attendanceCreateController.getParentsChildren().size(), 1);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testGetCaregivers() {
		Attendance testAttendance = attendanceService.loadAttendance("Attendance_Id_1");
		attendanceCreateController.setAttendance(testAttendance);
		Assert.assertNotNull("Could not load attendance", attendanceCreateController.getAttendance());

		Assert.assertEquals("Could not find all caregivers", attendanceCreateController.getCaregivers().size(), 1);

		attendanceCreateController.refreshCaregivers();
		Assert.assertEquals("Could not find all caregivers after reload", attendanceCreateController.getCaregivers().size(), 1);

	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testGetParents() {
		Attendance testAttendance = attendanceService.loadAttendance("Attendance_Id_1");
		attendanceCreateController.setAttendance(testAttendance);
		Assert.assertNotNull("Could not load attendance", attendanceCreateController.getAttendance());

		Assert.assertEquals("Could not find all parents", attendanceCreateController.getParents().size(), 2);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testPickUpPerson() {
		Attendance testAttendance = attendanceService.loadAttendance("Attendance_Id_1");
		attendanceCreateController.setAttendance(testAttendance);
		Assert.assertNotNull("Could not load attendance", attendanceCreateController.getAttendance());

		Assert.assertEquals("Could not find right pickUpPerson (parent)", attendanceCreateController.getPickUpPersonId(),
				"at.qe.sepm.skeleton.model.User[ id='Parent_Id_1']");
		attendanceCreateController.setPickUpPersonId("Caregiver_Id_1");
		Assert.assertEquals("Could not find right pickUpPerson (caregiver)", attendanceCreateController.getPickUpPersonId(),
				"at.qe.sepm.skeleton.model.Caregiver[ id='Caregiver_Id_1' name='Caregiver_1_FN Caregiver_1_LN']");
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testSaveAttendance() {
		Attendance testAttendance = attendanceService.loadAttendance("Attendance_Id_1");
		attendanceCreateController.setAttendance(testAttendance);
		Assert.assertNotNull("Could not load attendance", attendanceCreateController.getAttendance());
		attendanceCreateController.saveAttendance();
		Assert.assertEquals("Could not save attendance", attendanceCreateController.getAttendance(),  testAttendance);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testCreateAttendance() {
		attendanceCreateController.createNewAttendance();
		Assert.assertNotNull("Could not load attendance", attendanceCreateController.getAttendance());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testAttendanceById() {
		attendanceCreateController.setAttendanceById("Attendance_Id_1");
		Assert.assertNotNull("Could not load attendance", attendanceCreateController.getAttendance());
		Assert.assertEquals("Wrong attendance", "Attendance_Id_1", attendanceCreateController.getAttendanceById());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testDay() {
		Assert.assertFalse("Illegal day set", attendanceCreateController.isDaySet());

		Calendar day = Calendar.getInstance();
		day.set(2017, Calendar.OCTOBER, 02);
		attendanceCreateController.createNewAttendance();
		attendanceCreateController.setDay(day.getTime());
		Assert.assertTrue("Could not set day", attendanceCreateController.isDaySet());
		Assert.assertTrue("Could not validate right day", attendanceCreateController.isValidDay());

		attendanceCreateController.setValidDay(false);
		Assert.assertFalse("Could not set day to invalid", attendanceCreateController.isValidDay());

		attendanceCreateController.resetAttendance();
		Assert.assertTrue("Could not reset valid day", attendanceCreateController.isValidDay());
	}

}

package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.Sibling;
import at.qe.sepm.skeleton.repositories.SiblingRepository;
import at.qe.sepm.skeleton.repositories.ChildRepository;

import java.util.Calendar;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

/**
 * Service for accessing and manipulating {@link Sibling} data.
 * 
 */
@Component
@Scope("application")
public class SiblingService {

	@Autowired
	private SiblingRepository siblingRepository;

	@Autowired
	private ChildRepository childRepository;

	/**
	 * Returns a collection of all siblings.
	 *
	 * @return a collection of all siblings.
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Collection<Sibling> getAllSiblings() {
		return siblingRepository.findAll();
	}

	/**
	 * Loads a single sibling identified by its id.
	 *
	 * @param id
	 *            the id to search for
	 * @return the sibling with the given id
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Sibling loadSibling(String id) {
		return siblingRepository.findFirstById(id);
	}

	/**
	 * Saves the sibling. This method will also set {@link Sibling#creationDate}
	 * for new.
	 *
	 * @param sibling
	 *            the sibling to save
	 * @return the updated sibling
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Sibling saveSibling(Sibling sibling) {
		if (sibling.isNew()) {
			sibling.setCreationDate(Calendar.getInstance());
			// TODO: logging
		} else {
			// TODO: logging
		}
		return siblingRepository.save(sibling);
	}

	/**
	 * Deletes the sibling.
	 *
	 * @param sibling
	 *            the sibling to delete
	 */
	@PreAuthorize("hasAuthority('ADMIN')")
	public void deleteSibling(Sibling sibling) {
		for (Child relatedChild : sibling.getSiblingOf()) {
			relatedChild.removeExternalSibling(sibling);
			childRepository.save(relatedChild);
		}

		siblingRepository.delete(sibling);
	}
}

package at.qe.sepm.skeleton.services;

import java.util.Properties;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Service for sending mails.
 *
 */
@Component
@Scope("application")
public class MailService {

	private String host = "smtp.gmail.com";
	private String port = "587";
	private String username = "abc.kinderkrippe@gmail.com";
	private String password = "sepmLover";
	private Session session;

	@PostConstruct
	public void Init() {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
	}

	/**
	 * Sends multiple mails.
	 * 
	 * @param to
	 *            Array with E-Mail Addresses
	 * @param subject
	 *            Subject of the E-Mail
	 * @param message
	 *            Message of the E-Mail
	 * @throws AddressException
	 * @throws MessagingException
	 */
	public void SendMail(String[] to, String subject, String message) throws AddressException, MessagingException {
		for (String t : to) {
			SendMail(t, subject, message);
		}
	}

	/**
	 * Sends one single mail.
	 * 
	 * @param to
	 *            String with E-Mail Address
	 * @param subject
	 *            Subject of E-Mail
	 * @param message
	 *            Message of E-Mail
	 * @throws AddressException
	 * @throws MessagingException
	 */
	public void SendMail(String to, String subject, String message) throws AddressException, MessagingException {
		MimeMessage m = new MimeMessage(session);
		m.setContent(message, "text/html; charset=utf-8");
		m.setFrom(new InternetAddress(username));
		m.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
		m.setSubject(subject);
		Transport.send(m);
	}
}

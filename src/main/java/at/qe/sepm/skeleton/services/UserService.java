package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.Message;
import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.repositories.ChildRepository;
import at.qe.sepm.skeleton.repositories.MessageRepository;
import at.qe.sepm.skeleton.repositories.TaskRepository;
import at.qe.sepm.skeleton.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Collection;

/**
 * Service for accessing and manipulating {@link User} data.
 *
 */
@Component
@Scope("application")
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChildRepository childRepository;
    
    @Autowired
    private MessageRepository messageRepository;
    
    @Autowired
    private TaskRepository taskRepository;
    
    @Autowired
    private AttendanceService attendanceService;

    /**
     * Returns a collection of all users.
     *
     * @return collection of all users.
     */
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
    public Collection<User> loadAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Returns a collection of all employees.
     *
     * @return collection of all employees.
     */
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
    public Collection<User> loadAllEmployees() {
        return userRepository.findByRole(UserRole.EMPLOYEE);
    }

    /**
     * Returns a collection of all parents.
     *
     * @return collection of all parents
     */
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
    public Collection<User> loadAllParents() {
        return userRepository.findByRole(UserRole.PARENT);
    }

    /**
     * Returns a collection of all enabled parents.
     *
     * @return collection of all enabled parents
     */
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
    public Collection<User> loadAllEnabledParents() {
        return userRepository.findEnabledByRole(UserRole.PARENT);
    }

    /**
     * Returns a collection of all admins.
     *
     * @return collection of all admins
     */
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
    public Collection<User> loadAllAdmins() {
        return userRepository.findByRole(UserRole.ADMIN);
    }

    /**
     * Loads a single user identified by its id.
     *
     * @param id
     *            the id to search for
     * @return the user with the given id
     */
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('PARENT') or hasAuthority('ADMIN')")
    public User loadUser(String id) {
        User user = userRepository.findFirstById(id);
        User currentUser = getAuthenticatedUser();
        if (currentUser.getRoles().contains(UserRole.ADMIN) || currentUser.getRoles().contains(UserRole.EMPLOYEE)
                || user.getId().equals(id))
            return user;
        else
            throw new AccessDeniedException(
                    "The authenticated User " + currentUser.getId() + " doesn't have Permission load " + user.getId());
    }

    /**
     * Loads a single user by its email.
     * 
     * @param email
     *            the email to search for
     * @return the user with the given email
     */
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or principal.username eq #email ")
    public User loadUserByEmail(@Param("email") String email) {
        return userRepository.findFirstByEmail(email);
    }

    /**
     * Saves the user. This method will also set {@link User#creationDate} for
     * new entities.
     *
     * @param user
     *            the user to save
     * @return the updated user
     */
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('PARENT')")
    public User saveUser(User user) {
        if (user.isNew()) {
            user.setCreationDate(Calendar.getInstance());

        }
        return userRepository.save(user);
    }

    /**
     * Creates a new user and saves it in the DB.
     * 
     * @param email
     *            the email of the new user
     * @param firstName
     *            the firstName of the new user
     * @param lastName
     *            the lastName of the new user
     * @param password
     *            the password of the new user
     * @return the new user
     */
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
    public User createUser(String email, String firstName, String lastName, String password) {
        User user = new User();

        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPassword(new BCryptPasswordEncoder().encode(password));
        user.setEnabled(true);
        return userRepository.save(user);
    }

    /**
     * Deletes the user.
     *
     * @param user
     *            the user to delete
     */
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
    public void deleteUser(User user) {
        for (Child relatedChild : childRepository.findByParent(user)) {
            relatedChild.removeParent(user);
            childRepository.save(relatedChild);
        }
        for (Child relatedChild : childRepository.findByEmergencyContact(user)) {
            relatedChild.setEmergencyContact(null);
            childRepository.save(relatedChild);
        }
        for (Attendance relatedAttendance : attendanceService.loadAttendancesByPickUpPerson(user)) {
            relatedAttendance.setPickUpPerson(null);
            attendanceService.saveAttendance(relatedAttendance);
        }
        for (Message message : messageRepository.findBySender(user)) {
            message.setSender(null);
            messageRepository.save(message);
        }
        for (Message message : messageRepository.findByRecipient(user)) {
            message.setRecipient(null);
            messageRepository.save(message);
        }
        for (Task task : taskRepository.findByParent(user)) {
            taskRepository.delete(task);
        }
        user = loadUser(user.getId());
        userRepository.delete(user);
    }

    /**
     * Helper method to get current user
     *
     */
    private User getAuthenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth == null) {
            return null;
        } else {
            return loadUserByEmail(auth.getName());
        }
    }
}

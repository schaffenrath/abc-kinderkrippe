package at.qe.sepm.skeleton.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.configs.DayPlanServiceConfig;
import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.model.Lunch;
import at.qe.sepm.skeleton.model.MonthPlan;
import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.repositories.AttendanceRepository;
import at.qe.sepm.skeleton.repositories.DayPlanRepository;
import at.qe.sepm.skeleton.repositories.LunchRepository;
import at.qe.sepm.skeleton.services.AuditService.Action;

/**
 * Service for accessing and manipulating {@link DayPlan} data.
 * 
 */
@Component
@Scope("application")
public class DayPlanService {

    @Autowired
    private DayPlanRepository dayPlanRepository;

    @Autowired
    private AttendanceRepository attendanceRepository;

    @Autowired
    private LunchRepository lunchRepository;

    @Autowired
    private DayPlanServiceConfig dayPlanServiceConfig;

    @Autowired
    private AuditService auditService;

    /**
     * Loads a single dayplan identified by its date.
     *
     * @param date
     *            the date to search for
     * @return the dayplan with the given date
     */
    public DayPlan loadDayPlan(Calendar date) {
        Calendar dateWithoutTime = removeTimeFromDate(date);
        return dayPlanRepository.findFirstByDayDate(dateWithoutTime);
    }

    /**
     * Loads a list containing either a single dayplan identified by its date or
     * no element.
     * 
     * @param date
     *            the date to search for
     * @return a list of all dayplans with the given date
     */
    public List<DayPlan> loadDayPlans(Calendar date) {
        Calendar dateWithoutTime = removeTimeFromDate(date);
        return dayPlanRepository.findByDayDate(dateWithoutTime);
    }

    /**
     * Loads a every dayplan in a specific time span.
     *
     * @param from
     *            the start date to search for
     * @param to
     *            the end date to search for
     * @return list of dayplan from the time span
     */
    public List<DayPlan> loadDayPlansInTimeSpan(Calendar from, Calendar to) throws IllegalArgumentException {
        if (from.after(to)) {
            throw new IllegalArgumentException(
                    "Calendar 'from' must represent a time before to time represented by the Calendar 'to'.");
        }
        Calendar dateWithoutTime = Calendar.getInstance();
        dateWithoutTime.setTimeInMillis(from.getTimeInMillis());
        dateWithoutTime = removeTimeFromDate(dateWithoutTime);
        List<DayPlan> dayPlans = new ArrayList<>();
        do {
            List<DayPlan> days = dayPlanRepository.findByDayDate(dateWithoutTime);
            if (days.size() > 0) {
                dayPlans.add(days.get(0));
            }
            dateWithoutTime.add(Calendar.DATE, 1);
        } while (dateWithoutTime.before(to));
        return dayPlans;
    }

    /**
     * Loads a list of all dayPlans.
     * 
     * @return list of all dayplans
     */
    public List<DayPlan> loadAllDayPlans() {
        return dayPlanRepository.findAll();
    }

    /**
     * Saves the dayplan. This method will also set {@link DayPlan#creationDate}
     * for new entities.
     *
     * @param dayPlan
     *            the dayplan to save
     * @return the updated dayplan
     */
    public DayPlan saveDayPlan(DayPlan dayPlan) {
        if (dayPlan.isNew()) {
            dayPlan.setCreationDate(Calendar.getInstance());
            logDayPlan(Action.Create, dayPlan);
        } else {
            logDayPlan(Action.Modify, dayPlan);
        }
        return dayPlanRepository.save(dayPlan);
    }

    /**
     * Creates a new dayPlan for the given date and saves it in the database.
     * 
     * @param date
     *            the date for the new dayPlan
     * @return the new dayPlan
     */
    public DayPlan createDayPlan(Calendar date) {
        DayPlan dayPlan = new DayPlan();
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        dayPlan.setDayDate(date);
        dayPlan.setOpen(true);
        return saveDayPlan(dayPlan);
    }

    /**
     * Creates a new dayPlan for the given date and saves it in the database.
     * Also adds {@link DayPlan#earliestArrivalDate},
     * {@link DayPlan#latestArrivalDate}, {@link DayPlan#earliestDepartureDate}
     * and {@link DayPlan#latestDepartureDate}.
     * 
     * @param date
     *            the date for the new dayPlan
     * @param earliestArrivalDate
     *            the earliestArrivalDate for the new dayPlan
     * @param latestArrivalDate
     *            the latestArrivalDate for the new dayPlan
     * @param earliestDepartureDate
     *            the earliestDepartureDate for the new dayPlan
     * @param latestDepartureDate
     *            the latestDepartureDate for the new dayPlan
     * @return the new dayPlan
     */
    public DayPlan createDayPlan(Calendar date, Calendar earliestArrivalDate, Calendar latestArrivalDate,
        Calendar earliestDepartureDate, Calendar latestDepartureDate) {
        DayPlan dayPlan = new DayPlan();
        dayPlan.setDayDate(date);
        dayPlan.setEarliestArrivalDate(earliestArrivalDate);
        dayPlan.setLatestArrivalDate(latestArrivalDate);
        dayPlan.setEarliestDepartureDate(earliestDepartureDate);
        dayPlan.setLatestDepartureDate(latestDepartureDate);
        return saveDayPlan(dayPlan);
    }

    /**
     * Generates a list of all monthPlans for a specified year.
     * 
     * @param year
     *            a random date of the year asked for
     * @return the list of monthPlans
     */
    public List<MonthPlan> loadMonthPlansByYear(Calendar year) {
        List<MonthPlan> monthPlans = new ArrayList<>();
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        from.set(Calendar.MILLISECOND, 0);
        for (int i = 0; i < 12; i++) {
            int ovrOccupancy = 0;
            int open = 0;
            int avrOccupancy;
            from.set(year.get(Calendar.YEAR), i, 0, 0, 0, 0);
            to.setTimeInMillis(from.getTimeInMillis());
            to.add(Calendar.MONTH, 1);
            to.add(Calendar.DATE, -1);
            for (DayPlan dayPlan : loadDayPlansInTimeSpan(from, to)) {
                open++;
                ovrOccupancy += dayPlan.getAttendances().size();
            }

            if (open > 0) {
                avrOccupancy = ovrOccupancy / open;
            } else {
                avrOccupancy = 0;
            }
            monthPlans.add(new MonthPlan(i, from.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.GERMAN),
                    ovrOccupancy, avrOccupancy, open));
        }
        return monthPlans;
    }

    /**
     * Creates a new attendance for the given dayplan and child.
     * 
     * @param dayPlan
     *            the dayplan for the new attendance
     * @param child
     *            the child that will attend the establishment
     * @param arrivalTime
     *            the time the child will arrive at the establishment
     * @param departureTime
     *            the time the child will leave the establishment
     * @return the new attendance
     */
    public Attendance addAttendance(DayPlan dayPlan, Child child, Calendar arrivalTime, Calendar departureTime) {
        Attendance attendance = new Attendance();
        attendance.setDayPlan(dayPlan);
        attendance.setChild(child);
        attendance.setCreationDate(Calendar.getInstance());
        attendance.setArrivalTime(arrivalTime);
        attendance.setDepartureTime(departureTime);
        return attendanceRepository.save(attendance);
    }

    /**
     * Creates a new attendance for the given dayplan and child.
     * 
     * @param dayPlan
     *            the dayplan for the new attendance
     * @param child
     *            the child that will attend the establishment
     * @param arrivalTime
     *            the time the child will arrive at the establishment
     * @param departureTime
     *            the time the child will leave the establishment
     * @param pickUpPerson
     *            the person that will fetch the child
     * @return the new attendance
     */
    public Attendance addAttendance(DayPlan dayPlan, Child child, Calendar arrivalTime, Calendar departureTime,
            Person pickUpPerson) {
        Attendance attendance = addAttendance(dayPlan, child, arrivalTime, departureTime);
        attendance.setPickUpPerson(pickUpPerson);
        return attendanceRepository.save(attendance);
    }

    /**
     * Creates a new lunch for the given dayplan.
     * 
     * @param dayPlan
     *            the dayplan for the new lunch
     * @param menu
     *            the menu that will be offered
     * @param price
     *            the price for the menu
     * @return the new lunch
     */
    public Lunch addLunch(DayPlan dayPlan, String menu, float price) {
        Lunch lunch = new Lunch();
        lunch.setDayPlan(dayPlan);
        lunch.setCreationDate(Calendar.getInstance());
        lunch.setMenu(menu);
        lunch.setPrice(price);
        return lunchRepository.save(lunch);
    }

    /**
     * Gives a the deadline for lunches of the given week.
     * 
     * @param init
     *            a random date of the week
     * @return the deadline
     */
    public Calendar loadCurrentLunchDeadline(Calendar init) {
        Calendar initCal = init;
        while (initCal.get(Calendar.DAY_OF_WEEK) != Integer
                .parseInt(dayPlanServiceConfig.getDPconfig("lunch.deadline.day"))) {
            initCal.add(Calendar.DATE, 1);
        }
        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(Long.parseLong(dayPlanServiceConfig.getDPconfig("lunch.deadline.time")));
        initCal.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
        initCal.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
        initCal.set(Calendar.SECOND, 0);
        initCal.set(Calendar.MILLISECOND, 0);
        return initCal;
    }

    /**
     * Resets dpconfig.properties to the default values.
     * 
     */
    public void resetDayPlanConfig() {
        dayPlanServiceConfig.resetDPconfig();
    }

    private Calendar removeTimeFromDate(Calendar date) {
        Calendar dateWithoutTime = date;
        dateWithoutTime.set(Calendar.HOUR_OF_DAY, 0);
        dateWithoutTime.set(Calendar.MINUTE, 0);
        dateWithoutTime.set(Calendar.SECOND, 0);
        dateWithoutTime.set(Calendar.MILLISECOND, 0);
        return dateWithoutTime;
    }

    /**
     * Logs the state of an dayplan object.
     * 
     * @param action
     *            Action performed
     * @param dayplan
     *            Object instance
     */
    private void logDayPlan(AuditService.Action action, DayPlan dayplan) {
        try {
            StringBuilder str = new StringBuilder();
            str.append("DayPlan" + "[" + dayplan.toString() + "]:\n");

            if (dayplan.getDayDate() != null)
                str.append("\tDayDate: " + dayplan.getDayDate().toString() + "\n");

            if (dayplan.getEarliestArrivalDate() != null)
                str.append("\tEarliestArrivalDate: " + dayplan.getEarliestArrivalDate().toString() + "\n");

            if (dayplan.getEarliestDepartureDate() != null)
                str.append("\tEarliestDepartureDate: " + dayplan.getEarliestDepartureDate().toString() + "\n");

            str.append("\tMaxAttendances: " + dayplan.getMaxAttendances() + "]\n");

            auditService.log(action, str.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

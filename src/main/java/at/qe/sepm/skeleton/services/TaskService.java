package at.qe.sepm.skeleton.services;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.repositories.TaskRepository;

/**
 * Service for accessing and manipulating {@link Task} data.
 * 
 */
@Component
@Scope("application")
public class TaskService {
	
	@Autowired
	TaskRepository taskRepository;
	
	/**
	 * Loads the task with the given id.
	 * 
	 * @param id
	 *            the id of the task
	 * @return the task
	 */
	public Task loadTask(String id) {
		return taskRepository.findFirstById(id);
	}
	
	/**
	 * Loads a list of all tasks that are not completed yet.
	 * 
	 * @return a list of the tasks
	 */
	public List<Task> loadUncompletedTasks() {
		return taskRepository.findByCompleted(false);
	}
	
	/**
	 * Loads a list of all tasks that the parents are already
	 * informed about and that are not completed yet.
	 * 
	 * @return a list of the tasks
	 */
	public List<Task> loadActiveTasks() {
		return taskRepository.findByInfoDateAfterAndCompleted(Calendar.getInstance(), false);
	}
	
	/**
	 * Loads a list of all tasks assigned to the given parent.
	 * 
	 * @param parent
	 *            the user assigned to the tasks
	 * @return a list of the tasks
	 */
	public List<Task> loadTasksByParent(User parent) {
		return taskRepository.findByParent(parent);
	}
	
	/**
	 * Loads a list of all tasks assigned to the given parent that he
	 * is already informed about and that haven't been completed yet.
	 * 
	 * @param parent
	 *            the user assigned to the tasks
	 * @return a list of the tasks
	 */
	public List<Task> loadActiveTasksByParent(User parent) {
		return taskRepository.findByParentAndInfoDateAfterAndCompleted(parent, Calendar.getInstance(), false);
	}
	
	/**
	 * Loads a list of all tasks assigned to the given parent that
	 * have their deadline set to the given date.
	 * 
	 * @param parent
	 *            the user assigned to the tasks
	 * @param deadline
	 *            the deadline of the tasks
	 * @return a list of the tasks
	 */
	public List<Task> loadTasksByParentAndDeadline(User parent, Calendar deadline) {
		Calendar deadlineDate = deadline;
		deadlineDate.set(Calendar.MILLISECOND, 0);
		deadlineDate.set(Calendar.SECOND, 0);
		deadlineDate.set(Calendar.MINUTE, 0);
		deadlineDate.set(Calendar.HOUR_OF_DAY, 0);
		return taskRepository.findByParentAndDeadline(parent, deadlineDate);
	}
	
	/**
	 * Creates a new task and saves it in the DB.
	 * 
	 * @param parent
	 *            the parent assigned to the new task
	 * @param subject
	 *            the subject of the new task
	 * @param description
	 *            the description of the new task
	 * @param infoDate
	 *            the date the parent gets informed about the task
	 * @param deadlineFrom
	 *            the deadline of the new task
	 * @param deadlineTo
	 *             the deadline of the new task
	 * @return the new task
	 */
	public Task createTask(User parent, String subject, String description, Calendar infoDate, Calendar deadlineFrom, Calendar deadlineTo) {
		Task task = new Task();
		task.setParent(parent);
		task.setSubject(subject);
		task.setDescription(description);
		task.setDeadlineFrom(deadlineFrom);
		task.setDeadlineTo(deadlineTo);
		task.setInfoDate(infoDate);
		return saveTask(task);
	}
	
	/**
	 * Saves the given Task in the DB. Also adds CreationDate if the Task is new.
	 * 
	 * @param task
	 *            the task to save
	 * @return the saved task
	 */
	public Task saveTask(Task task) {
		if(task.isNew()) {
			task.setCreationDate(Calendar.getInstance());
		}
		return taskRepository.save(task);
	}
	
	/**
	 * Sets the given task to completed.
	 * 
	 * @param task
	 *            the completed task
	 * @return the completed task
	 */
	public Task completeTask(Task task) {
		task.setCompleted(true);
		return saveTask(task);
	}
	
}

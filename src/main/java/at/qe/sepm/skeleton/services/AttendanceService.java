package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.repositories.AttendanceRepository;
import at.qe.sepm.skeleton.repositories.DayPlanRepository;
import at.qe.sepm.skeleton.services.AuditService.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Service for accessing and manipulating {@link Attendance} data.
 * 
 */
@Component
@Scope("application")
public class AttendanceService {

    @Autowired
    private AttendanceRepository attendanceRepository;

    @Autowired
    private DayPlanRepository dayPlanRepository;

    @Autowired
    private DayPlanService dayPlanService;

    @Autowired
    private AuditService auditService;

    /**
     * Loads a single attendance identified by its date.
     *
     * @param id
     *            the attendance id to search for
     * @return the attendance with the given id
     */
    public Attendance loadAttendance(String id) {
        return attendanceRepository.findFirstById(id);
    }

    /**
     * Loads every Attendance from a specific child
     * 
     * @param child
     *            the child the attendances belong to
     * @return the attendances by child
     */
    public List<Attendance> loadAttendancesByChild(Child child) {
        return attendanceRepository.findByChild(child);
    }

    /**
     * Loads every Attendance from a specific child from a given Date.
     *
     * @param child
     *            the child the attendances belong to
     * @param date
     *            the from date to search for
     * @return the attendances by child
     */
    public List<Attendance> loadAttendancesByChildFromDate(Child child, Calendar date) {
        List<Attendance> attendances = attendanceRepository.findByChild(child);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        Iterator<Attendance> iter = attendances.iterator();
        while (iter.hasNext()) {
            Attendance a = iter.next();
            // System.out.println(a.getDayPlan().getDayDate().before(date));
            if (a.getDayPlan().getDayDate().before(date)) {
                iter.remove();
                attendances.remove(a);
            }
        }
        return attendances;
    }

    /**
     * Loads a list of all attendances of the given child in the specified
     * timespan.
     * 
     * @param child
     *            the child attending
     * @param from
     *            the start of the timespan
     * @param to
     *            the end of the timespan
     * @return the attendances
     */
    public List<Attendance> loadAttendancesByChildInTimespan(Child child, Calendar from, Calendar to) {
        List<Attendance> attendances = new ArrayList<>();
        List<DayPlan> dayPlans = dayPlanService.loadDayPlansInTimeSpan(from, to);
        for (DayPlan dayPlan : dayPlans) {
            attendances.addAll(attendanceRepository.findByChildAndDayPlan(child, dayPlan));
        }
        return attendances;
    }

    /**
     * Loads a list of all attendances of all children of the given parent in
     * the specified timespan.
     * 
     * @param parent
     *            the parent of the children attending
     * @param from
     *            the start of the timespan
     * @param to
     *            the end of the timespan
     * @return the attendances
     */
    public List<Attendance> loadAttendancesOfChildrenByParentInTimespan(User parent, Calendar from, Calendar to) {
        List<Attendance> attendances = new ArrayList<>();
        List<DayPlan> dayPlans = dayPlanService.loadDayPlansInTimeSpan(from, to);
        for (DayPlan dayPlan : dayPlans) {
            for (Child child : parent.getChildren()) {
                attendances.addAll(attendanceRepository.findByChildAndDayPlan(child, dayPlan));
            }
        }
        return attendances;
    }

    /**
     * Loads the first Attendance from a specific child at a given Date.
     *
     * @param child
     *            the child the attendances belong to
     * @param date
     *            the date to search for
     * @return the first attendances by child and date
     */
    public Attendance loadAttendanceByChildAndDate(Child child, Calendar date) {
        return attendanceRepository.findFirstByChildAndDayPlan(child, dayPlanRepository.findFirstByDayDate(date));
    }

    /**
     * Loads every Attendance from a specific child at a given Date.
     *
     * @param child
     *            the child the attendances belong to
     * @param date
     *            the date to search for
     * @return the attendances by child and date
     */
    public List<Attendance> loadAttendancesByChildAndDate(Child child, Calendar date) {
        return attendanceRepository.findByChildAndDayPlan(child, dayPlanRepository.findFirstByDayDate(date));
    }

    /**
     * Loads a collection of all attendances.
     *
     * @return a collection of all attendances.
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    public Collection<Attendance> loadAllAttendances() {
        return attendanceRepository.findAll();
    }

    /**
     * Returns all Attendances of all Children of a given Parent.
     *
     * @param parent
     *            the Parent of the children you want the Attendances of
     * @return List of all the Attendances of all Children of the Parent
     */
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('EMPLOYEE') or principal.username eq #parent.email")
    public List<Attendance> loadAttendancesOfChildrenByParent(@Param("parent") User parent) {
        List<Attendance> attendances = new ArrayList<Attendance>();
        for (Child c : parent.getChildren())
            attendances.addAll(loadAttendancesByChild(c));
        return attendances;
    }

    /**
     * Returns all Attendances of all Children of a given Parent from a given
     * Date.
     *
     * @param parent
     *            the Parent of the children you want the Attendances of
     * @param date
     *            the from date to search for
     * @return List of all the Attendances of all Children of the Parent
     */
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('EMPLOYEE') or principal.username eq #parent.email")
    public List<Attendance> loadAttendancesOfChildrenByParentFromDate(@Param("parent") User parent, Calendar date) {
        List<Attendance> attendances = new ArrayList<Attendance>();
        for (Child c : parent.getChildren())
            attendances.addAll(loadAttendancesByChildFromDate(c, date));
        Collections.sort(attendances, sortByDate);
        return attendances;
    }

    /**
     * Returns all Attendances of all Children of a given Parent at a given
     * Date.
     *
     * @param parent
     *            the Parent of the children you want the Attendances of
     * @param date
     *            the date to search for
     * @return List of all the Attendances of all Children of the Parent
     */
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('EMPLOYEE') or principal.username eq #parent.email")
    public List<Attendance> loadAttendancesOfChildrenByParentAndDate(@Param("parent") User parent, Calendar date) {
        List<Attendance> attendances = new ArrayList<Attendance>();
        for (Child c : parent.getChildren())
            if (loadAttendanceByChildAndDate(c, date) != null) {
                attendances.add(loadAttendanceByChildAndDate(c, date));
            }
        return attendances;
    }

    /**
     * Loads a list of all attendances containing the given pick-up person.
     * 
     * @param pickUpPerson
     *            the pick-up person searched for
     * @return the list of attendances
     */
    public List<Attendance> loadAttendancesByPickUpPerson(Person pickUpPerson) {
        return attendanceRepository.findByPickUpPerson(pickUpPerson);
    }

    /**
     * Saves the Attendance. This method will also set
     * {@link Attendance#creationDate} for new entities.
     *
     * @param attendance
     *            the attendance to save
     * @return the updated attendance
     */
    public Attendance saveAttendance(Attendance attendance) {
        if (attendance.isNew()) {
            attendance.setCreationDate(Calendar.getInstance());
            logAttendance(Action.Create, attendance);
        } else {
            logAttendance(Action.Modify, attendance);
        }

        return attendanceRepository.save(attendance);
    }

    /**
     * Deletes the attendance.
     *
     * @param attendance
     *            the attendance to delete
     */
    public void deleteAttendance(Attendance attendance) {
        attendance.getDayPlan().removeAttendance(attendance);
        if (attendance.getLunch() != null) {
            attendance.getLunch().removeConsumer(attendance);
        }
        logAttendance(Action.Delete, attendance);
        attendanceRepository.delete(attendance);
    }

    /**
     * Logs the state of an attendance object.
     *
     * @param action
     *            Action performed
     * @param attendance
     *            Object instance
     */
    private void logAttendance(AuditService.Action action, Attendance attendance) {
        try {
            StringBuilder str = new StringBuilder();
            str.append("Attendance" + "[" + attendance.toString() + "]:\n");

            if (attendance.getChild() != null)
                str.append("\tChild[" + attendance.getChild().toString() + "]\n");

            if (attendance.getLunch() != null)
                str.append("\tLunch[" + attendance.getLunch().toString() + "]\n");

            if (attendance.getPickUpPerson() != null)
                str.append("\tPickUpPerson[" + attendance.getPickUpPerson().toString() + "]\n");

            if (attendance.getArrivalTime() != null)
                str.append("\tArrivalTime[" + attendance.getArrivalTime().toString() + "]\n");

            if (attendance.getDepartureTime() != null)
                str.append("\tDepartureTime[" + attendance.getDepartureTime().toString() + "]\n");

            auditService.log(action, str.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sorts attendances by date in ascending order
     *
     */
    private Comparator<Attendance> sortByDate = new Comparator<Attendance>() {
        @Override
        public int compare(Attendance o1, Attendance o2) {
            return o1.getDayPlan().getId().compareTo(o2.getDayPlan().getId());
        }
    };

}

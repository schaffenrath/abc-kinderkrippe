package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.Main;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Service for accessing and managing the AuditLogger.
 *
 */
@Component
@Scope("application")
public class AuditService {

	/**
	 * Defines the action type of the message.
	 * 
	 */
	public enum Action {
		Create, Delete, Modify, Info, Warning
	};

	/**
	 * Defines an audit message.
	 * 
	 */
	public class AuditMessage {
		public Action action;
		public String message;
		public Date time;

		public AuditMessage(Action action, String message) {
			this.action = action;
			this.message = message;
			this.time = new Date();
		}
	}

	/**
	 * Audit path
	 *
	 */
	private String path = "src/main/resources/audit.log";

	/**
	 * List that contains all current audit messages.
	 * 
	 */
	private List<AuditMessage> messages;

	private Logger logger;

	@PostConstruct
	public void init() {
		messages = new ArrayList<AuditMessage>();
		logger = Logger.getLogger(Main.class.getName());
		this.log(Action.Info, "---- Audit Service Startup ----");
		logger.info("Audit Service Startup");
	}

	/**
	 * Saves the current audit messages to file.
	 * 
	 */
	private void save() {
		synchronized (this) {
			try (FileWriter fw = new FileWriter(path, true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw)) {
				for (AuditMessage message : messages) {
					StringBuilder builder = new StringBuilder();
					builder.append("[" + message.time.toString() + "]");
					builder.append("[" + message.action.toString() + "]: ");
					builder.append(message.message);

					out.println(builder.toString());
				}
			} catch (IOException e) {
				logger.info("ERROR: " + e.getMessage());
			}

			messages.clear();
		}
	}

	/**
	 * Logs an audit message.
	 * 
	 * @param action
	 *            the action to log
	 * @param message
	 *            the message to log
	 */
	public void log(AuditService.Action action, String message) {

		synchronized (this) {
			messages.add(new AuditMessage(action, message));
		}

		this.save();
	}
}

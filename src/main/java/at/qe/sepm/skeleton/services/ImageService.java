package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.Main;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.http.MediaType;
import java.io.FileInputStream;

/**
 * Service for accessing and saving images on the file system.
 *
 */
@Component
@Scope("application")
public class ImageService {

    @Autowired
    ServletContext context;

    private Logger log = Logger.getLogger(Main.class.getName());

    private String path_image_default;
    private String path_images_child;
    private String path_images_parent;

    private String default_image;
    private String child_images;
    private String parent_images;
    private String caregiver_images;

    @PostConstruct
    public void init() {
        default_image = "src/main/images/default.jpg";
        child_images = "src/main/images/child/";
        parent_images = "src/main/images/parent/";
        caregiver_images = "src/main/images/caregiver/";
    }

    public void sendImageChild(HttpServletResponse response, String id) throws IOException {
        sendImage(response, child_images + id + ".jpg");
    }

    public void sendImageParent(HttpServletResponse response, String id) throws IOException {
        sendImage(response, parent_images + id + ".jpg");
    }

    public void sendImageCaregiver(HttpServletResponse response, String id) throws IOException {
        sendImage(response, caregiver_images + id + ".jpg");
    }

    /**
     * Sends file , if there is no file, return default image.
     * 
     * @param response
     * @throws IOException
     */
    public void sendImage(HttpServletResponse response, String path) throws IOException {
        InputStream in = null;

        try {

            in = new FileInputStream(path);
            log.info(String.valueOf(in));
        } catch (Exception e) {
        } // ignore it

        // load default image if it does not exist
        if (in == null) {
            try {
                in = new FileInputStream(default_image);
                log.info(String.valueOf(in));
            } catch (Exception e) {
            	response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
        }

        // send image
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        IOUtils.copy(in, response.getOutputStream());
    }

    /**
     * Sends image of parent.
     * 
     * @param response
     * @param id
     * @throws IOException
     */
    public void sendProfilimageParent(HttpServletResponse response, String id) throws IOException {
        InputStream in = null;

        try {
            in = new FileInputStream(path_images_parent + id + ".jpg");
        } 
        catch (Exception e) {
        } // ignore it

        // load default image if it does not exist
        if (in == null) {
            try {
                in = new FileInputStream(path_image_default);
                log.info(String.valueOf(in));
            } catch (Exception e) {
            	response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
        }

        // send image
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        IOUtils.copy(in, response.getOutputStream());
    }

    /**
     * Saves the picture into the child folder.
     * 
     * @param stream
     *            input stream of the image
     * @param id
     *            id of the child
     */
    public void saveImageChild(InputStream stream, String id) throws FileNotFoundException, IOException {
        new File(child_images + id + ".jpg");
        OutputStream out = new FileOutputStream(child_images + id + ".jpg");
        IOUtils.copy(stream, out);
        stream.close();
        out.close();
    }

    /**
     * Saves the picture into the parent folder.
     *
     * @param stream
     *            input stream of the image
     * @param id
     *            id of the parent
     */
    public void saveImageParent(InputStream stream, String id) throws FileNotFoundException, IOException {
        new File(parent_images + id + ".jpg");
        OutputStream out = new FileOutputStream(parent_images + id + ".jpg");
        IOUtils.copy(stream, out);
        stream.close();
        out.close();
    }

    /**
     * Saves the picture into the caregiver folder.
     *
     * @param stream
     *            input stream of the image
     * @param id
     *            id of the caregiver
     */
    public void saveImageCaregiver(InputStream stream, String id) throws FileNotFoundException, IOException {
        new File(caregiver_images + id + ".jpg");
        OutputStream out = new FileOutputStream(caregiver_images + id + ".jpg");
        IOUtils.copy(stream, out);
        stream.close();
        out.close();
    }
}

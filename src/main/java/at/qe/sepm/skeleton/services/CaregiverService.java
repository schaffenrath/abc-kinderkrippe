package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Caregiver;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.repositories.CaregiverRepository;
import at.qe.sepm.skeleton.repositories.ChildRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Collection;

/**
 * Service for accessing and manipulating {@link Caregiver} data.
 * 
 */
@Component
@Scope("application")
public class CaregiverService {

	@Autowired
	private CaregiverRepository caregiverRepository;
	
	@Autowired
	private ChildRepository childRepository;

	@Autowired
	private AuditService auditService;

	/**
	 * Loads a single caregiver identified by its id.
	 *
	 * @param id
	 *            the id to search for
	 * @return the child with the given id
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('PARENT')")
	public Caregiver loadCaregiver(String id) {
		return caregiverRepository.findFirstById(id);
	}

	/**
	 * Returns a collection of all caregivers.
	 *
	 * @return a collection of all caregivers.
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Collection<Caregiver> loadAllCaregivers() {
		return caregiverRepository.findAll();
	}

	/**
	 * Returns a collection of all confirmed caregivers.
	 *
	 * @return a collection of all confirmed caregivers.
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Collection<Caregiver> loadAllConfirmedCaregivers() {
		return caregiverRepository.findConfirmed();
	}

	/**
	 * Returns a collection of all unconfirmed caregivers.
	 *
	 * @return a collection of all unconfirmed caregivers.
	 */
	public Collection<Caregiver> loadAllUnconfirmedCaregivers() {
		return caregiverRepository.findUnconfirmed();
	}

	/**
	 * Saves the caregiver. This method will also set
	 * {@link Caregiver#creationDate} for new.
	 *
	 * @param caregiver
	 *            the caregiver to save
	 * @return the updated caregiver
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('PARENT')")
	public Caregiver saveCaregiver(Caregiver caregiver) {
		if (caregiver.isNew()) {
			caregiver.setCreationDate(Calendar.getInstance());
			logCaregiver(AuditService.Action.Create, caregiver);
		} else {
			logCaregiver(AuditService.Action.Modify, caregiver);
		}

		return caregiverRepository.save(caregiver);
	}

	/**
     * Deletes the caregiver.
     *
     * @param caregiver
	 *            the caregiver to delete
     */
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('PARENT')")
    public void deleteCaregiver(Caregiver caregiver) {
    	for(Child relatedChild : childRepository.findByCaregiver(caregiver)) {
    		relatedChild.removeCaregiver(caregiver);
    		childRepository.save(relatedChild);
    	}
    	for(Child relatedChild : childRepository.findByEmergencyContact(caregiver)) {
    		relatedChild.setEmergencyContact(null);
    		childRepository.save(relatedChild);
    	}
    	logCaregiver(AuditService.Action.Delete, caregiver);
        caregiverRepository.delete(caregiver);
    }

	/**
	 * Method logs the state of an caregiver object.
	 * 
	 * @param caregiver
	 */
	private void logCaregiver(AuditService.Action action, Caregiver caregiver) {
		try {
			StringBuilder str = new StringBuilder();
			str.append("Caregiver" + "[" + caregiver.toString() + "]:\n");

			if (caregiver.getFirstName() != null)
				str.append("\tFirstname: " + caregiver.getFirstName() + "\n");

			if (caregiver.getLastName() != null)
				str.append("\tLastname: " + caregiver.getLastName() + "\n");

			if (caregiver.getEmail() != null)
				str.append("\tEmail: " + caregiver.getEmail() + "\n");

			if (caregiver.getChildren() != null)
				for (Child child : caregiver.getChildren())
					str.append("\tChild[" + child.getId().toString() + "]\n");

			auditService.log(action, str.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

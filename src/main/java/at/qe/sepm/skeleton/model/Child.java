package at.qe.sepm.skeleton.model;

import java.util.*;

import javax.persistence.*;

import org.springframework.data.domain.Persistable;

/**
 * Entity representing children.
 * 
 */
@Entity
public class Child implements Persistable<String> {

	private static final long serialVersionUID = 1L;

	@Id
	private final String id;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar creationDate;

	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String lastName;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Gender gender;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Calendar birthdate;

	private boolean[] attendanceDays;

	private String remarks;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Calendar registrationDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar deregistrationDate;

	private String importantInformation;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "CHILDREN_CAREGIVER",
			joinColumns = @JoinColumn(name = "CHILD_ID", referencedColumnName = "ID"),
			inverseJoinColumns = @JoinColumn(name = "CAREGIVER_ID", referencedColumnName = "ID"))
	private Set<Caregiver> caregivers;

	@ManyToOne
	@JoinColumn(name = "EMERGENCY_CONTACT_ID", referencedColumnName = "ID")
	private Person emergencyContact;

	@ManyToMany(mappedBy = "children", fetch = FetchType.EAGER)
	private Set<User> parents;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "CHILD_INTERNAL_SIBLINGS",
			joinColumns = @JoinColumn(name = "CHILD_ID", referencedColumnName = "ID"),
			inverseJoinColumns = @JoinColumn(name = "SIBLING_ID", referencedColumnName = "ID"))
	private Set<Child> internalSiblings;

	@ManyToMany(mappedBy = "internalSiblings", fetch = FetchType.EAGER)
	private Set<Child> internalSiblingsOf;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "CHILD_EXTERNAL_SIBLINGS",
			joinColumns = @JoinColumn(name = "CHILD_ID", referencedColumnName = "ID"),
			inverseJoinColumns = @JoinColumn(name = "SIBLING_ID", referencedColumnName = "ID"))
	private Set<Sibling> externalSiblings;

	boolean enabled;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "child")
	private Set<Attendance> attendances;

	public Child() {
		this.parents = new HashSet<>();
		this.caregivers = new HashSet<>();
		this.internalSiblings = new HashSet<>();
		this.internalSiblingsOf = new HashSet<>();
		this.externalSiblings = new HashSet<>();
		this.attendances = new HashSet<>();
		this.id = UUID.randomUUID().toString();
		this.enabled = true;
		this.attendanceDays = new boolean[7];
		Arrays.fill(attendanceDays, false);
	}

	public Child(String firstName, String lastName, Gender gender, Calendar birthdate, Calendar registrationDate) {
		this();
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.birthdate = birthdate;
		this.registrationDate = registrationDate;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public boolean isNew() {
		return (null == creationDate);
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Calendar getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Calendar birthdate) {
		this.birthdate = birthdate;
	}

	public boolean[] getAttendanceDays() {
		return attendanceDays;
	}

	public void setAttendanceDays(boolean[] attendanceDays) {
		this.attendanceDays = attendanceDays;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Calendar getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Calendar registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Calendar getDeregistrationDate() {
		return deregistrationDate;
	}

	public void setDeregistrationDate(Calendar deregistrationDate) {
		this.deregistrationDate = deregistrationDate;
	}

	public String getImportantInformation() {
		return importantInformation;
	}

	public void setImportantInformation(String importantInformation) {
		this.importantInformation = importantInformation;
	}

	public Set<Caregiver> getCaregivers() {
		return caregivers;
	}

	public void addCaregiver(Caregiver caregiver) {
		caregivers.add(caregiver);
	}

	public void removeCaregiver(Caregiver caregiver) {
		caregivers.remove(caregiver);
	}

	public Person getEmergencyContact() {
		return emergencyContact;
	}

	public void setEmergencyContact(Person emergencyContact) {
		this.emergencyContact = emergencyContact;
	}

	public Set<User> getParents() {
		return parents;
	}

	public void addParent(User parent) {
		this.parents.add(parent);
	}

	public void removeParent(User parent) {
		this.parents.remove(parent);
	}

	public Set<Child> getInternalSiblings() {
		return internalSiblings;
	}

	public void addInternalSibling(Child internalSibling) {
		this.internalSiblings.add(internalSibling);
	}

	public void addAllInternalSibling(Set<Child> internalSiblings) {
		this.internalSiblings.addAll(internalSiblings);
	}

	public void removeInternalSibling(Child internalSibling) {
		this.internalSiblings.remove(internalSibling);
	}

	public void removeAllInternalSibling(Set<Child> internalSiblings) {
		this.internalSiblings.removeAll(internalSiblings);
	}

	public Set<Sibling> getExternalSiblings() {
		return externalSiblings;
	}

	public void addExternalSibling(Sibling externalSibling) {
		this.externalSiblings.add(externalSibling);
	}

	public void addAllExternalSibling(Set<Sibling> externalSiblings) {
		this.externalSiblings.addAll(externalSiblings);
	}

	public void removeExternalSibling(Sibling externalSibling) {
		this.externalSiblings.remove(externalSibling);
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Set<Attendance> getAttendances() {
		if(attendances == null) {
			attendances = new HashSet<>();
		}
		return attendances;
	}

	public void addAttendance(Attendance attendance) {
		if(attendances == null) {
			attendances = new HashSet<>();
		}
		this.attendances.add(attendance);
	}

	public void removeAttendance(Attendance attendance) {
		if(attendances == null) {
			this.attendances = new HashSet<>();
		} else {
			this.attendances.remove(attendance);
		}
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 59 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Child)) {
			return false;
		}
		final Child other = (Child) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "at.qe.sepm.skeleton.model.Child[ id='" + this.id + "' name='"
				+ this.firstName + " " + this.lastName + "']";
	}

}

package at.qe.sepm.skeleton.model;

import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.domain.Persistable;

/**
 * Entity representing external siblings.
 * 
 */
@Entity
public class Sibling implements Persistable<String> {

	private static final long serialVersionUID = 1L;

	@Id
	private final String id;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar creationDate;

	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String lastName;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Calendar birthdate;

	@ManyToMany(mappedBy = "externalSiblings", fetch = FetchType.EAGER)
	private Set<Child> siblingOf;

	public Sibling() {
		this.siblingOf = new HashSet<>();
		this.id = UUID.randomUUID().toString();
	}

	public Sibling(String firstName, String lastName, Calendar birthdate) {
		this();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthdate = birthdate;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public boolean isNew() {
		return (null == creationDate);
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Calendar getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Calendar birthdate) {
		this.birthdate = birthdate;
	}

	public Set<Child> getSiblingOf() {
		return siblingOf;
	}

	public void addSiblingOf(Child child) {
		this.siblingOf.add(child);
	}

	public void addAllSiblingOf(Set<Child> children) {
		this.siblingOf.addAll(children);
	}

	public void removeSiblingOf(Child child) {
		this.siblingOf.remove(child);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 59 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Sibling)) {
			return false;
		}
		final Sibling other = (Sibling) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "at.qe.sepm.skeleton.model.Sibling[ id='" + this.id + "' name='" + this.firstName
				+ " " + this.lastName + "']";
	}

}
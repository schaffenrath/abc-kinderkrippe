package at.qe.sepm.skeleton.model;

/**
 * Enumeration of available genders.
 * 
 */
public enum Gender {
	MALE, FEMALE, OTHER
}

package at.qe.sepm.skeleton.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

/**
 * Entity representing caregivers.
 * 
 */
@Entity
@Table(name = "PERSON")
@DiscriminatorValue("C")
public class Caregiver extends Person {

    private static final long serialVersionUID = 1L;

    @Column
    boolean confirmed;

    @ManyToMany(mappedBy = "caregivers", fetch = FetchType.EAGER)
    private Set<Child> children;

    public Caregiver() {
        super();
        this.confirmed = false;
        this.children = new HashSet<>();
    }

    public Caregiver(String firstName, String lastName) {
        super(firstName, lastName);
        this.confirmed = false;
        this.children = new HashSet<>();
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Set<Child> getChildren() {
        return this.children;
    }

    public void setChildren(Set<Child> children) {
        this.children = children;
    }

    public void addChild(Child child) {
        this.children.add(child);
    }

    public void addAllChildren(Set<Child> children) {
        this.children.addAll(children);
    }

    public void removeChild(Child child) {
        this.children.remove(child);
    }

    public void removeAllChildren(Set<Child> children) {
        this.children.removeAll(children);
    }

    @Override
    public String toString() {
        return "at.qe.sepm.skeleton.model.Caregiver[ id='" + this.getId() + "' name='" + super.getFirstName()
                + " " + super.getLastName() + "']";
    }

}

package at.qe.sepm.skeleton.model;

import java.util.Calendar;
import java.util.Objects;
import javax.persistence.*;
import org.springframework.data.domain.Persistable;

/**
 * Entity representing messages and notifications.
 * 
 */
@Entity
public class Message implements Persistable<Long> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar creationDate;

	@Column(nullable = false)
	private String subject;

	@Column(nullable = false)
	private String content;

	@Column(nullable = false)
	private boolean hideMessage;

	@Column(nullable = false)
	private boolean employeeMessage;

	@Column(nullable = false)
	private boolean publicMessage;

    @Column(nullable = false)
    private boolean confirmCaregiverMessage;

    @Column
    private String additionalId;

    @ManyToOne
    @JoinColumn(name = "SENDER_ID")
    private User sender;

	@ManyToOne
	@JoinColumn(name = "RECIPIENT_ID")
	private User recipient;

	public Message() {
	}

    public Message(String subject, String content, Boolean hideMessage, Boolean employeeMessage, Boolean publicMessage) {
        this();
        this.subject = subject;
        this.content = content;
        this.hideMessage = hideMessage;
        this.employeeMessage = employeeMessage;
        this.publicMessage = publicMessage;
        this.confirmCaregiverMessage = false;
        this.additionalId = null;
    }

    public Message(String subject, String content, Boolean hideMessage, Boolean employeeMessage, Boolean publicMessage, User sender, User recipient) {
        this(subject, content, hideMessage, employeeMessage, publicMessage);
        this.sender = sender;
        this.recipient = recipient;
    }

    @Override
    public Long getId() {
        return id;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isHideMessage() {
        return hideMessage;
    }

    public void setHideMessage(boolean hideMessage) {
        this.hideMessage = hideMessage;
    }

    public boolean isEmployeeMessage() {
        return employeeMessage;
    }

    public void setEmployeeMessage(boolean employeeMessage) {
        this.employeeMessage = employeeMessage;
    }

    public boolean isPublicMessage() {
        return publicMessage;
    }

    public void setPublicMessage(boolean publicMessage) {
        this.publicMessage = publicMessage;
    }

    public boolean isConfirmCaregiverMessage() {
        return confirmCaregiverMessage;
    }

    public void setConfirmCaregiverMessage(boolean confirmCaregiverMessage) {
        this.confirmCaregiverMessage = confirmCaregiverMessage;
    }

    public String getAdditionalId() {
        return additionalId;
    }

    public void setAdditionalId(String additionalId) {
        this.additionalId = additionalId;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    @Override
    public boolean isNew() {
        return (null == creationDate);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Message)) {
            return false;
        }
        final Message other = (Message) obj;
        if (!Objects.equals(this.getId(), other.getId())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "at.qe.sepm.skeleton.model.Message[ id='" + this.getId() + "']";
    }

}

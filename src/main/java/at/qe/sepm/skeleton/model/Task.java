package at.qe.sepm.skeleton.model;

import java.util.Calendar;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.domain.Persistable;

/**
 * Abstract entity representing tasks.
 * 
 */
@Entity
public class Task implements Persistable<String> {

	private static final long serialVersionUID = 1L;

	@Id
	private final String id;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar creationDate;

	@ManyToOne(optional = false)
	@JoinColumn(name = "PARENT_ID")
	private User parent;

	private String subject;
	private String description;

	@Temporal(TemporalType.DATE)
	private Calendar deadlineFrom;

	@Temporal(TemporalType.DATE)
	private Calendar deadlineTo;

	@Temporal(TemporalType.DATE)
	private Calendar infoDate;

	private boolean completed;

	public Task() {
		this.id = UUID.randomUUID().toString();
		this.completed = false;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public boolean isNew() {
		return creationDate == null;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public User getParent() {
		return parent;
	}

	public void setParent(User parent) {
		this.parent = parent;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Calendar getDeadlineFrom() {
		return deadlineFrom;
	}

	public void setDeadlineFrom(Calendar deadlineFrom) {
		this.deadlineFrom = deadlineFrom;
	}

	public Calendar getDeadlineTo() {
		return deadlineTo;
	}

	public void setDeadlineTo(Calendar deadlineTo) {
		this.deadlineTo = deadlineTo;
	}

	public Calendar getInfoDate() {
		return infoDate;
	}

	public void setInfoDate(Calendar infoDate) {
		this.infoDate = infoDate;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 59 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Task)) {
			return false;
		}
		final Task other = (Task) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "at.qe.sepm.skeleton.model.Task[ id='" + this.id + "']";
	}

}

package at.qe.sepm.skeleton.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity representing users.
 * 
 */
@Entity
@Table(name = "Person")
@DiscriminatorValue("U")
public class User extends Person {

	private static final long serialVersionUID = 1L;

	@Column(length = 100)
	private String password;

	boolean enabled;

	@ElementCollection(targetClass = UserRole.class, fetch = FetchType.EAGER)
	@CollectionTable(name = "User_UserRole")
	@Enumerated(EnumType.STRING)
	private Set<UserRole> roles;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "parent")
	private Set<Task> tasks;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "PARENTS",
			joinColumns = @JoinColumn(name = "PARENT_ID", referencedColumnName = "ID"),
			inverseJoinColumns = @JoinColumn(name = "CHILD_ID", referencedColumnName = "ID"))
	private Set<Child> children;

	public User() {
		super();
		this.children = new HashSet<>();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Set<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<UserRole> roles) {
		this.roles = roles;
	}

	public Set<Task> getTasks() {
		return tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

	public Set<Child> getChildren() {
		return children;
	}

	public void setChildren(Set<Child> children) {
		this.children = children;
	}

	public void addChild(Child child) {
		this.children.add(child);
		enabled = true;
	}

	public void addAllChildren(Set<Child> children) {
		this.children.addAll(children);
	}

	public void removeChild(Child child) {
		this.children.remove(child);
		if(children.size() == 0) {
		    enabled = false;
		}
	}

	@Override
	public String toString() {
		return "at.qe.sepm.skeleton.model.User[ id='" + getId() + "']";
	}

}

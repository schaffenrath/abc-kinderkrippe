package at.qe.sepm.skeleton.model;

import java.util.UUID;
import java.util.Calendar;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.domain.Persistable;

/**
 * Entity representing attendances of children on certain days.
 * 
 */
@Entity
public class Attendance implements Persistable<String> {

	private static final long serialVersionUID = 1L;

	@Id
	private final String id;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar creationDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar arrivalTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar departureTime;

	@ManyToOne(optional = false)
	@JoinColumn(name = "DAY_PLAN_DATE")
	private DayPlan dayPlan;

	@ManyToOne(optional = false)
	@JoinColumn(name = "CHILD_ID")
	private Child child;

	@ManyToOne
	@JoinColumn(name = "LUNCH_ID")
	private Lunch lunch;

	@ManyToOne
	@JoinColumn(name = "PERSON_ID")
	private Person pickUpPerson;

	@Column
	private boolean attending;

	public Attendance() {
		this.id = UUID.randomUUID().toString();
		attending = true;
	}

	public Attendance(DayPlan dayPlan, Child child) {
		this();
		this.dayPlan = dayPlan;
		this.child = child;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public boolean isNew() {
		return creationDate == null;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public Calendar getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Calendar arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public Calendar getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Calendar departureTime) {
		this.departureTime = departureTime;
	}

	public DayPlan getDayPlan() {
		return dayPlan;
	}

	public void setDayPlan(DayPlan dayPlan) {
		this.dayPlan = dayPlan;
	}

	public Child getChild() {
		return child;
	}

	public void setChild(Child child) {
		this.child = child;
	}

	public Lunch getLunch() {
		return lunch;
	}

	public void setLunch(Lunch lunch) {
		this.lunch = lunch;
	}

	public Person getPickUpPerson() {
		return pickUpPerson;
	}

	public void setPickUpPerson(Person pickUpPerson) {
		this.pickUpPerson = pickUpPerson;
	}

	public boolean isAttending() {
		return attending;
	}

	public void setAttending(boolean attending) {
		this.attending = attending;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 59 * hash + Objects.hashCode(this.getId());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Attendance)) {
			return false;
		}
		final Attendance other = (Attendance) obj;
		if (!Objects.equals(this.getId(), other.getId())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "at.qe.sepm.skeleton.model.Attendance[ id='" + this.getId() + "']";
	}

}

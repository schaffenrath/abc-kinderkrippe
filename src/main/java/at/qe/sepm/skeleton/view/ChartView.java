/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.view;

import at.qe.sepm.skeleton.model.MonthPlan;
import at.qe.sepm.skeleton.ui.controllers.DayPlanDetailController;
import at.qe.sepm.skeleton.ui.controllers.DayPlanListController;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * TODO: add doc
 */
@Component
@Scope("view")
public class ChartView {
    
    @Autowired 
    private DayPlanListController dayPlanController;
    
    @Autowired
    private DayPlanDetailController yearController;

    public ChartView() {
    }
    
    
    public HorizontalBarChartModel getAttendancesBar(){
        HorizontalBarChartModel childAttendanceBar = new HorizontalBarChartModel();
        ChartSeries attendance = new ChartSeries();
        
        int max = 0;
        
        attendance.setLabel("Angemeldete Kinder");
        for(MonthPlan month : dayPlanController.getMonthPlans(yearController.getYear())){
            attendance.set(month.month, month.ovrOccupancy);
            if(month.ovrOccupancy > max) max = month.ovrOccupancy;
        }
        
        childAttendanceBar.setTitle("Anwesenheit pro Monat");
        childAttendanceBar.setAnimate(true);
        childAttendanceBar.setLegendPosition("ne");        
        childAttendanceBar.addSeries(attendance);
        childAttendanceBar.setBarWidth(30);
        childAttendanceBar.setShadow(true);
        childAttendanceBar.setSeriesColors("5BC0EB,FA7921,60495A,9BC53D,E55934,FDE74C,2B2D42");
        childAttendanceBar.setMouseoverHighlight(true);
        Axis xAxis = childAttendanceBar.getAxis(AxisType.X);
        xAxis.setTickFormat("%d");  
        xAxis.setTickCount(max+2);
        xAxis.setMax(max+1);
        
        return childAttendanceBar;
    }
    
    public HorizontalBarChartModel getAttendancePercentageBar(){
        HorizontalBarChartModel childAttendanceBar = new HorizontalBarChartModel();
        ChartSeries percent = new ChartSeries();

        percent.setLabel("Durchschnitt");
        for(MonthPlan month : dayPlanController.getMonthPlans(yearController.getYear())){
            percent.set(month.month, month.avrOccupancy*100);
        }
        
        childAttendanceBar.setTitle("Anwesenheitsverteilung");
        childAttendanceBar.setAnimate(true);
        childAttendanceBar.setLegendPosition("ne");        
        childAttendanceBar.addSeries(percent);
        childAttendanceBar.setBarWidth(30);
        childAttendanceBar.setShadow(true);
        childAttendanceBar.setSeriesColors("2B2D42,FDE74C,E55934,9BC53D,60495A,FA7921,5BC0EB");
        childAttendanceBar.setMouseoverHighlight(true);
        Axis xAxis = childAttendanceBar.getAxis(AxisType.X);
        xAxis.setTickFormat("%d%");
        xAxis.setMax(100);
        return childAttendanceBar;
    }
    
    
}

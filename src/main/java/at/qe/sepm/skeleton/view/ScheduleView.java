package at.qe.sepm.skeleton.view;

import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.model.Lunch;
import at.qe.sepm.skeleton.services.DayPlanService;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Special view for the primefaces schedule in the dayplan-calendar site.
 * This view creates all events for the schedule and handels the menu selection
 * for the menu-dialog on dayplan-calendar.
 *
 */

@Component
@Scope("view")
public class ScheduleView implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Autowired
    private DayPlanService dayPlanService;
    
    private ScheduleModel eventModel;
    
    private ScheduleEvent event = new DefaultScheduleEvent();
    
    private String selectMenu;
    
    private Calendar id;
    
    /**
     * This method is called when the dayplan-calendar site is generated. It
     * creates all events which show the attendancies, lunches and arrival- and
     * departure times. 
     * 
     */
    public void init() {        
        Collection<DayPlan> allDayPlans = dayPlanService.loadAllDayPlans();
        if(eventModel != null) return;
        eventModel = new DefaultScheduleModel();
        StringBuilder dayMenu;
        
        //iterate over every dayplan to create a EventModel for it
        for(DayPlan currentDay : allDayPlans){
            //create EventModel for number of attending childen
            if(currentDay.getAttendances() == null) eventModel.addEvent(new DefaultScheduleEvent("Anm.: 0", currentDay.getId().getTime(), currentDay.getId().getTime(), currentDay));
            else eventModel.addEvent(new DefaultScheduleEvent("Anm.: "+currentDay.getAttendances().size(), currentDay.getId().getTime(), currentDay.getId().getTime(), currentDay));
            
            //bild string with available menus
            dayMenu = new StringBuilder();
            Collection<Lunch> allLunches = currentDay.getLunches();
            for(Lunch currentLunch : allLunches)
                dayMenu.append(currentLunch.getMenu()).append(" ");
            
            //create EventModel for available menus
            Calendar lunchTimeStart = currentDay.getId();
            lunchTimeStart.set(Calendar.HOUR_OF_DAY, 12);
            eventModel.addEvent(new DefaultScheduleEvent("Menü: " + dayMenu, lunchTimeStart.getTime(), lunchTimeStart.getTime(), currentDay));
            
            //create EventModel for arrival/departure times
            if(currentDay.getEarliestArrivalDate() != null)
            {                
                eventModel.addEvent(new DefaultScheduleEvent("Bringzeit", 
                        currentDay.getEarliestArrivalDate().getTime(), currentDay.getLatestArrivalDate().getTime(), currentDay));
                eventModel.addEvent(new DefaultScheduleEvent("Abholzeit", 
                        currentDay.getEarliestDepartureDate().getTime(), currentDay.getLatestDepartureDate().getTime(), currentDay));
            }
        }
    }
    
    /**
     * Gets the EventModel, which was created once
     * 
     * @return eventModel for schedule
     */
    public ScheduleModel getEventModel() {
        init();
        return eventModel;
    }
    
    
    public ScheduleEvent getEvent() {
        return event;
    }
    
    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }
    
    /**
     * Methods is called when arrival/departure values are changed
     *  
     */
    public void changeValue() {
        init();
    }
    
    public Date getCurrentDay(){        
        return Calendar.getInstance().getTime();
    }   

    /**
     * Get value 0,1,2,3 as string, which indicates the selected menu in the 
     * edit dialog in dayplan-calendar
     * 
     * @return 0,1,2,3 as string
     */
    public String getSelectMenu() {
        return selectMenu;
    }

    /**
     * Resets the selected menupoint to default
     */
    public void resetMenuSelection(){
        init();
        this.selectMenu = "0";
    }
    
    /**
     * Return true if no menuitem is selected for edit dialog in dayplan-calendar
     * 
     * @return true if not menuitem is selected, false otherwise
     */
    public boolean isNotSet(){
        return this.selectMenu == null || this.selectMenu.equals("0");
    }
    
    /**
     * Return true if first menuitem is selected for edit dialog in dayplan-calendar
     * 
     * @return true if first menuitem is selected, false otherwise
     */
    public boolean isFirst(){
        return this.selectMenu != null && this.selectMenu.equals("1");
    }
    
    /**
     * Return true if second menuitem is selected for edit dialog in dayplan-calendar
     * 
     * @return true if second menuitem is selected, false otherwise
     */
    public boolean isSecond(){
        return this.selectMenu != null && this.selectMenu.equals("2");
    }
    
    /**
     * Return true if third menuitem is selected for edit dialog in dayplan-calendar
     * 
     * @return true if third menuitem is selected, false otherwise
     */
    public boolean isThird(){
        return this.selectMenu != null && this.selectMenu.equals("3");
    }

    /**
     * Sets 0,1,2 or 3 as string, which equals the menu selected in the 
     * edit dialog in dayplan-calendar
     * 
     * @param selectMenu set by dropdownmenu in menu dialog
     */
    public void setSelectMenu(String selectMenu) {
        this.selectMenu = selectMenu;
    }
}

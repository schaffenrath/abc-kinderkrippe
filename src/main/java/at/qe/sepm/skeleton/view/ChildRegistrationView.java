
package at.qe.sepm.skeleton.view;

import java.io.Serializable;
import java.util.Date;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * TODO: add doc
 */
@Component
@Scope("view")
public class ChildRegistrationView implements Serializable{
    
    private Date date;
    
    private boolean shouldAddParent;

    public boolean getShouldAddParent() {
        return shouldAddParent;
    }
    
    public void change(){
        this.shouldAddParent = !this.shouldAddParent;
    }
    
    public void setShouldAddParent(boolean shouldAddParent) {
        this.shouldAddParent = shouldAddParent;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    
    
}

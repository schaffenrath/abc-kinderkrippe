package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Message;
import at.qe.sepm.skeleton.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Repository for managing {@link Message} entities.
 * 
 */
public interface MessageRepository extends AbstractRepository<Message, Long> {

	Message findFirstById(Long id);

	@Query("SELECT m FROM Message m WHERE m.sender = :sender")
	List<Message> findBySender(@Param("sender") User sender);

	@Query("SELECT m FROM Message m WHERE m.recipient = :recipient")
	List<Message> findByRecipient(@Param("recipient") User recipient);

	@Query("SELECT m FROM Message m WHERE m.sender = :sender AND m.hideMessage IS FALSE")
	List<Message> findVisibleBySender(@Param("sender") User sender);

	@Query("SELECT m FROM Message m WHERE m.recipient = :recipient AND m.hideMessage IS FALSE")
	List<Message> findVisibleByRecipient(@Param("recipient") User recipient);

	@Query("SELECT m FROM Message m WHERE m.employeeMessage IS TRUE")
	List<Message> findAllEmployeeMessages();

	@Query("SELECT m FROM Message m WHERE m.employeeMessage IS TRUE AND m.hideMessage IS FALSE")
	List<Message> findAllVisibleEmployeeMessages();

	@Query("SELECT m FROM Message m WHERE m.publicMessage IS TRUE")
	List<Message> findAllPublicMessages();

	@Query("SELECT m FROM Message m WHERE m.publicMessage IS TRUE AND m.hideMessage IS FALSE")
	List<Message> findAllVisiblePublicMessages();

	@Query("SELECT m FROM Message m WHERE m.subject LIKE %:string%")
	List<Message> findSubjectContains(@Param("string") String string);
}

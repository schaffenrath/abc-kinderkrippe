package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Caregiver;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Repository for managing {@link Caregiver} entities.
 * 
 */
public interface CaregiverRepository extends AbstractRepository<Caregiver, String> {

	Caregiver findFirstById(String id);

	@Query("SELECT c FROM Caregiver c WHERE c.confirmed is TRUE")
	List<Caregiver> findConfirmed();

	@Query("SELECT c FROM Caregiver c WHERE c.confirmed is FALSE")
	List<Caregiver> findUnconfirmed();
}

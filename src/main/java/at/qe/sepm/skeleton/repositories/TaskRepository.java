package at.qe.sepm.skeleton.repositories;

import java.util.Calendar;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.repositories.AbstractRepository;

/**
 * Repository for managing {@link Task} entities.
 * 
 */
public interface TaskRepository extends AbstractRepository<Task, String> {

	Task findFirstById(String id);

	List<Task> findByParent(User parent);

	List<Task> findByCompleted(boolean completed);

	@Query("SELECT t FROM Task t WHERE t.infoDate <= :infoDate AND t.completed = :completed")
	List<Task> findByInfoDateAfterAndCompleted(@Param("infoDate") Calendar infoDate,
			@Param("completed") boolean completed);

	@Query("SELECT t FROM Task t WHERE t.parent = :parent AND t.deadlineFrom <= :deadline AND t.deadlineTo >= :deadline")
	List<Task> findByParentAndDeadline(@Param("parent") User parent, @Param("deadline") Calendar deadline);

	@Query("SELECT t FROM Task t WHERE t.parent = :parent AND t.infoDate <= :infoDate AND t.completed = :completed")
	List<Task> findByParentAndInfoDateAfterAndCompleted(@Param("parent") User parent,
			@Param("infoDate") Calendar infoDate, @Param("completed") boolean completed);

	@Query("SELECT t FROM Task t WHERE t.deadlineFrom <= :deadline AND t.deadlineTo >= :deadline")
	List<Task> findByDeadline(@Param("deadline") Calendar deadline);

}

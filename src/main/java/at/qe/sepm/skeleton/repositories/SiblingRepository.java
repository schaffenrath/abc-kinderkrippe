package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Sibling;

/**
 * Repository for managing {@link Sibling} entities.
 * 
 */
public interface SiblingRepository extends AbstractRepository<Sibling, String> {

	Sibling findFirstById(String id);
}

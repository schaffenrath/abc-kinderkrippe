package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.model.Lunch;

import java.util.Calendar;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for managing {@link DayPlan} entities.
 * 
 */
public interface DayPlanRepository extends AbstractRepository<DayPlan, Calendar> {

	DayPlan findFirstByDayDate(Calendar dayDate);
	
	List<DayPlan> findByDayDate(Calendar dayDate);

	@Query("SELECT d FROM DayPlan d WHERE :attendance MEMBER OF d.attendances")
	DayPlan findByAttendance(@Param("attendance") Attendance attendance);

	@Query("SELECT d FROM DayPlan d WHERE :lunch MEMBER OF d.lunches")
	DayPlan findByLunch(@Param("lunch") Lunch lunch);

}

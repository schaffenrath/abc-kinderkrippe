package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.model.Lunch;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for managing {@link Lunch} entities.
 * 
 */
public interface LunchRepository extends AbstractRepository<Lunch, String> {

	Lunch findFirstById(String id);

	List<Lunch> findByDayPlan(DayPlan dayPlan);

	@Query("SELECT l FROM Lunch l WHERE :consumer MEMBER OF l.consumers")
	List<Lunch> findByConsumer(@Param("consumer") Attendance consumer);
}

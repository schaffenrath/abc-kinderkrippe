package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.model.Sibling;
import at.qe.sepm.skeleton.model.User;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for managing {@link Child} entities.
 * 
 */
public interface ChildRepository extends AbstractRepository<Child, String> {

	Child findFirstById(String id);

	@Query("SELECT c FROM Child c LEFT JOIN FETCH c.attendances WHERE c.id = :id")
	Child findFirstByIdAndFetchAttendancesEagerly(@Param("id") String id);

	List<Child> findByEnabled(boolean enabled);

	@Query("SELECT c FROM Child c WHERE CONCAT(c.firstName, ' ', c.lastName) = :wholeName")
	List<Child> findByWholeNameConcat(@Param("wholeName") String wholeName);

	@Query("SELECT caregivers FROM Child c WHERE c.id = :id")
	List<Person> findCaregiversById(@Param("id") String id);

	@Query("SELECT c FROM Child c WHERE :caregiver MEMBER OF c.caregivers")
	List<Child> findByCaregiver(@Param("caregiver") Person caregiver);

	@Query("SELECT emergencyContact FROM Child c WHERE c.id = :id")
	List<Person> findEmergencyContactById(@Param("id") String id);

	List<Child> findByEmergencyContact(Person emergencyContact);

	@Query("SELECT parents FROM Child c WHERE c.id = :id")
	List<User> findParentsById(@Param("id") String id);

	@Query("SELECT c FROM Child c WHERE :parent MEMBER OF c.parents")
	List<Child> findByParent(@Param("parent") User parent);

	@Query("SELECT internalSiblings FROM Child c WHERE c.id = :id")
	List<Child> findInternalSiblingsById(@Param("id") String id);

	@Query("SELECT externalSiblings FROM Child c WHERE c.id = :id")
	List<Sibling> findExternalSiblingsById(@Param("id") String id);
}

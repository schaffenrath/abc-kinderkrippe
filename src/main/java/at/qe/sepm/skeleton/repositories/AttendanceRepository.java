package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.model.Person;

import java.util.List;

/**
 * Repository for managing {@link Attendance} entities.
 * 
 */
public interface AttendanceRepository extends AbstractRepository<Attendance, String> {

	Attendance findFirstById(String id);

    List<Attendance> findByDayPlan(DayPlan dayPlan);

    Attendance findFirstByChildAndDayPlan(Child child, DayPlan dayplan);
    
    List<Attendance> findByChild(Child child);
    
    List<Attendance> findByChildAndDayPlan(Child child, DayPlan dayPlan);
    
    List<Attendance> findByPickUpPerson(Person pickUpPerson);
    
}

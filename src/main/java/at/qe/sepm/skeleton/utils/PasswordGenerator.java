package at.qe.sepm.skeleton.utils;

import java.security.SecureRandom;
import java.math.BigInteger;

/**
 * Very simple password generator
 * used for new users.
 */
public class PasswordGenerator {
	private SecureRandom random = new SecureRandom();

    public String getPassword() {
        return new BigInteger(130, random).toString(32);
    }
}

package at.qe.sepm.skeleton.configs;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.services.AttendanceService;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.DayPlanService;

/**
 * Configuration and scheduled methods for dayplans and attendances.
 * 
 */
@Configuration
@EnableScheduling
public class DayPlanServiceConfig {

    @Autowired
    private DayPlanService dayPlanService;

    @Autowired
    private ChildService childService;

    @Autowired
    private AttendanceService attendanceService;

    public final static String[] weekdays = { "monday", "tuesday", "wednesday", "thursday", "friday", "saturday",
            "sunday" };

    /**
     * Initializes the new DayPlans for the week starting in 52 weeks. The
     * Scheduling is set to every Monday at 0:00:00.
     */
    @Scheduled(cron = "0 0 0 * * MON")
    public void autoinitDayPlans() {

        PropertiesConfiguration properties = loadDPconfigs();

        Calendar initDate = Calendar.getInstance();
        initDate.add(Calendar.DATE, 7 * 52);
        initDate.set(Calendar.HOUR_OF_DAY, 0);
        initDate.set(Calendar.MINUTE, 0);
        initDate.set(Calendar.SECOND, 0);
        initDate.set(Calendar.MILLISECOND, 0);
        DayPlan dayPlan;
        Calendar time = Calendar.getInstance();

        for (int i = 0; i < 7; i++) {
            if (Boolean.valueOf((String) properties.getProperty(weekdays[i] + ".open"))) {
                dayPlan = dayPlanService.createDayPlan(initDate);
                time.setTimeInMillis(Long.parseLong((String) properties.getProperty(weekdays[i] + ".earliestArrival")));
                dayPlan.setEarliestArrivalDate(adjustTimeOfCalendar(initDate, time));
                time.setTimeInMillis(Long.parseLong((String) properties.getProperty(weekdays[i] + ".latestArrival")));
                dayPlan.setLatestArrivalDate(adjustTimeOfCalendar(initDate, time));
                time.setTimeInMillis(
                        Long.parseLong((String) properties.getProperty(weekdays[i] + ".earliestDeparture")));
                dayPlan.setEarliestDepartureDate(adjustTimeOfCalendar(initDate, time));
                time.setTimeInMillis(Long.parseLong((String) properties.getProperty(weekdays[i] + ".latestDeparture")));
                dayPlan.setLatestDepartureDate(adjustTimeOfCalendar(initDate, time));
                dayPlan.setMaxAttendances(Integer.parseInt((String) properties.getProperty(weekdays[i] + ".max")));
                dayPlanService.saveDayPlan(dayPlan);
                initDate.add(Calendar.DATE, 1);
            }
        }
    }

    /**
     * Initializes the Attendances of the week starting in 5 weeks. Scheduling
     * is set to every Monday at 1:00:00.
     */
    @Scheduled(cron = "0 0 1 * * MON")
    public void autoinitAttendances() {
        Calendar initDate = Calendar.getInstance();
        initDate.add(Calendar.DATE, 7 * 5);
        initDate.set(Calendar.HOUR_OF_DAY, 0);
        initDate.set(Calendar.MINUTE, 0);
        initDate.set(Calendar.SECOND, 0);
        initDate.set(Calendar.MILLISECOND, 0);

        List<Child> children = childService.loadChildrenByEnabled(true);

        for (int i = 0; i < 7; i++) {
            List<DayPlan> dayPlans = dayPlanService.loadDayPlans(initDate);
            if (dayPlans.size() == 1) {
                DayPlan dayPlan = dayPlans.get(0);
                if (dayPlan.isOpen()) {
                    for (Child child : children) {
                        if (child.getDeregistrationDate() == null) {
                            Calendar future = Calendar.getInstance();
                            future.setTimeInMillis(initDate.getTimeInMillis());
                            future.add(Calendar.YEAR, 1);
                            child.setDeregistrationDate(future);
                        }
                        if (child.getRegistrationDate().before(initDate)
                                && child.getDeregistrationDate().after(initDate) && child.getAttendanceDays()[i]
                                && (attendanceService.loadAttendancesByChildAndDate(child, initDate).size() == 0)) {
                            attendanceService.saveAttendance(new Attendance(dayPlan, child));
                        }
                    }
                }
            }
            initDate.add(Calendar.DATE, 1);
        }
    }

    /**
     * Initializes the Attendances of the given child for all DayPlans in the
     * next 4 weeks.
     * 
     * @param child
     *            the child
     */
    public void initAttendancesForChild(Child child) {
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.DATE, 1 + 3 * 7);
        endDate.set(Calendar.HOUR_OF_DAY, 0);
        endDate.set(Calendar.MINUTE, 0);
        endDate.set(Calendar.SECOND, 0);
        endDate.set(Calendar.MILLISECOND, 0);
        while (endDate.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            endDate.add(Calendar.DATE, 1);
        }
        if (child.getAttendanceDays() != null) {
            for (DayPlan dayPlan : dayPlanService.loadDayPlansInTimeSpan(child.getRegistrationDate(), endDate)) {
                if ((child.getDeregistrationDate() != null) && dayPlan.getId().after(child.getDeregistrationDate())) {
                    break;
                } else {
                    Attendance attendance = new Attendance(dayPlan, child);
                    attendance.setAttending(true);
                    attendanceService.saveAttendance(attendance);
                }
            }
        }
    }

    /**
     * Initializes all DayPlans for a year and all Attendances of all Children
     * for 4 weeks.
     * 
     */
    public void manualInit() {
        PropertiesConfiguration properties = loadDPconfigs();
        Calendar initDate = Calendar.getInstance();
        initDate.add(Calendar.DATE, 1);
        initDate.set(Calendar.HOUR_OF_DAY, 0);
        initDate.set(Calendar.MINUTE, 0);
        initDate.set(Calendar.SECOND, 0);
        initDate.set(Calendar.MILLISECOND, 0);

        do {
            DayPlan dayPlan;
            Calendar time = Calendar.getInstance();
            int i = (initDate.get(Calendar.DAY_OF_WEEK) + 5) % 7;
            if (Boolean.valueOf((String) properties.getProperty(weekdays[i] + ".open"))) {
                if (dayPlanService.loadDayPlans(initDate).size() == 0) {
                    dayPlan = new DayPlan();
                } else {
                    dayPlan = dayPlanService.loadDayPlan(initDate);
                }
                dayPlan = dayPlanService.createDayPlan(initDate);
                time.setTimeInMillis(Long.parseLong((String) properties.getProperty(weekdays[i] + ".earliestArrival")));
                dayPlan.setEarliestArrivalDate(adjustTimeOfCalendar(initDate, time));
                time.setTimeInMillis(Long.parseLong((String) properties.getProperty(weekdays[i] + ".latestArrival")));
                dayPlan.setLatestArrivalDate(adjustTimeOfCalendar(initDate, time));
                time.setTimeInMillis(
                        Long.parseLong((String) properties.getProperty(weekdays[i] + ".earliestDeparture")));
                dayPlan.setEarliestDepartureDate(adjustTimeOfCalendar(initDate, time));
                time.setTimeInMillis(Long.parseLong((String) properties.getProperty(weekdays[i] + ".latestDeparture")));
                dayPlan.setLatestDepartureDate(adjustTimeOfCalendar(initDate, time));
                dayPlan.setMaxAttendances(Integer.parseInt((String) properties.getProperty(weekdays[i] + ".max")));
                dayPlanService.saveDayPlan(dayPlan);

                for (Child child : childService.loadChildrenByEnabled(true)) {
                    if (child.getDeregistrationDate() == null) {
                        Calendar future = Calendar.getInstance();
                        future.setTimeInMillis(initDate.getTimeInMillis());
                        future.add(Calendar.YEAR, 5);
                        child.setDeregistrationDate(future);
                    }
                    if (child.getAttendanceDays() == null) {
                        child.setAttendanceDays(new boolean[7]);
                    }
                    if (child.getRegistrationDate().before(initDate) && child.getDeregistrationDate().after(initDate)
                            && child.getAttendanceDays()[i]
                            && (attendanceService.loadAttendancesByChildAndDate(child, initDate).size() == 0)) {
                        attendanceService.saveAttendance(new Attendance(dayPlan, child));
                    }
                }
            }
            initDate.add(Calendar.DATE, 1);
        } while (initDate.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY);

        for (int i = 0; i < 51; i++) {
            for (int j = 0; j < 7; j++) {
                DayPlan dayPlan;
                Calendar time = Calendar.getInstance();
                if (Boolean.valueOf((String) properties.getProperty(weekdays[j] + ".open"))) {
                    if (dayPlanService.loadDayPlans(initDate).size() == 0) {
                        dayPlan = new DayPlan();
                    } else {
                        dayPlan = dayPlanService.loadDayPlan(initDate);
                    }
                    dayPlan = dayPlanService.createDayPlan(initDate);
                    time.setTimeInMillis(
                            Long.parseLong((String) properties.getProperty(weekdays[j] + ".earliestArrival")));
                    dayPlan.setEarliestArrivalDate(adjustTimeOfCalendar(initDate, time));
                    time.setTimeInMillis(
                            Long.parseLong((String) properties.getProperty(weekdays[j] + ".latestArrival")));
                    dayPlan.setLatestArrivalDate(adjustTimeOfCalendar(initDate, time));
                    time.setTimeInMillis(
                            Long.parseLong((String) properties.getProperty(weekdays[j] + ".earliestDeparture")));
                    dayPlan.setEarliestDepartureDate(adjustTimeOfCalendar(initDate, time));
                    time.setTimeInMillis(
                            Long.parseLong((String) properties.getProperty(weekdays[j] + ".latestDeparture")));
                    dayPlan.setLatestDepartureDate(adjustTimeOfCalendar(initDate, time));
                    dayPlan.setMaxAttendances(Integer.parseInt((String) properties.getProperty(weekdays[j] + ".max")));
                    dayPlanService.saveDayPlan(dayPlan);

                    if (i < 5) {
                        for (Child child : childService.loadChildrenByEnabled(true)) {
                            if (child.getDeregistrationDate() == null) {
                                Calendar future = Calendar.getInstance();
                                future.setTimeInMillis(initDate.getTimeInMillis());
                                future.add(Calendar.YEAR, 1);
                                child.setDeregistrationDate(future);
                            }
                            if (child.getAttendanceDays() == null) {
                                child.setAttendanceDays(new boolean[7]);
                            }
                            if (child.getRegistrationDate().before(initDate)
                                    && child.getDeregistrationDate().after(initDate) && child.getAttendanceDays()[j]
                                    && (attendanceService.loadAttendancesByChildAndDate(child, initDate).size() == 0)) {
                                attendanceService.saveAttendance(new Attendance(dayPlan, child));

                            }
                        }
                    }
                }
                initDate.add(Calendar.DATE, 1);
            }
        }
    }

    /**
     * Loads the configuration saved under the given key. Depending on the type
     * of the configuration the return value may has to be converted into a
     * readable format.
     * 
     * @param key
     *            the key to search for
     * @return the configuration as String
     */
    public String getDPconfig(String key) {
        PropertiesConfiguration properties = loadDPconfigs();
        return (String) properties.getProperty(key);
    }

    /**
     * Sets the configuration under the given key. The way of translation into
     * the needed String should be reversed in the getter.
     * 
     * @param key
     *            the key of the configuration to set
     * @param value
     *            the configuration as String
     */
    public void setDPconfig(String key, String value) {
        PropertiesConfiguration properties = loadDPconfigs();
        properties.setProperty(key, value);
        try {
            properties.write(new FileWriter("src/main/resources/dpconfigs.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Resets the properties file to the default configuration.
     * 
     * @return the properties object to prevent unnecessary loading
     */
    public PropertiesConfiguration resetDPconfig() {
        PropertiesConfiguration properties = new PropertiesConfiguration();
        Calendar time = Calendar.getInstance();
        time.set(0, 0, 0);
        time.set(Calendar.MINUTE, 0);
        time.set(Calendar.SECOND, 0);
        time.set(Calendar.MILLISECOND, 0);
        try {
            for (int i = 0; i < 7; i++) {
                if (i < 5) {
                    properties.setProperty(weekdays[i] + ".open", String.valueOf(true));
                } else {
                    properties.setProperty(weekdays[i] + ".open", String.valueOf(false));
                }
                properties.setProperty(weekdays[i] + ".max", String.valueOf(20));
                time.set(Calendar.HOUR_OF_DAY, 7);
                time.set(Calendar.MINUTE, 0);
                properties.setProperty(weekdays[i] + ".earliestArrival", String.valueOf(time.getTimeInMillis()));
                time.set(Calendar.HOUR_OF_DAY, 9);
                properties.setProperty(weekdays[i] + ".latestArrival", String.valueOf(time.getTimeInMillis()));
                time.set(Calendar.HOUR_OF_DAY, 16);
                properties.setProperty(weekdays[i] + ".earliestDeparture", String.valueOf(time.getTimeInMillis()));
                time.set(Calendar.HOUR_OF_DAY, 19);
                properties.setProperty(weekdays[i] + ".latestDeparture", String.valueOf(time.getTimeInMillis()));
            }
            properties.setProperty("lunch.deadline.day", String.valueOf(Calendar.FRIDAY));
            time.set(Calendar.HOUR_OF_DAY, 12);
            properties.setProperty("lunch.deadline.time", String.valueOf(time.getTimeInMillis()));

            properties.write(new FileWriter("src/main/resources/dpconfigs.properties"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    /**
     * Gives a new Calendar with the date of the parameter calendar and the time
     * of the parameter time.
     * 
     * @param calendar
     *            the date
     * @param time
     *            the time
     * @return the new calendar
     */
    public Calendar adjustTimeOfCalendar(Calendar calendar, Calendar time) {
        Calendar incDate = Calendar.getInstance();
        incDate.setTimeInMillis(calendar.getTimeInMillis());
        incDate.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
        incDate.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
        incDate.set(Calendar.SECOND, time.get(Calendar.SECOND));
        return incDate;
    }

    private PropertiesConfiguration loadDPconfigs() {
        PropertiesConfiguration properties = new PropertiesConfiguration();
        try {
            properties.read(new FileReader("src/main/resources/dpconfigs.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        return properties;
    }

}

package at.qe.sepm.skeleton.configs;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring configuration for servlet context.
 * 
 */
@SuppressWarnings("deprecation")
@Configuration
public class CustomServletContextInitializer implements ServletContextInitializer {

	@Override
	public void onStartup(ServletContext sc) throws ServletException {
		sc.setInitParameter("javax.faces.DEFAULT_SUFFIX", ".xhtml");
		sc.setInitParameter("javax.faces.PROJECT_STAGE", "Production");
		sc.setInitParameter("javax.faces.DATETIMECONVERTER_DEFAULT_TIMEZONE_IS_SYSTEM_TIMEZONE", "true");
		sc.setInitParameter("javax.faces.FACELETS_SKIP_COMMENTS", "true");
		sc.setInitParameter("primefaces.UPLOADER", "commons");
	}

	@Bean
	public FilterRegistrationBean FileUploadFilter() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new org.primefaces.webapp.filter.FileUploadFilter());
		registration.setName("PrimeFaces FileUpload Filter");
		return registration;
	}
}

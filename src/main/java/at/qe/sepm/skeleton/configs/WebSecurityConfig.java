package at.qe.sepm.skeleton.configs;

import at.qe.sepm.skeleton.ui.controllers.RedirectController;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Spring configuration for web security.
 * 
 */
@Configuration
@EnableWebSecurity()
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;

	@Autowired
	RedirectController successHandler;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable();

		http.headers().frameOptions().disable(); // needed for H2 console

		http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).invalidateHttpSession(true)
				.logoutSuccessUrl("/welcome.xhtml");

		http.authorizeRequests()
				// Permit access to the H2 console
				.antMatchers("/h2-console/**").permitAll()
				// Permit access for all to error pages
				.antMatchers("/error/**").permitAll()
				// Only access with admin role
				.antMatchers("/admin/**").hasAnyAuthority("ADMIN")

				.antMatchers("/parent/**").permitAll()
				// Permit access only for some roles
				.antMatchers("/employee/**").hasAnyAuthority("ADMIN", "EMPLOYEE")
				// If user doesn't have permission, forward him to login page
				.and().formLogin().loginPage("/login.xhtml").loginProcessingUrl("/login").successHandler(successHandler)
				.failureUrl("/login.xhtml?error");

		http.exceptionHandling().accessDeniedPage("/error/denied.xhtml");
		http.sessionManagement().invalidSessionUrl("/login.xhtml");

	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		// Configure roles and passwords via datasource
		auth.jdbcAuthentication().dataSource(dataSource).passwordEncoder(passwordEncoder())
				.usersByUsernameQuery("select email, password, enabled, 'U', id from person where email=?")
				.authoritiesByUsernameQuery(
						"select email, roles, user_id from user_user_role join person on user_id = id where email=?");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}

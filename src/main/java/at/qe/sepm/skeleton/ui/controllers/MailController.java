package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import org.primefaces.context.RequestContext;

@Component
@Scope("view")
public class MailController {
    
    @Autowired
    private MailService mailService;    
    
    @Autowired
    private UserListController userController;
    
    private String subject;
    
    private String text;
    
    private boolean selectedMailToAll = true;
    
    
    public String getSubject() {
        return subject;
    }
    
    public void setSubject(String subject) {
        this.subject = subject;
    }
    
    public String getText() {
        return text;
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    
    
    /**
     * A mail test method.
     *
     * @param to
     *            (Receiver of the mail)
     * @throws AddressException
     * @throws MessagingException
     */
    public void sendMailTest(String to) throws AddressException, MessagingException {
        mailService.SendMail(to, "TEST", "TEST");
    }
    
    /**
     * Send mail with the locally saved subject and message
     *
     * @param to
     *            (Receiver of the mail)
     * @throws MessagingException
     */
    public void sendMail(String to) throws MessagingException{
        mailService.SendMail(to, subject, text);
        RequestContext.getCurrentInstance().execute("PF('mailDialog').hide()");
    }
    
    /**
     * Send mail with all arguments.
     *
     * @param to
     *            (Receiver of the mail)
     * @param subject
     * @param message
     * @throws MessagingException
     */
    public void sendMail(String to, String subject, String message) throws MessagingException {
        MailAsyncronThread mail = new MailAsyncronThread(mailService, to, subject, message);
        
        
        Thread thread = new Thread(mail);
        thread.start();
        mailService.SendMail(to, subject, message);
        RequestContext.getCurrentInstance().execute("PF('mailDialog').hide()");
    }
    
    /**
     * Sends mail to every registrated parent.
     *
     * @throws MessagingException
     */
    public void sendMailToAll() throws MessagingException {
        String[] recipients = new String[userController.getAllEnabledParents().size()];
        int count = 0;
        for(User user : userController.getAllEnabledParents()){
            recipients[count] = user.getEmail();
            count++;
        }
        
        MailAsyncronThread mail = new MailAsyncronThread(mailService, recipients, subject, text);
        Thread thread = new Thread(mail);
        thread.start();
        RequestContext.getCurrentInstance().execute("PF('mailDialog').hide()");
    }
    
    /**
     * Send mail to multiple people
     *
     * @param to
     *            (Receiver of the mail)
     * @param subject
     * @param message
     * @throws MessagingException
     */
    public void sendMail(String[] to, String subject, String message) throws MessagingException {
        MailAsyncronThread mail = new MailAsyncronThread(mailService, to, subject, message);
        Thread thread = new Thread(mail);
        thread.start();
    }
    
    /**
     * Return true if on the dialog mail in parent-settings, mail to everyone
     * is selected. This indicates that a email should be send to every parent
     * registrated. Returns false if an email should be send to only one parent.
     *
     * @return true if sending email to every parent, false if seding email to one
     */
    public boolean isSelectedMailToAll() {
        return selectedMailToAll;
    }
    
    /**
     * The selectOneMenuButton sets this attribute if email should be send ether to
     * one parent or to all
     *
     * @param selectedMailToAll as boolean value
     */
    public void setSelectedMailToAll(boolean selectedMailToAll) {      
        this.selectedMailToAll = selectedMailToAll;
    }
    
    
}

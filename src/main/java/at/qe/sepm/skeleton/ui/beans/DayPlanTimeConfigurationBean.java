package at.qe.sepm.skeleton.ui.beans;

import java.util.Calendar;
import java.util.Date;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

//TODO: Docu for Class
@Component
@Scope("session")
public class DayPlanTimeConfigurationBean {

    private Calendar day;

    /**
     * Initialize the current day
     * Set the current day to the actual current day
     *
     */
    @PostConstruct
    public void init() {        
        this.day = Calendar.getInstance();
    }
    
    /**
     * Get current day as Date
     *
     * @return day as Date
     */
    public Date getDay(){
        if(this.day != null)
            return this.day.getTime();
        else
            return null;
    }
    
    /**
     * Get current day
     *
     * @return day
     */
    public Calendar getDayAsCalendar(){
        return this.day;
    }
    
    /**
     * Set current day with attribute as Date
     * 
     * @param dateDay
     *            as Date
     */
    public void setDay(Date dateDay){
        Calendar selectedDay = Calendar.getInstance();
        selectedDay.setTime(dateDay);
        this.day = selectedDay;
    }
    
    /**
     * Set current day to the day afterwards
     *
     */
    public void setNextDay(){
        this.day.add(Calendar.DATE, 1);
    }
    
    /**
     * Set current day to the day before
     *
     */
    public void setPrevDay(){
        this.day.add(Calendar.DATE, -1);
    }
    
    /**
     * Set current day to the actual current day
     *
     */
    public void setToday(){
        this.day = Calendar.getInstance();
    }

}

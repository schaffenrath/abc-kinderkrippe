package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Sibling;
import at.qe.sepm.skeleton.services.SiblingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Controller for the sibling detail view and modification.
 *
 */
@Component
@Scope("view")
public class SiblingDetailController {

    @Autowired
    private SiblingService siblingService;

    private Sibling sibling;
    
    /**
     * Get Sibling by id
     *
     * @param id
     *            as String
     * @return Sibling
     */
    public Sibling getById(String id) {
        return siblingService.loadSibling(id);
    }

    /**
     * Set current Sibling by id
     *
     * @param id
     *            as String
     */
    public void setById(String id) {
        this.sibling = getById(id);
    }

    public Sibling get() {
        return sibling;
    }

    public void set(Sibling sibling) {
        this.sibling = sibling;
    }

    public void save() {
        if (sibling != null)
            siblingService.saveSibling(sibling);
    }

    public void delete() {
        if (sibling != null)
            siblingService.deleteSibling(sibling);
    }
}

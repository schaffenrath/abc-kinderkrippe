package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Controller for the user list view.
 *
 */
@Component
@Scope("view")
public class UserListController {

    @Autowired
    private UserService userService;

    /**
     * These collections are used to store
     * users for the web interface.
     *
     */
    
    private Collection<User> allParents;
    
    private Collection<User> allEnabledParents;
    
    private Collection<User> allEmployees;
    
    private Collection<User> filteredUsers;

    private Collection<User> filteredParents;

    private Collection<User> filteredEmergency;

    /**
     * Method which returns all users.
     *
     * @return Collection of all users.
     */
    public Collection<User> getAllUsers() {
        return userService.loadAllUsers();
    }

    /**
     * Returns all admins.
     *
     * @return Collection of all admins.
     */
    public Collection<User> getAllAdmins() {
        return userService.loadAllAdmins();
    }

    /**
     * Returns a list of all users.
     *
     * @return Collection of all users.
     */
    public Collection<User> getAllEmployees() {
        if(this.allEmployees == null) 
            this.allEmployees = userService.loadAllEmployees();
        return this.allEmployees;
    }
    
    /**
     * Sets all employees in the order defined from the datatable
     * 
     * @param employees, which is set from the datatable
     */
    public void setAllEmployees(Collection<User> employees){
        this.allEmployees = employees;
    }

    /**
     * Returns a collection of all parents.
     *
     * @return Collection of all parents
     */
    public Collection<User> getAllParents() {
        if(this.allParents == null)
            this.allParents = userService.loadAllParents();
        return this.allParents;
    }
    /**
     * Sets all parents in the order defined from the datatable
     * 
     * @param parents, which is set from the datatable
     */
    public void setAllParents(Collection<User> parents){
        this.allParents = parents;
    }
    
    /**
     * Returns a collection of all parents.
     *
     * @return Collection of all parents
     */
    public Collection<User> getAllEnabledParents() {
        if(this.allEnabledParents == null)
            this.allEnabledParents = userService.loadAllEnabledParents();
        return this.allEnabledParents;
    }
    
    /**
     * Sets the local collection of parents in the order, defined by the 
     * datatable 
     * 
     * @param parents set by datatable
     */
    public void setAllEnabledParents(Collection<User> parents){
        this.allEnabledParents = parents;
    }
    
    /**
     * Reloads the parents collection if a parent got disabled
     */
    public void reloadParents(){
        this.allEnabledParents = userService.loadAllEnabledParents();
    }

    /**
     * Returns all filtered users. This is required for the filter attribute of
     * the dataTable
     * 
     * @return filteredUsers as Collection of users
     */
    public Collection<User> getFilteredUsers() {
        return filteredUsers;
    }

    /**
     * Set filtered users. This is required for the filter attribute of
     * the dataTable
     * 
     * @param filteredUsers
     *            as Collection of users
     */
    public void setFilteredUsers(Collection<User> filteredUsers) {
        this.filteredUsers = filteredUsers;
    }

    /**
     * Returns all parents users. This is required for the filter attribute of
     * the dataTable
     * 
     * @return 
     */
    public Collection<User> getFilteredParents() {
        return filteredParents;
    }

    /**
     * Set filtered parents. This is required for the filter attribute of
     * the dataTable
     * 
     * @param filteredParents 
     */
    public void setFilteredParents(Collection<User> filteredParents) {
        this.filteredParents = filteredParents;
    }

    /**
     * Method which returns the filtered emergency collection.
     *
     * @return Collection with emergency collection.
     */
    public Collection<User> getFilteredEmergency() {
        return filteredEmergency;
    }

    /**
     * Method to set the current filtered Emergency collection.
     *
     * @param filteredEmergency
     */
    public void setFilteredEmergency(Collection<User> filteredEmergency) {
        this.filteredEmergency = filteredEmergency;
    }
}

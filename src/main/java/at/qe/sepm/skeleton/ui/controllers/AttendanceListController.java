package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.AttendanceService;
import at.qe.sepm.skeleton.services.DayPlanService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Controller for the attendance list view.
 *
 */
@Component
@Scope("view")
public class AttendanceListController {
    
    @Autowired
            SessionInfoBean sessionInfoBean;
    
    @Autowired
    private AttendanceService attendanceService;
    
    @Autowired
    private DayPlanService dayplanService;
    
    @Autowired
    private UserService userService;
    
    private Collection<Attendance> filteredAttendances;
    
    private Collection<Attendance> allAttending;
    
    private Collection<Attendance> allMissing;
    
    /**
     * A method which returns the Attendances of all Children of a parent.
     *
     * @return Collection of Attendance.
     */
    public Collection<Attendance> getAttendancesOfChildrenByParent() {
        return attendanceService.loadAttendancesOfChildrenByParentFromDate(sessionInfoBean.getCurrentUser(), Calendar.getInstance());
    }
    
    /**
     *A method that returns all Attendances of the current logged-in user's children for the day after the function was called.
     *
     * @return Collection of Attendances for the next day
     */
    public List<Attendance> getAttendancesOfNextDayOfCurrentUser() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        return attendanceService.loadAttendancesOfChildrenByParentAndDate(sessionInfoBean.getCurrentUser(), c);
    }
    
    /**
     * Get the controller's filteredAttendances.
     *
     * @return filteredAttendances
     */
    public Collection<Attendance> getFilteredAttendances() {
        return this.filteredAttendances;
    }
    
    /**
     * Set the controller's filteredAttendances based on a filter.
     *
     * @param filteredAttendances
     *            value to set the controller's filteredAttendances to
     */
    public void setFilteredAttendances(Collection<Attendance> filteredAttendances) {
        this.filteredAttendances = filteredAttendances;
    }
    
    /**
     * A method that gets all attendances of the current in userController set user's children in given timespan.
     *
     * @param id
     * @return Collection of Attendancies in given timespan
     */
    public Collection<Attendance> getAttendancesByParentInTimespan(String id){
        User parent = userService.loadUser(id);
        Calendar from = Calendar.getInstance();
        from.set(Calendar.DAY_OF_MONTH, 1);
        Calendar to = Calendar.getInstance();
        if(parent == null) return new ArrayList<>();
        Collection<Attendance> allLoadedAttendances = attendanceService.loadAttendancesOfChildrenByParentInTimespan(parent, from, to);
        Collection<Attendance> attendancesWithMenu = new ArrayList<>();
        for(Attendance attendance : allLoadedAttendances){
            if(attendance.getLunch() != null)
                attendancesWithMenu.add(attendance);
        }
        return attendancesWithMenu;
    }
    
    /**
     * A method that gets the total sum of all the consumed lunches in a given timespan.
     *
     * @param id
     * @return sum in the given timespan
     */
    public String getTotalPriceInTimespan(String id){
        float sum = 0;
        try {
            for(Attendance attendance : getAttendancesByParentInTimespan(id)){
                if(attendance.getLunch() != null)
                    sum += attendance.getLunch().getPrice();
            }
        } catch(NullPointerException e) {
            return "0.00";
        }
        return String.format("%.2f\u20ac", sum);
    }
    
    /**
     * Gets a collection of all children, who are not marked as missing for
     * the current day
     * 
     * @return collection of attending children
     */
    public Collection<Attendance> getAttendancesOfTodayAttending(){
        if(this.allAttending == null){
            Calendar toady = Calendar.getInstance();
            Collection<Attendance> attending = new HashSet<>();
            if(dayplanService.loadDayPlan(toady) != null){
                for(Attendance attendance : dayplanService.loadDayPlan(toady).getAttendances()){
                    if(attendance.isAttending()){
                        attending.add(attendance);
                    }
                }
                this.allAttending = attending;
            }
        }
        return this.allAttending;
    }
    
    /**
     * Sets the collection of all attending children in the order, depending on
     * the sort of the datatable
     * 
     * @param attending set by datatable
     */
    public void setAttendancesOfTodayAttending(Collection<Attendance> attending){
        this.allAttending = attending;
    }
    
    /**
     * Gets a collection of all children, who are marked as missing for the
     * current day
     * 
     * @return collection of missing children
     */
    public Collection<Attendance> getAttendancesOfTodayMissing(){
        if(this.allMissing == null){
            Calendar toady = Calendar.getInstance();
            Collection<Attendance> missing = new HashSet<>();
            if(dayplanService.loadDayPlan(toady) != null){
                for(Attendance attendance : dayplanService.loadDayPlan(toady).getAttendances()){
                    if(!attendance.isAttending()){
                        missing.add(attendance);
                    }
                }
                this.allMissing = missing;
            }
        }
        return this.allMissing;
    }
    
    /**
     * Sets the collection of all missing children of today in the order, 
     * depending on the sort of the datatable
     * 
     * @param missing set by datatable 
     */
    public void setAttendancesOfTodayMissing(Collection<Attendance> missing){
        this.allMissing = missing;
    }
}

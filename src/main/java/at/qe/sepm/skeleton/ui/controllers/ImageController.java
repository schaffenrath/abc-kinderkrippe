package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@org.springframework.stereotype.Controller
public class ImageController {

    @Autowired
    private ImageService imageService;
    
    private String saveId;
    
    //Set image type to child as default, because child is most valuable
    private int type; 
    

    /**
     * Returns current saveId.
     *
     * @return current saveId.
     */
    public String getSaveId() {
        return saveId;
    }

    /**
     * Sets current saveId.
     *
     * @param saveId
     *            to set the current saveId to.
     */
    public void setSaveId(String saveId) {
        this.saveId = saveId;
    }
    
    /**
     * Sets current saveId and sets value to boolean array to signalize that
     * the current saved id is a child image
     * 
     * @param id 
     */
    public void setIdForChild(String id){
        this.saveId = id;
        this.type = 1;
    }

    /**
     * Sets current saveId and sets value to boolean array to signalize that
     * the current saved id is a parent image
     * 
     * @param id 
     */
    public void setIdForParent(String id){
        this.saveId = id;
        this.type = 2;
    }
    
    /**
     * Sets current saveId and sets value to boolean array to signalize that
     * the current saved id is a child image
     * 
     * @param id 
     */
    public void setIdForCaregiver(String id){
        this.saveId = id;
        this.type = 3;
    }
    /**
     * Checks if the saved image is from child
     * 
     * @return true if saved image is from child, false otherwise
     */
    public boolean isImageFromChild(){        
        return this.type == 1;
    }
    
    /**
     * Checks if the saved image is from parent
     * 
     * @return true if saved image is from parent, false otherwise
     */
    public boolean isImageFromParent(){        
        return this.type == 2;
    }
    
    /**
     * Checks if the saved image is from caregiver
     * 
     * @return true if saved image is from caregiver, false otherwise
     */
    public boolean isImageFromCaregiver(){        
        return this.type == 3;
    }

    /**
     * Returns the image of the specified child.
     *
     * @param id
     *            of the child.
     * @param response
     *            in which the image is send.
     * @throws IOException
     */
    @RequestMapping(value = "/imageChild", method = RequestMethod.GET, params = {"id"})
    @PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
    public void getImageChild(@RequestParam(value = "id") String id, HttpServletResponse response) throws IOException {
        imageService.sendImageChild(response, id.replace('/', '_').replace('\\','_').replace(' ', '-'));
    }

    /**
     * Returns the image of the specified parent.
     *
     * @param id
     *            of the parent.
     * @param response
     *            in which the image is send.
     * @throws IOException
     */
    @RequestMapping(value = "/imageParent", method = RequestMethod.GET, params = {"id"})
    @PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
    public void getImageParent(@RequestParam(value = "id") String id, HttpServletResponse response) throws IOException {
        imageService.sendImageParent(response, id.replace('/', '_').replace('\\','_').replace(' ', '-'));
    }

    /**
     * Returns the image of the specified caregiver.
     *
     * @param id
     *            of the caregiver.
     * @param response
     *            in which the image is send.
     * @throws IOException
     */
    @RequestMapping(value = "/imageCaregiver", method = RequestMethod.GET, params = {"id"})
    @PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
    public void getImageCaregiver(@RequestParam(value = "id") String id, HttpServletResponse response) throws IOException {
        imageService.sendImageCaregiver(response, id.replace('/', '_').replace('\\','_').replace(' ', '-'));
    }
}

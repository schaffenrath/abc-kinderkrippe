package at.qe.sepm.skeleton.ui.beans;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * This Bean is necessary to save information about users, especially when 
 * one user should be saved over multiple site. 
 * So if a person goes to a different view, the selected user is still available.
 */
@Component
@Scope("session")
public class TempSaveBean {
    
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public void setChildIdAndRedirect(String id){
        this.id = id;
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("./child-settings.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(TempSaveBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String requestIdOnce(){
        if(this.id != null){
            String requestedId = this.id;
            this.id = null;
            return requestedId;
        }
        return "";
    }
}

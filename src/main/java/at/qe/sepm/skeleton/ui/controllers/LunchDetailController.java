package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.model.Lunch;
import at.qe.sepm.skeleton.services.DayPlanService;
import at.qe.sepm.skeleton.services.LunchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import org.primefaces.context.RequestContext;

/**
 * Controller for the lunch detail view and modification.
 *
 */
@Component
@Scope("view")
public class LunchDetailController {

    @Autowired
    private LunchService lunchService;

    @Autowired
    private DayPlanService dayPlanService;

    private Lunch lunch;

    private String menu;

    private float price;

    /**
     * Loads Lunch by Id
     * 
     * @param id
     * @return Lunch
     */
    public Lunch getById(String id) {
        return lunchService.loadLunch(id);
    }

    /**
     * Set Lunch by parameter
     * 
     * @param id 
     */
    public void setById(String id) {
        this.lunch = getById(id);
    }

    /**
     * Returns the current Lunch
     * 
     * @return current Lunch
     */
    public Lunch getLunch() {
        return lunch;
    }

    /**
     * Set current Lunch by lunch
     * 
     * @param lunch 
     */
    public void setLunch(Lunch lunch) {
        this.lunch = lunch;
    }

    /**
     * Returns the current saved Menu as String
     * 
     * @return saved Menu as String
     */
    public String getMenu() {
        return menu;
    }

    /**
     * Set current Menu 
     * 
     * @param menu
     *            as String
     */
    public void setMenu(String menu) {
        this.menu = menu;
    }

    /**
     * Returns the current saved Price
     * 
     * @return Price as float
     */
    public float getPrice() {
        return price;
    }

    /**
     * Set Price as float
     * 
     * @param price
     *            as float
     */
    public void setPrice(float price) {
        this.price = price;
    }
    
    /**
     * Return true if Lunch is currently saved, false if no Lunch is currently saved
     * 
     * @return true or false
     */
    public boolean isSelected() {
        return lunch != null;
    }

    /**
     * Test function for debugging and demonstration using test data.sql
     *
     * @return first Lunch of October the 2nd, 2017
     */
    public String getTodaysLunch1() {
        Calendar calendar = Calendar.getInstance();
        if(dayPlanService.loadDayPlan(calendar) == null) return "Kein Menü festgelegt";    
        if(dayPlanService.loadDayPlan(calendar).getLunches().isEmpty()) return "Kein Menü festgelegt";
        return ("Menü 1: " + dayPlanService.loadDayPlan(calendar).getLunches().iterator().next().getMenu());
    }

    /**
     * Create new Lunch on given date and resets the currently saved menu and price, 
     * so that a new lunch can be created and the values don't have to be saved locally 
     * anymore.
     * If no DayPlan exists for the given date, a new one is created.
     * 
     * @param date
     *            as Date
     */
    public void addLunch(Date date) {
        Calendar calendarDate = Calendar.getInstance();
        calendarDate.setTime(date);
        DayPlan selectedDayPlan = dayPlanService.loadDayPlan(calendarDate);
        if (selectedDayPlan == null) selectedDayPlan = dayPlanService.createDayPlan(calendarDate);
        dayPlanService.addLunch(selectedDayPlan, this.menu, this.price);
        this.menu = "";
        this.price = 0;
        RequestContext.getCurrentInstance().execute("PF('add-lunch').hide()");

    }
    
    /**
     * Save the current lunch if it is not null
     *
     */
    public void save() {
        if(lunch != null) {
            lunchService.saveLunch(this.lunch);
            RequestContext.getCurrentInstance().execute("PF('edit-lunch').hide()");
        }
    }
    
    /**
     * Delete current lunch
     *
     */
    public void delete() {
    	if(lunch != null) {
    		lunchService.deleteLunch(lunch);
    	}
    }
    
    /**
     * Delete lunch by id
     *
     * @param id
     *            as String
     */
    public void deleteById(String id) {
        lunchService.deleteLunch(lunchService.loadLunch(id));
    }

    /**
     * Return the deadline for the current lunch
     *
     * @return deadline as Calendar
     */
    public Calendar getCurrentLunchDeadline() {
        return dayPlanService.loadCurrentLunchDeadline(Calendar.getInstance());
    }
}

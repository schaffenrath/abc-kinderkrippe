package at.qe.sepm.skeleton.ui.controllers;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.configs.DayPlanServiceConfig;
import org.primefaces.context.RequestContext;

/**
 * Controller for the dayplan configuration view and modification.
 *
 */
@Component
@Scope("view")
public class DayPlanConfigController {
    
    @Autowired
    private DayPlanServiceConfig dayPlanServiceConfig;
    private int selectedDay;
    
    public int getSelectedDay() {
        return selectedDay;
    }
    
    public void setSelectedDay(int selectedDay) {
        this.selectedDay = selectedDay;
    }
    
    public void initSystem(){
        dayPlanServiceConfig.manualInit();
    }
    
    
    public void resetConfigs() {
        dayPlanServiceConfig.resetDPconfig();
    }
    
    public Date getEarliestArrivalTime(int dayOfWeek) {
        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(Long.parseLong(
                dayPlanServiceConfig.getDPconfig(DayPlanServiceConfig.weekdays[dayOfWeek] + ".earliestArrival")));
        return time.getTime();
    }
    
    public void setEarliestArrivalTime(int dayOfWeek, Date timeDate) {
        Calendar time = Calendar.getInstance();
        time.setTime(timeDate);
        dayPlanServiceConfig.setDPconfig(DayPlanServiceConfig.weekdays[dayOfWeek] + ".earliestArrival",
                String.valueOf(time.getTimeInMillis()));
    }
    
    public Date getLatestArrivalTime(int dayOfWeek) {
        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(Long.parseLong(
                dayPlanServiceConfig.getDPconfig(DayPlanServiceConfig.weekdays[dayOfWeek] + ".latestArrival")));
        return time.getTime();
    }
    
    public void setLatestArrivalTime(int dayOfWeek, Date timeDate) {
        Calendar time = Calendar.getInstance();
        time.setTime(timeDate);
        dayPlanServiceConfig.setDPconfig(DayPlanServiceConfig.weekdays[dayOfWeek] + ".latestArrival",
                String.valueOf(time.getTimeInMillis()));
    }
    
    public Date getEarliestDepartureTime(int dayOfWeek) {
        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(Long.parseLong(
                dayPlanServiceConfig.getDPconfig(DayPlanServiceConfig.weekdays[dayOfWeek] + ".earliestDeparture")));
        return time.getTime();
    }
    
    public void setEarliestDepartureTime(int dayOfWeek, Date timeDate) {
        Calendar time = Calendar.getInstance();
        time.setTime(timeDate);
        dayPlanServiceConfig.setDPconfig(DayPlanServiceConfig.weekdays[dayOfWeek] + ".earliestDeparture",
                String.valueOf(time.getTimeInMillis()));
    }
    
    public Date getLatestDepartureTime(int dayOfWeek) {
        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(Long.parseLong(
                dayPlanServiceConfig.getDPconfig(DayPlanServiceConfig.weekdays[dayOfWeek] + ".latestDeparture")));
        return time.getTime();
    }
    
    public void setLatestDepartureTime(int dayOfWeek, Date timeDate) {
        Calendar time = Calendar.getInstance();
        time.setTime(timeDate);
        dayPlanServiceConfig.setDPconfig(DayPlanServiceConfig.weekdays[dayOfWeek] + ".latestDeparture",
                String.valueOf(time.getTimeInMillis()));
    }
    
    public int getMaxOccupancy(int dayOfWeek) {
        return Integer.parseInt(dayPlanServiceConfig.getDPconfig(DayPlanServiceConfig.weekdays[dayOfWeek] + ".max"));
    }
    
    public void setMaxOccupancy(int dayOfWeek, int max) {
        dayPlanServiceConfig.setDPconfig(DayPlanServiceConfig.weekdays[dayOfWeek] + ".max", String.valueOf(max));
    }
    
    public int getMaxOccupancySelected(){
        return getMaxOccupancy(selectedDay);
    }
    
    public void setMaxOccupancySelected(int max){
        setMaxOccupancy(selectedDay, max);
    }
    
    public boolean isOpen(int dayOfWeek) {
        return Boolean.parseBoolean(dayPlanServiceConfig.getDPconfig(DayPlanServiceConfig.weekdays[dayOfWeek] + ".open"));
    }
    
    public void setOpen(int dayOfWeek, boolean open) {
        dayPlanServiceConfig.setDPconfig(DayPlanServiceConfig.weekdays[dayOfWeek] + ".open", String.valueOf(open));
    }
    
    public boolean isOpenMonday(){
        return isOpen(0);
    }
    
    public void setOpenMonday(boolean open) {
        setOpen(0, open);
    }
    
    public boolean isOpenThuesday(){
        return isOpen(1);
    }
    
    public void setOpenThuesday(boolean open) {
        setOpen(1, open);
    }
    
    public boolean isOpenWendesday(){
        return isOpen(2);
    }
    
    public void setOpenWendesday(boolean open) {
        setOpen(2, open);
    }
    
    public boolean isOpenThursday(){
        return isOpen(3);
    }
    
    public void setOpenThursday(boolean open) {
        setOpen(3, open);
    }
    
    public boolean isOpenFriday(){
        return isOpen(4);
    }
    
    public void setOpenFriday(boolean open) {
        setOpen(4, open);
    }
    
    public boolean isOpenSaturday(){
        return isOpen(5);
    }
    
    public void setOpenSaturday(boolean open) {
        setOpen(5, open);
    }
    
    public boolean isOpenSunday(){
        return isOpen(6);
    }
    
    public void setOpenSunday(boolean open) {
        setOpen(6, open);
    }
    
    public int getLunchDeadlineDay() {
        return (Integer.parseInt(dayPlanServiceConfig.getDPconfig("lunch.deadline.day")) + 5) % 7;
    }
    
    public void setLunchDeadlineDay(int dayOfWeek) {
        dayPlanServiceConfig.setDPconfig("lunch.deadline.day", String.valueOf((dayOfWeek + 2) % 7));
    }
    
    public Date getLunchDeadlineTime() {
        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(Long.parseLong(
                dayPlanServiceConfig.getDPconfig("lunch.deadline.time")));
        return time.getTime();
    }
    
    public void setLunchDeadlineTime(Date timeDate) {
        Calendar time = Calendar.getInstance();
        time.setTime(timeDate);
        dayPlanServiceConfig.setDPconfig("lunch.deadline.time",
                String.valueOf(time.getTimeInMillis()));
    }
    
    public Date getEarliestArrivalTimeMonday(){
        return getEarliestArrivalTime(0);
    }
    
    public void setEarliestArrivalTimeMonday(Date time){
        setEarliestArrivalTime(0, time);
    }
    
    public Date getEarliestDepartureTimeMonday(){
        return getEarliestDepartureTime(0);
    }
    
    public void setEarliestDepartureTimeMonday(Date time){
        setEarliestDepartureTime(0, time);
    }
    
    public Date getLatestArrivalTimeMonday(){
        return getLatestArrivalTime(0);
    }
    
    public void setLatestArrivalTimeMonday(Date time){
        setLatestArrivalTime(0, time);
    }
    
    public Date getLatestDepartureTimeMonday(){
        return getLatestDepartureTime(0);
    }
    
    public void setLatestDepartureTimeMonday(Date time){
        setLatestDepartureTime(0, time);
    }
    
    public Date getEarliestArrivalTimeThuesday(){
        return getEarliestArrivalTime(1);
    }
    
    public void setEarliestArrivalTimeThuesday(Date time){
        setEarliestArrivalTime(1, time);
    }
    
    public Date getEarliestDepartureTimeThuesday(){
        return getEarliestDepartureTime(1);
    }
    
    public void setEarliestDepartureTimeThuesday(Date time){
        setEarliestDepartureTime(1, time);
    }
    
    public Date getLatestArrivalTimeThuesday(){
        return getLatestArrivalTime(1);
    }
    
    public void setLatestArrivalTimeThuesday(Date time){
        setLatestArrivalTime(1, time);
    }
    
    public Date getLatestDepartureTimeThuesday(){
        return getLatestDepartureTime(1);
    }
    
    public void setLatestDepartureTimeThuesday(Date time){
        setLatestDepartureTime(1, time);
    }
    
    public Date getEarliestArrivalTimeWednesday(){
        return getEarliestArrivalTime(2);
    }
    
    public void setEarliestArrivalTimeWednesday(Date time){
        setEarliestArrivalTime(2, time);
    }
    
    public Date getEarliestDepartureTimeWednesday(){
        return getEarliestDepartureTime(2);
    }
    
    public void setEarliestDepartureTimeWednesday(Date time){
        setEarliestDepartureTime(2, time);
    }
    
    public Date getLatestArrivalTimeWednesday(){
        return getLatestArrivalTime(2);
    }
    
    public void setLatestArrivalTimeWednesday(Date time){
        setLatestArrivalTime(2, time);
    }
    
    public Date getLatestDepartureTimeWednesday(){
        return getLatestDepartureTime(2);
    }
    
    public void setLatestDepartureTimeWednesday(Date time){
        setLatestDepartureTime(2, time);
    }
    
    public Date getEarliestArrivalTimeThursday(){
        return getEarliestArrivalTime(3);
    }
    
    public void setEarliestArrivalTimeThursday(Date time){
        setEarliestArrivalTime(3, time);
    }
    
    public Date getEarliestDepartureTimeThursday(){
        return getEarliestDepartureTime(3);
    }
    
    public void setEarliestDepartureTimeThursday(Date time){
        setEarliestDepartureTime(3, time);
    }
    
    public Date getLatestArrivalTimeThursday(){
        return getLatestArrivalTime(3);
    }
    
    public void setLatestArrivalTimeThursday(Date time){
        setLatestArrivalTime(3, time);
    }
    
    public Date getLatestDepartureTimeThursday(){
        return getLatestDepartureTime(3);
    }
    
    public void setLatestDepartureTimeThursday(Date time){
        setLatestDepartureTime(3, time);
    }
    
    public Date getEarliestArrivalTimeFriday(){
        return getEarliestArrivalTime(4);
    }
    
    public void setEarliestArrivalTimeFriday(Date time){
        setEarliestArrivalTime(4, time);
    }
    
    public Date getEarliestDepartureTimeFriday(){
        return getEarliestDepartureTime(4);
    }
    
    public void setEarliestDepartureTimeFriday(Date time){
        setEarliestDepartureTime(4, time);
    }
    
    public Date getLatestArrivalTimeFriday(){
        return getLatestArrivalTime(4);
    }
    
    public void setLatestArrivalTimeFriday(Date time){
        setLatestArrivalTime(4, time);
    }
    
    public Date getLatestDepartureTimeFriday(){
        return getLatestDepartureTime(4);
    }
    
    public void setLatestDepartureTimeFriday(Date time){
        setLatestDepartureTime(4, time);
    }
    
    public Date getEarliestArrivalTimeSaturday(){
        return getEarliestArrivalTime(5);
    }
    
    public void setEarliestArrivalTimeSaturday(Date time){
        setEarliestArrivalTime(5, time);
    }
    
    public Date getEarliestDepartureTimeSaturday(){
        return getEarliestDepartureTime(5);
    }
    
    public void setEarliestDepartureTimeSaturday(Date time){
        setEarliestDepartureTime(5, time);
    }
    
    public Date getLatestArrivalTimeSaturday(){
        return getLatestArrivalTime(5);
    }
    
    public void setLatestArrivalTimeSaturday(Date time){
        setLatestArrivalTime(5, time);
    }
    
    public Date getLatestDepartureTimeSaturday(){
        return getLatestDepartureTime(5);
    }
    
    public void setLatestDepartureTimeSaturday(Date time){
        setLatestDepartureTime(5, time);
    }
    
    public Date getEarliestArrivalTimeSunday(){
        return getEarliestArrivalTime(6);
    }
    
    public void setEarliestArrivalTimeSunday(Date time){
        setEarliestArrivalTime(6, time);
    }
    
    public Date getEarliestDepartureTimeSunday(){
        return getEarliestDepartureTime(6);
    }
    
    public void setEarliestDepartureTimeSunday(Date time){
        setEarliestDepartureTime(6, time);
    }
    
    public Date getLatestArrivalTimeSunday(){
        return getLatestArrivalTime(6);
    }
    
    public void setLatestArrivalTimeSunday(Date time){
        setLatestArrivalTime(6, time);
    }
    
    public Date getLatestDepartureTimeSunday(){
        return getLatestDepartureTime(6);
    }
    
    public void setLatestDepartureTimeSunday(Date time){
        setLatestDepartureTime(6, time);
    }
    
    public void resetMaxOccupancy(){
        RequestContext.getCurrentInstance().reset("form:max-occupancy-input");
    }
    
    public void resetTime(){
        RequestContext.getCurrentInstance().reset("form:time-config");
    }
    
    public void resetLunch(){
        RequestContext.getCurrentInstance().reset("form:deadline-lunch-input");
    }
}

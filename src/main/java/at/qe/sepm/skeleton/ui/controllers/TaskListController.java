package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.TaskService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Collection;

/**
 * Controller for the task list view.
 *
 */
@Component
@Scope("view")
public class TaskListController {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private TaskService taskService;
    
    @Autowired
    private SessionInfoBean sessionInfoBean;
    
    private Collection<Task> allTasks;
    
    private Collection<Task> allParentTasks;
    
    private Collection<Task> filteredTasks;
    
    /**
     * Get User by id
     *
     * @param id
     *            as String
     * @return User
     */
    private User getUser(String id) {
        return userService.loadUser(id);
    }
    
    /**
     * Get all tasks of a parent
     * 
     * @param id
     *            from parent as String
     * @return Collection of tasks
     */
    public Collection<Task> getTasksOfParent(String id) {
        return taskService.loadTasksByParent(getUser(id));
    }
    
    /**
     * Get all active tasks of a parent
     * 
     * @param id
     *            of parent as String
     * @return Collection of tasks
     */
    public Collection<Task> getActiveTasksOfParent(String id) {
        return taskService.loadActiveTasksByParent(getUser(id));
    }
    
    /**
     * Get all active tasks of the logged in user
     * 
     * @return Collection of tasks
     */
    public Collection<Task> getActiveTasksOfCurrentUser() {
        if(this.allParentTasks == null)
            this.allParentTasks = getActiveTasksOfParent(sessionInfoBean.getCurrentUser().getId());
        return this.allParentTasks;
    }
    
    public void setActiveTasksOfCurrentUser(Collection<Task> tasks){
        this.allParentTasks = tasks;
    }
    
    /**
     * Get the number of active tasks of current logged in user as a string
     *
     * @return number of active tasks as string
     */
    public String getNumberOfActiveTasksOfCurrentUser() {
        return String.valueOf(getActiveTasksOfCurrentUser().size());
    }
    
    /**
     * Get all tasks of a parent that have the deadline on the given date
     * 
     * @param id
     *            of task as String
     * @param deadline
     *            as Calendar
     * @return Collection of tasks
     */
    public Collection<Task> getTasksOfParentAndDeadline(String id, Calendar deadline) {
        return taskService.loadTasksByParentAndDeadline(getUser(id), deadline);
    }
    
    /**
     * Get all uncompleted tasks
     * 
     * @return Collection of tasks
     */
    public Collection<Task> getUncompletedTasks(){
        if(this.allTasks == null)
            this.allTasks = taskService.loadUncompletedTasks();
        return this.allTasks;
    }
    
    /**
     * Sets the collection of uncompleted task in the order, which the
     * datatable defines 
     * 
     * @param tasks set by the datatable
     */
    public void setUncompletedTasks(Collection<Task> tasks){
        this.allTasks = tasks;
    }
    
    /**
     * Get all active tasks
     * 
     * @return Collection of tasks
     */
    public Collection<Task> getActiveTasks(){
        return taskService.loadActiveTasks();
    }
    
    /**
     * Method specific for the filter attribute of the dataTable to find a specific
     * task in the dataTable
     * 
     * @return Collection of filtered tasks
     */
    public Collection<Task> getFilteredTasks() {
        return filteredTasks;
    }
    
    /**
     * Method specific for the filter attribute of the dataTable to find a specific task
     * 
     * @param filteredTasks
     *            as Collection of tasks
     */
    public void setFilteredTasks(Collection<Task> filteredTasks) {
        this.filteredTasks = filteredTasks;
    }
}

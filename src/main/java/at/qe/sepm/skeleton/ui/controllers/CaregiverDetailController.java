package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Caregiver;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.services.CaregiverService;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Controller for the caregiver detail view and modification.
 *
 */
@Component
@Scope("view")
public class CaregiverDetailController {

    @Autowired
    private CaregiverService caregiverService;

    @Autowired
    private ChildService childService;

    @Autowired
    private SessionInfoBean sessionInfoBean;

    private Caregiver caregiver;

    private List<String> selectedChildren;

    /**
     * A method which returns a string array of
     * all selected children.
     *
     * @return List of selectedChildren
     */
    public List<String> getSelectedChildren() {
        return this.selectedChildren;
    }

    /**
     * Sets the array of selected children.
     *
     * @param children
     *            to set the controller's selectedChildren to
     */
    public void setSelectedChildren(List<String>  children) {
        this.selectedChildren = children;
    }

    /**
     * A method which returns the Caregiver.
     *
     * @return Caregiver object.
     */
    public Caregiver getCaregiverById() {
        return this.caregiver;
    }

    /**
     * A method which sets the current Caregiver object of the
     * controller.
     *
     * @param id
     *            Id of Caregiver.
     */
    public void setCaregiverById(String id) {
        if (id != null && !id.isEmpty()) {
            caregiver = caregiverService.loadCaregiver(id);
            reload();
        }
    }

    /**
     * Gets the caregiver object.
     *
     * @return current caregiver
     */
    public Caregiver getCaregiver() {
        return caregiver;
    }

    /**
     * Sets the caregiver object
     *
     */
    public void setCaregiver(Caregiver caregiver) {
        if (caregiver != null) {
            this.caregiver = caregiver;
            reload();
            selectedChildren = new LinkedList<>();
            for (Child c : caregiver.getChildren())
                selectedChildren.add(c.getId());
        }
    }

    /**
     * Reload caregiver object from database.
     *
     */
    public void reload() {
        if (caregiver == null) return;
        caregiver = caregiverService.loadCaregiver(caregiver.getId());
    }

    /**
     * Save caregiver object in the database.
     *
     */
    public void save() {
        //remove old children and add new children
        if (selectedChildren != null && selectedChildren.size() != 0) {
            Iterator<Child> iter = caregiver.getChildren().iterator();
            while (iter.hasNext()) {
                Child c = iter.next();
                iter.remove();
                childService.removeCaregiver(c, caregiver);
            }

            for (String child : selectedChildren) {
                Child childObject = childService.loadChild(child);
                childService.addCaregiver(childObject, caregiver);
            }
            caregiverService.saveCaregiver(caregiver);
        }
    }

    /**
     * Mark the caregiver as inactive.
     *
     */
    public void delete() {
        if (caregiver == null) return;
        caregiverService.deleteCaregiver(caregiver);
        caregiver = null;
    }

    /**
     * Returns a boolean indicating if any caregiver is loaded.
     *
     * @return True if yes, False if no.
     */
    public boolean isCaregiverSelected() {
        return this.caregiver != null;
    }

    /**
     * Return all children of current logged-in user.
     *
     * @return Collection of children
     */
    public Collection<Child> getPossibleChildren() {
        return childService.loadChildrenByParent(sessionInfoBean.getCurrentUser());
    }
}

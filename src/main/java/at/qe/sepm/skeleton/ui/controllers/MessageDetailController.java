package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Caregiver;
import at.qe.sepm.skeleton.model.Message;
import at.qe.sepm.skeleton.services.CaregiverService;
import at.qe.sepm.skeleton.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Controller for the message detail view and modification.
 *
 */
@Component
@Scope("view")
public class MessageDetailController {

    @Autowired
    private MessageService messageService;

    @Autowired
    private CaregiverService caregiverService;
    
    private Message message;

    private Caregiver caregiver;
    
    /**
     * Create new current message
     *
     */
    public void createMessage(){
        this.message = new Message();
    }

    /**
     * Return current message
     *
     * @return message
     */
    public Message getMessage() {
        return message;
    }

    /**
     * Set current message.
     * If the message contains information about a caregiver, this caregiver gets 
     * saved. If the message contains no information about a caregiver, the current
     * gets reseted.
     * 
     * @param message 
     */
    public void setMessage(Message message) {
        if (message.getAdditionalId() != null) {
            if (message.isConfirmCaregiverMessage()) {
                this.caregiver = caregiverService.loadCaregiver(message.getAdditionalId());
            }
        } else {
            this.caregiver = null;
        }

        this.message = message;
    }

    /**
     * Get current Caregiver
     *
     * @return caregiver
     */
    public Caregiver getCaregiver() {
        return caregiver;
    }

    /**
     * Set current Caregiver
     * 
     * @param caregiver 
     */
    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    /**
     * Get message by id
     *
     * @param id
     *            as Long
     */
    public void loadById(Long id){
        messageService.loadMessage(id);
    }
    
    public void saveMessage(){
        this.message = messageService.saveMessage(message);
    }
    
    public void deleteMessage(){
        messageService.deleteMessage(message);
    }
    
    /**
     * Set message attribute hide to true, to signalise,
     * that the message has been read
     *
     */
    public void hideMessage() {
        message.setHideMessage(true);
        messageService.saveMessage(message);
    }

    /**
     * If message contained information about a caregiver, the caregiver
     * gets confirmed and the message attribute hide gets set to true
     *
     */
    public void confirmCaregiver() {
        if (caregiver != null) {
            caregiver.setConfirmed(true);
            caregiverService.saveCaregiver(caregiver);
        }
        message.setHideMessage(true);
        messageService.saveMessage(message);
    }
   
}

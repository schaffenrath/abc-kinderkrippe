package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Message;
import at.qe.sepm.skeleton.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Controller for the notification list view.
 *
 */
@Component
@Scope("view")
public class MessageListController {

    @Autowired
    private MessageService messageService;
    
    
    /**
     * Get all Messages
     * 
     * @return Collection of Messages
     */
    public Collection<Message> getAllMessages() {
        return messageService.loadAllMessages();
    }
    
    /**
     * Get all Messages specific for the employees
     * 
     * @return Collection of Messages for employees
     */
    public Collection<Message> getEmployeeMessages(){
        return messageService.loadAllEmployeeMessages();
    }

    /**
     * Get all Messages for employees and parents
     * 
     * @return Collection of Messages for all
     */
    public Collection<Message> getPublicMessages(){
        return messageService.loadAllPublicMessages();
    }
    
    /**
     * Get all Messages specific for employees that are not hidden
     * 
     * @return Collection of visible Messages
     */
    public Collection<Message> getVisibleEmployeeMessages(){
        return messageService.loadAllVisibleEmployeeMessages();
    }
    
    /**
     * Get all Messages for employees and parents that are not hidden
     * 
     * @return Collection of visible Messages for all
     */
    public Collection<Message> getVisiblePublicMessages(){
        return messageService.loadAllVisiblePublicMessages();
    }
}

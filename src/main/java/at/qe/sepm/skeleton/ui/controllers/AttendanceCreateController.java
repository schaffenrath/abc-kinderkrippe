package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

/**
 * Controller for the attendance creation.
 *
 */
@Component
@Scope("view")
public class AttendanceCreateController {

    @Autowired
    ChildService childService;

    @Autowired
    AttendanceService attendanceService;

    @Autowired
    UserService userService;

    @Autowired
    DayPlanService dayPlanService;

    @Autowired
    LunchService lunchService;

    @Autowired
    CaregiverService caregiverService;

    @Autowired
    SessionInfoBean sessionInfoBean;

    @Autowired
    MessageService messageService;

    private Attendance attendance;

    private boolean customTimes;

    private boolean validDay = true;


    /**
     * Creates an empty object for the controller.
     *
     */
    public void createNewAttendance() {
        this.attendance = new Attendance();
    }

    /**
     * Get current Attendance object from controller.
     *
     * @return Attendance object
     */
    public Attendance getAttendance() {
        return attendance;
    }

    /**
     * Sets the Attendance object of the controller
     *
     * @param attendance
     *            value to set controller's attendance to
     */
    public void setAttendance(Attendance attendance) {
        this.attendance = attendance;
    }

    /**
     * Get the ID of the current attendance object
     *
     * @return id of current attendance
     */
    public String getAttendanceById() {
        if (attendance == null)
            return "(null)";
        return attendance.getId();
    }

    /**
     * Sets the current Attendance object of the controller
     *
     * @param attendance
     *            ID of the Attendance object
     */
    public void setAttendanceById(String attendance) {
        this.attendance = attendanceService.loadAttendance(attendance);
    }

    /**
     * Returns the day of the Attendance.
     *
     * @return Day of the Attendance.
     */
    public Date getDay() {
        if (this.attendance == null) createNewAttendance();
        if (attendance.getDayPlan() == null) return new Date();
        return this.attendance.getDayPlan().getDayDate().getTime();
    }

    /**
     * Sets the date of the Attendance.
     *
     * @param inputDay
     *            Date.
     */
    public void setDay(Date inputDay) {
        if (inputDay != null) {
            Calendar dayPlan = Calendar.getInstance();
            Calendar currentDay = Calendar.getInstance();
            dayPlan.setTime(inputDay);
            DayPlan day = dayPlanService.loadDayPlan(dayPlan);
            if (day != null && day.isOpen() && currentDay.before(dayPlan)) {
                this.attendance.setDayPlan(day);
                if(!getParentsChildren().isEmpty()) {
                    validDay = true;
                }
                else {
                    validDay = false;
                }
            } else {
                validDay = false;
            }
        }
    }

    /**
     * A method which returns the arrival time of attendance if customTimes
     * is set or else the EarliestArrivalDate of the DayPlan.
     *
     * @return Date of arrival time.
     */
    public Date getArrivalTime() {
        if (customTimes) {
            if (this.attendance.getArrivalTime() == null) return attendance.getDayPlan().getEarliestArrivalDate().getTime();
            return this.attendance.getArrivalTime().getTime();
        } else {
            if (this.attendance.getDayPlan() == null || this.attendance.getDayPlan().getEarliestArrivalDate() == null)
                return new Date(); //just as long as data.sql does not provide data
            return this.attendance.getDayPlan().getEarliestArrivalDate().getTime();
        }
    }

    /**
     * A method which sets the arrival time
     *
     * @param arrivalTime custom arrival time
     */
    public void setArrivalTime(Date arrivalTime) {
        Calendar time = Calendar.getInstance();
        time.setTime(arrivalTime);
        time.set(Calendar.YEAR, attendance.getDayPlan().getDayDate().get(Calendar.YEAR));
        time.set(Calendar.MONTH, attendance.getDayPlan().getDayDate().get(Calendar.MONTH));
        time.set(Calendar.DATE, attendance.getDayPlan().getDayDate().get(Calendar.DATE));
        this.attendance.setArrivalTime(time);
    }

    /**
     * A method which returns the departure time of attendance if customTimes
     * is set or else the LatestDepartureDate of the DayPlan.
     *
     * @return Date of departure time
     */
    public Date getDepartureTime() {
        if (customTimes) {
            if (this.attendance.getDepartureTime() == null) return attendance.getDayPlan().getLatestDepartureDate().getTime();
            return this.attendance.getDepartureTime().getTime();
        } else {
            if (this.attendance.getDayPlan() == null || this.attendance.getDayPlan().getLatestDepartureDate() == null)
                return new Date(); //just as long as data.sql does not provide data
            return this.attendance.getDayPlan().getLatestDepartureDate().getTime();
        }
    }

    /**
     * A method which sets the departure time of attendance.
     *
     * @param departureTime
     *            custom departure time
     */
    public void setDepartureTime(Date departureTime) {
        Calendar time = Calendar.getInstance();
        time.setTime(departureTime);
        time.set(Calendar.YEAR, attendance.getDayPlan().getDayDate().get(Calendar.YEAR));
        time.set(Calendar.MONTH, attendance.getDayPlan().getDayDate().get(Calendar.MONTH));
        time.set(Calendar.DATE, attendance.getDayPlan().getDayDate().get(Calendar.DATE));
        this.attendance.setDepartureTime(time);
    }

    /**
     * A method to check the user set arrival and departure times.
     *
     * @return boolean if the set values are allowed
     */
    public boolean checkCustomTimes() {
        return getArrivalTime().before(getDepartureTime());
    }

    /**
     * A method which returns the id of
     * the current loaded child.
     *
     * @return id of current child
     */
    public String getChildId() {
        if (this.attendance.getChild() == null) {
            return new Child().getId();
        }
        return attendance.getChild().getId();
    }

    /**
     * A method which sets the id of the child.
     *
     * @param childId
     *            Id of child.
     */
    public void setChildId(String childId) {
        if (childId != null && !childId.isEmpty()) {
            attendance.setChild(childService.loadChild(childId));
        }
    }

    /**
     * A method which gets the id of the PickUp person.
     *
     * @return Id of PickUp person.
     */
    public String getPickUpPersonId() {
        if (this.attendance.getPickUpPerson() == null) return new Caregiver().toString();
        return attendance.getPickUpPerson().toString();
    }

    /**
     * A method which sets the PickUp person of the child
     * to the given ID.
     *
     * @param pickUpPersonId
     */
    public void setPickUpPersonId(String pickUpPersonId) {
        if (pickUpPersonId != null && !pickUpPersonId.isEmpty())
            if(caregiverService.loadCaregiver(pickUpPersonId) != null)  {           //pickUpPerson is a caregiver
                attendance.setPickUpPerson(caregiverService.loadCaregiver(pickUpPersonId));
            }  else {                                                              //pickUpPerson is a parent
                attendance.setPickUpPerson(userService.loadUser(pickUpPersonId));
            }
    }

    /**
     * A method which returns a collection of Children owned by
     * the current user, which are not currently registered for the chosen day.
     * (used for the menus)
     *
     * @return Collection of Children belonging to the current user.
     */
    public Collection<Child> getParentsChildren() {
        Collection<Child> children = childService.loadChildrenByParent(sessionInfoBean.getCurrentUser());
        if(attendance != null && attendance.getDayPlan() != null) {

            Collection<Attendance> alreadyRegistered = attendanceService.loadAttendancesOfChildrenByParentAndDate(sessionInfoBean.getCurrentUser(), attendance.getDayPlan().getDayDate());

            for(Attendance a : alreadyRegistered) {
                if(children.contains(a.getChild())) {
                    children.remove(a.getChild());
                }
            }

            //remove children if not registered for given day
            for(Child c : childService.loadChildrenByParent(sessionInfoBean.getCurrentUser())) {
                if(attendance.getDayPlan().getDayDate().before(c.getRegistrationDate()) || attendance.getDayPlan().getDayDate().after(c.getDeregistrationDate())) {
                    children.remove(c);
                }
            }
        }
        return children;
    }

    /**
     * A method which returns a collection of all
     * caregivers.
     *
     * @return Collection of all Caregivers.
     */
    public Collection<Caregiver> getCaregivers() {
        Collection<Caregiver> result = new HashSet<>();
        if (attendance.getChild() == null) {
            return result;
        }
        for(Caregiver c : attendance.getChild().getCaregivers()) {
            if(c.isConfirmed()) {
                result.add(c);
            }
        }
        return result;
    }

    /**
     * A method which returns a collection of all
     * parents of the child in attendance.
     *
     * @return Collection of all parents.
     */
    public Collection<User> getParents() {
        if (attendance.getChild() == null) return new HashSet<>();
        return attendance.getChild().getParents();
    }

    /**
     * A method that returns a boolean, which specifies if a
     * DayPlan is set or not.
     *
     * @return
     */
    public boolean isDaySet() {
        if (attendance == null) return false;
        return (attendance.getDayPlan() != null);
    }

    /**
     * A method that reloads the caregivers.
     *
     */
    public void refreshCaregivers() {
        attendance.getChild().getCaregivers();
    }

    /**
     * A method that saves the current attendance.
     *
     */
    public void saveAttendance() {
        Calendar creation = Calendar.getInstance();
        attendance.setCreationDate(creation);
        attendanceService.saveAttendance(attendance);
        attendance = attendanceService.loadAttendance(attendance.getId());
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        String formattedDate = format.format(attendance.getDayPlan().getDayDate().getTime());
        if(attendance.getDayPlan().getMaxAttendances() < attendance.getDayPlan().getAttendances().size()) {
            messageService.createEmployeeMessage("Überbelegung",
                    "Für den " + formattedDate + " sind zu viele Kinder angemeldet.");
        }
        if(!attendance.isAttending()) {
            messageService.createEmployeeMessage("Abwesend",
                    attendance.getChild().getFirstName() + " " + attendance.getChild().getLastName() +
                            " wurde für den " + formattedDate + " als abwesend markiert.");
        }
    }

    /**
     * A method resets the current attendance to a new Attendance Object,
     * sets validDay true and customTimes false.
     *
     */
    public void resetAttendance() {
        this.attendance = new Attendance();
        validDay = true;
        customTimes = false;
    }

    /**
     * Returns a boolean representing whether user set customTimes or not.
     *
     * @return customTimes value
     */
    public boolean isCustomTimes() {
        return customTimes;
    }

    /**
     * Returns a boolean representing whether user set customTimes or not.
     *
     * @return customTimes value
     */
    public boolean getCustomTimes() {
        return customTimes;
    }

    /**
     * Set controller's customTimes and reset custom ArrivalTime/DepartureTime to default DayPlan value.
     *
     * @param customTimes
     *            boolean value to set controller's customTimes to
     */
    public void setCustomTimes(boolean customTimes) {
        this.customTimes = customTimes;
        if(customTimes == false && attendance.getDayPlan() != null) {
            setArrivalTime(attendance.getDayPlan().getEarliestArrivalDate().getTime());
            setDepartureTime(attendance.getDayPlan().getLatestDepartureDate().getTime());
        }
    }

    /**
     * Returns a boolean representing whether user set validDay or not.
     *
     * @return validDay value
     */
    public boolean isValidDay() {
        return validDay;
    }

    /**
     * Set controller's validDay.
     *
     * @param validDay
     *            boolean value to set controller's validDay to
     */
    public void setValidDay(boolean validDay) {
        this.validDay = validDay;
    }
}
package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.model.Lunch;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.AttendanceService;
import at.qe.sepm.skeleton.services.DayPlanService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Controller for the lunch list view.
 *
 */
@Component
@Scope("view")
public class LunchListController {
    
    @Autowired
    private AttendanceService attendanceService;

    @Autowired
    private DayPlanService dayPlanService;
    
    @Autowired
    private SessionInfoBean sessionInfoBean;

    @Autowired
    private DayPlanListController dayPlanListController;

    /**
     * Get all Lunches in the time between from and to, depending on the parent
     *
     * @param parent
     * @param from
     *            as Calendar
     * @param to
     *            as Calendar
     * @return List of Lunches
     */
    public Collection<Lunch> getLunchesByParentInTimespan(User parent, Calendar from, Calendar to) {
    	List<Lunch> lunches = new ArrayList<>();
    	for(Attendance attendance : attendanceService.loadAttendancesOfChildrenByParentInTimespan(parent, from, to)) {
    		lunches.add(attendance.getLunch());
    	}
    	return lunches;
    }
    
    /**
     * Get all lunches for the current day
     *
     * @return List of Lunches
     */
    public Collection<Lunch> getTodaysLunch() {
        return dayPlanService.loadDayPlan(Calendar.getInstance()).getLunches();
    }

    /**
     * Gets every day in the current week on which at least one child of current user is registered for lunch and concatenates the
     * abbreviated (german) names of the days, seperated by /.
     *
     * @return abbreviation of days
     */
    public String getThisWeeksLunchRegistrations() {
        Collection<DayPlan> currentWeek = dayPlanListController.getCurrentWeek();
        List<DayPlan> registeredDays = new LinkedList<>();

        //get days on which at least one child is registered
        for(DayPlan day: currentWeek) {
            Set<Attendance> intersection = new HashSet<>(day.getAttendances());
            Collection<Attendance> usersChildren = attendanceService.loadAttendancesOfChildrenByParentFromDate(sessionInfoBean.getCurrentUser(), day.getDayDate());
            intersection.retainAll(usersChildren);
            if (!intersection.isEmpty()) {
                registeredDays.add(day);
            }
        }

        if(!registeredDays.isEmpty()) {
            //build output by appending first two char of each day
            StringBuilder result = new StringBuilder();
            for (DayPlan day : registeredDays) {
                result.append(day.getDayDate().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.GERMAN).subSequence(0, 2));
                result.append("/");
            }

            //discard last slash of output
            return result.toString().substring(0, result.length() - 1);
        }
        return "Keine Anmeldungen";

    }
}

package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.services.DayPlanService;
import at.qe.sepm.skeleton.model.MonthPlan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Controller for the dayplan list view.
 *
 */
@Component
@Scope("view")
public class DayPlanListController {

    @Autowired
    private DayPlanService dayPlanService;

    /**
     * Get all DayPlan objects.
     *
     * @return all DayPlans
     */
    public Collection<DayPlan> getAllDayPlans() {
        return dayPlanService.loadAllDayPlans();
    }

    /**
     * Get DayPlan objects from start to end time.
     *
     * @param start
     *            time of oldest possible wished DayPlan
     * @param end
     *            time of newest possible wished DayPlan
     * @return DayPlans in specified time span
     */
    public Collection<DayPlan> getDayPlan(Calendar start, Calendar end) {
        return dayPlanService.loadDayPlansInTimeSpan(start, end);
    }
    
    /**
     * Get a list of all monthPlans for a specified year.
     * 
     * @param year
	 *            a random date of the year asked for
	 * @return the list of monthPlans
     */

    public Collection<MonthPlan> getMonthPlans(Calendar year) {
    	return dayPlanService.loadMonthPlansByYear(year);
    }

    /**
     * Get all DayPlans for the current week.
     *
     * @return DayPlans for the current week
     */
    public Collection<DayPlan> getCurrentWeek() {
        Calendar c = Calendar.getInstance();
        int currentDay = c.get(Calendar.DAY_OF_WEEK); //Sunday = 1

        Calendar from = Calendar.getInstance();
        c.add(Calendar.DATE, -currentDay + 2); //Monday of current week
        from.setTimeInMillis(c.getTimeInMillis());

        Calendar to = Calendar.getInstance();
        c.add(Calendar.DATE, 6); //Sunday of current week
        to.setTimeInMillis(c.getTimeInMillis());

        return dayPlanService.loadDayPlansInTimeSpan(from, to);
    }

}

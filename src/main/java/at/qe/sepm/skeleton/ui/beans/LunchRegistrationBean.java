package at.qe.sepm.skeleton.ui.beans;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.model.Lunch;
import at.qe.sepm.skeleton.services.AttendanceService;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.DayPlanService;
import at.qe.sepm.skeleton.services.LunchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Lunch registration bean to retrieve and modify registrations for next week's lunch.
 * Used by parents.
 */
@Component
@Scope("view")
public class LunchRegistrationBean {

    @Autowired
    DayPlanService dayPlanService;
    
    @Autowired
    LunchService lunchService;
    
    @Autowired
    ChildService childService;
    
    @Autowired
    AttendanceService attendanceService;
    
    @Autowired
    SessionInfoBean sessionInfoBean;

    private Collection<DayPlan> nextWeek; //week open for registration
    
    private String lunch;
    
    private Collection<String> attendances;
    
    private DayPlan dayPlan;

    /**
     * get DayPlans and corresponding lunches for the next week.
     *
     */
    @PostConstruct
    public void init() {
        Calendar c = Calendar.getInstance();
        if(c.after(dayPlanService.loadCurrentLunchDeadline(c))) { //reached deadline, load the week after that
            c.add(Calendar.DATE, 7);
        }
        int currentDay = c.get(Calendar.DAY_OF_WEEK); //Sunday = 1

        Calendar from = Calendar.getInstance();
        c.add(Calendar.DATE, 9 - currentDay); //next Monday
        from.setTimeInMillis(c.getTimeInMillis());

        Calendar to = Calendar.getInstance();
        c.add(Calendar.DATE, 6); //next Sunday
        to.setTimeInMillis(c.getTimeInMillis());

        nextWeek = dayPlanService.loadDayPlansInTimeSpan(from, to);
        List<DayPlan> nextWeekClone = new LinkedList<>(nextWeek);

        //add days in which the user has at least one child registered
        for(DayPlan day: nextWeekClone) {
            Set<Attendance> intersection = new HashSet<>(day.getAttendances());
            Collection<Attendance> usersChildren = attendanceService.loadAttendancesOfChildrenByParentFromDate(sessionInfoBean.getCurrentUser(), day.getDayDate());
            intersection.retainAll(usersChildren);
            if (intersection.isEmpty()) {
                nextWeek.remove(day);
            }
        }
    }

    /**
     * Register lunch registrations for user's children on specified days.
     *
     */
    public void saveRegistration() {
        //get Child objects
        List<Attendance> attendanceObjects = new LinkedList<>();
        for (String attendanceId : attendances) {
            attendanceObjects.add(attendanceService.loadAttendance(attendanceId));
        }

        //delete if they were registered for other lunches and add to new one
        Lunch lunchObject = lunchService.loadLunch(lunch);
        for (Lunch l : dayPlan.getLunches()) {
            l.getConsumers().removeAll(attendanceObjects);
            lunchService.saveLunch(l);
        }

        //add new registrations
        lunchObject.getConsumers().addAll(attendanceObjects);
        lunchService.saveLunch(lunchObject);
        for(Attendance a : attendanceObjects) {
            a.setLunch(lunchObject);
            attendanceService.saveAttendance(a);
        }
        init();
    }

    /**
     * Delete all lunch registrations for user's children on specified day.
     *
     */
    public void deleteRegistration() {
        Collection<Child> parentsChildren = childService.loadChildrenByParent(sessionInfoBean.getCurrentUser());
        for (Lunch lunch : this.dayPlan.getLunches()) {
            for(Attendance attendance : lunch.getConsumers()) {
                if(parentsChildren.contains(attendance.getChild())) {
                    attendance.setLunch(null);
                    attendanceService.saveAttendance(attendance);
                    lunch.removeConsumer(attendance);
                    lunchService.saveLunch(lunch);
                }
            }
        }
    }

    /**
     * Finds all registered children of a parent for the in
     * {@link LunchRegistrationBean} saved day.
     *
     * @return Collection of parents' children that can be registered for lunch
     */
    public Collection<Attendance> getPossibleChildren() {
        Collection<Attendance> possibleAttendances = new LinkedList<>();
        for(Attendance a: dayPlan.getAttendances()) {
            if(childService.loadChildrenByParent(sessionInfoBean.getCurrentUser()).contains(a.getChild()))     //child is registered for today and user is parent
                possibleAttendances.add(a);
        }
        return possibleAttendances;
    }

    /**
     * Get children of current user that are registered for given lunch.
     *
     * @param lunch
     *            for which children are wanted
     * @return collection of user's children that are registered
     */
    public Collection<Child> getRegisteredChildren(Lunch lunch) {
        Collection<Child> registeredChildren = new HashSet<>();
        Collection<Child> usersChildren = childService.loadChildrenByParent(sessionInfoBean.getCurrentUser());
        for (Attendance attendance : lunch.getConsumers()) {
            if(usersChildren.contains(attendance.getChild())) {
                registeredChildren.add(attendance.getChild());
            }
        }
        return registeredChildren;
    }

    /**
     * Get weekday of specified day as String
     *
     * @param day
     *            to get weekday from
     * @return weekday in German
     */
    public String getWeekday(DayPlan day) {
        return day.getDayDate().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.GERMAN);
    }

    public Collection<DayPlan> getNextWeek() {
        return nextWeek;
    }

    public String getLunch() {
        return lunch;
    }

    public void setLunch(String lunch) {
        this.lunch = lunch;
    }

    public Collection<String> getAttendances() {
        return attendances;
    }

    public void setAttendances(Collection<String> attendances) {
        this.attendances = attendances;
    }

    public DayPlan getDayPlan() {
        return dayPlan;
    }

    public void setDayPlan(DayPlan dayPlan) {
        if(dayPlan != null) {
            this.dayPlan = dayPlan;
        }
    }

    public void setDayPlanByTime(Calendar time) {
        dayPlan = dayPlanService.loadDayPlan(time);
    }

    public boolean isDayPlanSelected() {
        return dayPlan != null;
    }
}
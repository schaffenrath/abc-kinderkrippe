package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Sibling;
import at.qe.sepm.skeleton.services.SiblingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Controller for the sibling list view.
 *
 */
@Component
@Scope("view")
public class SiblingListController {

    @Autowired
    private SiblingService siblingService;

    /**
     * Get all Siblings
     *
     * @return Collection of Siblings
     */
    public Collection<Sibling> getAll() {
        return siblingService.getAllSiblings();
    }
}

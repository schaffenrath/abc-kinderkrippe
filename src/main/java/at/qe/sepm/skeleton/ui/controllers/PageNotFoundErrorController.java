package at.qe.sepm.skeleton.ui.controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * A Controller is required if an unexpected error accurs.
 *
 * @author robert
 */
@ControllerAdvice
public class PageNotFoundErrorController {

    /**
     * Catches unexpected web exception and return string
     *
     * @param ex
     * @return 404 as String
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    public String handle(Exception ex){
        return "404";
    }
}

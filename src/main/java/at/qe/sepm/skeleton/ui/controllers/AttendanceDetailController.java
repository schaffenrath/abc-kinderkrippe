package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.services.AttendanceService;
import at.qe.sepm.skeleton.services.CaregiverService;
import at.qe.sepm.skeleton.services.MessageService;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 * Controller for the attendance detail view and modification.
 *
 */
@Component
@Scope("application")
public class AttendanceDetailController {

    @Autowired
    CaregiverService caregiverService;
    
    @Autowired
    private AttendanceService attendanceService;

    @Autowired
    MessageService messageService;

    private Attendance attendance;

    private String pickUpPerson;

    /**
     * A method that returns the attendance with a certain id.
     *
     * @param id
     *            Id of attendance.
     * @return Object of Attendance.
     */
    public Attendance getById(String id) {
        return attendanceService.loadAttendance(id);
    }

    /**
     * A method which sets the current Attendance of the
     * controller.
     *
     * @param id
     *            id of attendance to set controller's attendance to
     */
    public void setById(String id) {
        this.attendance = getById(id);
    }

    /**
     * A method which returns the current Attendance.
     *
     * @return Object of Attendance
     */
    public Attendance getAttendance() {
        return this.attendance;
    }

    /**
     * A method that sets the current Attendance of the
     * controller.
     *
     * @param attendance
     *            value to set controller's attendance to
     */
    public void setAttendance(Attendance attendance) {
        this.attendance = attendance;
    }

    /**
     * A method which saves the current Attendance.
     *
     */
    public void save() {
        if (attendance != null) {
            attendanceService.saveAttendance(attendance);
        }
    }

    /**
     * A method which changes the attending attribute and saves the current Attendance.
     *
     */
    public void changeAttending() {
        attendance.setAttending(!attendance.isAttending());
        save();

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        String formattedDate = format.format(attendance.getDayPlan().getDayDate().getTime());
        if(!attendance.isAttending()) {
            messageService.createEmployeeMessage("Abwesend",
                    attendance.getChild().getFirstName() + " " + attendance.getChild().getLastName() +
                            " wurde für den " + formattedDate + " als abwesend markiert.");
        }
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("./attendance.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ChildCreateController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * A method which deletes the current Attendance.
     */
    public void delete() {
        if (attendance != null) {
            attendanceService.deleteAttendance(attendance);
        }
    }

    /**
     * A method which returns the Id of the PickUp Person.
     *
     * @return Id of PickUp Person
     */
    public String getPickUpPersonById() {
        return pickUpPerson;
    }

    /**
     * A method which sets the PickUp person of an Attendance.
     *
     * @param id
     *            of PickUp Person
     */
    public void setPickUpPersonById(String id) {
        pickUpPerson = id;
        attendance.setPickUpPerson(caregiverService.loadCaregiver(id));
        attendanceService.saveAttendance(attendance);
    }

    /**
     * Returns boolean that indicated if an attendance object is set.
     *
     * @return whether current attendance is unequal null
     */
    public boolean isAttendanceSelected() {
        return attendance != null;
    }
}

package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Controller for the child list view.
 *
 */
@Component
@Scope("view")
public class ChildListController {

    @Autowired
    SessionInfoBean sessionInfoBean;

    @Autowired
    private ChildService childService;
    
    @Autowired
    private UserService userService;
    
    private Collection<Child> allChildren;
    
    private Collection<Child> allEnabledChildren;

    private Collection<Child> filteredChildren;

    /**
     * Returns a collection with all children.
     *
     * @return
     */
    public Collection<Child> getChildren() {

        if(this.allChildren == null)
            this.allChildren =  childService.loadAllChildren();
        return this.allChildren;
    }
    
    /**
     * Sets collection with all children in oder, depending on the selection
     * on the datatable
     * 
     * @param children set by datatable
     */
    public void setChildren(Collection<Child> children){
        this.allChildren = children;
    }
    
    /**
     * Returns a collection with all active children.
     *
     * @return
     */
    public Collection<Child> getEnabledChildren() {
        if(this.allEnabledChildren == null)
            this. allEnabledChildren = childService.loadChildrenByEnabled(true);
        return this.allEnabledChildren;
    }
    
    /**
     * Sets collection with all active children in oder, depending on the selection
     * on the datatable
     * 
     * @param children set by datatable
     */
    public void setEnabledChildren(Collection<Child> children) {
        this.allEnabledChildren = children;
    }

    /**
     * Returns a collection with the active children of the given parent.
     *
     * @param id
     *            id of parent object
     * @return
     */
    public Collection<Child> getChildrenOfParent(String id) {
        User parent = userService.loadUser(id);
        return childService.loadChildrenByParent(parent);
    }

    /**
     * Returns a collection of all children of the current user.
     *
     * @return Collection of Children.
     */
    public Collection<Child> getChildrenOfCurrentUser() {
        return getChildrenOfParent(sessionInfoBean.getCurrentUser().getId());
    }

    /**
     * Get the controller's filteredChildren.
     *
     * @return filteredChildren
     */
    public Collection<Child> getFilteredChildren() {
        return this.filteredChildren;
    }

    /**
     * Set the controller's filteredChildren based on a filter.
     *
     * @param filteredChildren
     *            value to set the controller's filteredChildren to
     */
    public void setFilteredChildren(Collection<Child> filteredChildren) {
        this.filteredChildren = filteredChildren;
    }
}

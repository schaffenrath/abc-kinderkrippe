package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.MailService;
import at.qe.sepm.skeleton.services.TaskService;
import at.qe.sepm.skeleton.services.UserService;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 * Controller for the task detail view and modification.
 *
 */
@Component
@Scope("view")
public class TaskDetailController {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private TaskService taskService;
    
    @Autowired
    private MailService mailService;
    
    private Task task;
    
    private User parent;
    
    /**
     * Set current task and current parent
     *
     * @param task
     */
    public void setTask(Task task) {
        this.task = task;
        this.parent = task.getParent();
    }
    
    /**
     * Sets  the task by id.
     *
     * @param id
     *            Id of task.
     */
    public void setTaskById(String id) {
        task = taskService.loadTask(id);
        this.parent = task.getParent();
    }
    
    /**
     * Get task by id
     *
     * @return task
     */
    public Task getTaskById(){
        return this.task;
    }
    
    public Task getTask() {
        return task;
    }
    
    /**
     * Return true if current task is set, and false if no current task is set.
     *
     * @return true or false
     */
    public boolean isTaskSelected(){
        return this.task != null;
    }
    
    /**
     * Create new current task
     *
     */
    public void createTask(){
        this.task = new Task();
    }
    
    /**
     * Return the current day as Date, so the no erlier day can be selected for
     * the deadlineFrom attribute
     *
     * @return current day as Date
     */
    public Date getCurrentDay(){
        Calendar today = Calendar.getInstance();
        return today.getTime();
    }
    
    /**
     * Set infoDate of task as Calendar
     *
     * @param dateTime
     *            as Date
     */
    public void setInfoDate(Date dateTime){
        Calendar calendarTime = Calendar.getInstance();
        calendarTime.setTime(dateTime);
        this.task.setInfoDate(calendarTime);
    }
    
    /**
     * Get infoDate of current task
     *
     * @return infoDate as Date
     */
    public Date getInfoDate(){
        if(this.task != null && this.task.getInfoDate() != null){
            return this.task.getInfoDate().getTime();
        }
        return null;
    }
    
    /**
     * Set deadlineFrom of current task
     *
     * @param dateTime
     *            as Date
     */
    public void setDeadlineFrom(Date dateTime){
        Calendar calendarTime = Calendar.getInstance();
        calendarTime.setTime(dateTime);
        this.task.setDeadlineFrom(calendarTime);
    }
    
    /**
     * Get deadlineFrom of current task
     *
     * @return deadlineFrom as Date
     */
    public Date getDeadlineFrom(){
        if(this.task != null && this.task.getDeadlineFrom()!= null){
            return this.task.getDeadlineFrom().getTime();
        }
        return null;
    }
    
    /**
     * Set deadlineTo of current task
     *
     * @param dateTime
     *            as Date
     */
    public void setDeadlineTo(Date dateTime){
        Calendar calendarTime = Calendar.getInstance();
        calendarTime.setTime(dateTime);
        this.task.setDeadlineTo(calendarTime);
    }
    
    /**
     * Get deadlineTo of current Task
     *
     * @return deadlineTo as Date
     */
    public Date getDeadlineTo(){
        if(this.task != null && this.task.getDeadlineTo()!= null){
            return this.task.getDeadlineTo().getTime();
        }
        return null;
    }
    
    /**
     * Return the earliest day the deadline can be set to
     *
     * @return erliest completionday for the task as Date
     */
    public Date getEarliestEnd(){
        if(getDeadlineFrom() != null) return getDeadlineFrom();
        return getCurrentDay();
    }
    
    public User getParent() {
        return parent;
    }
    
    public void setParent(User parent) {
        this.parent = parent;
    }
    
    /**
     * Set current parent by id
     *
     * @param id
     *            as String
     */
    public void setParentById(String id){
        this.parent = userService.loadUser(id);
    }
    
    /**
     * Get currentParent
     *
     * @return parent
     */
    public User getParentById(){
        return this.parent;
    }
    
    /**
     * This method marks the task as completed and sends a mail to the parent
     *
     */
    public void complete(){
        if(task == null) return;
        
        StringBuilder sb = new StringBuilder();
        sb.append("<h4>Hallo " + parent.getFirstName()+"</h4><br/><br/>");
        sb.append("Es wurde eine Aufgabe von Ihnen Abgeschlossen :<br/><br/>");
        sb.append("<h5>Bezeichnung:</h5>");
        sb.append(task.getSubject() + "<br/>");
        sb.append("<h5>Beschreibung:</b5>");
        sb.append(task.getDescription() + "<br/><br/>");
        
        sb.append("Vielen Dank für Ihr bemühen.<br/>");
        
        sb.append("Viele Grüße<br/>");
        sb.append("ABC Programming Team");
        
        MailAsyncronThread mail = new MailAsyncronThread(mailService, parent.getEmail(), "Abgeschlossene Aufgabe - ABC Kinderkrippe", sb.toString());
        
        
        Thread thread = new Thread(mail);
        thread.start();
        
        taskService.completeTask(task);
        
        this.task = null;
        this.parent = null;
        
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("./parent-tasks.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ChildCreateController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * This method saves the task.
     *
     * @return true if save is successful, false if not successful
     */
    public boolean save() {
        if(task == null) {
            return false;
        }
        if(parent == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Person fehlt! Bitte aus Liste auswählen", ""));
            return false;
        }
        this.task.setParent(parent);
        taskService.saveTask(task);
        return true;
    }
    
    /**
     * Saves the created task and sends a message to the parent
     */
    public void saveCreated(){
        if(save()) {
            sendCreationMail();
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("./parent-tasks.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(ChildCreateController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * Saves the changes made in the edit dialog and sends a message to the parent
     */
    public void saveChanges(){
        if(save()) {
            sendEditedMail();
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("./parent-tasks.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(ChildCreateController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }
    
    /**
     * Create email if a new task is assigned to a parent. The text is generated
     * with a StringBuilder and afterwards send with a new Thread.
     *
     */
    public void sendCreationMail(){
        
        StringBuilder sb = new StringBuilder();
        sb.append("<h4>Hallo " + parent.getFirstName()+"</h4><br/><br/>");
        sb.append("Ihnen wurde eine neue Aufgabe zugewießen:<br/><br/>");
        sb.append("<h5>Bezeichnung:</h5>");
        sb.append(task.getSubject() + "<br/>");
        sb.append("<h5>Beschreibung:</b5>");
        sb.append(task.getDescription() + "<br/><br/>");
        
        sb.append("Als Fälligkeitsdatum wurde das Datum: " + getDeadlineTo() + "festgelegt<br/>");
        sb.append("Um genauere Information zu bekommen besuchen Sie die unsere Website oder wenden Sie sich and das Kinderkrippenpersonal<br/><br/>");
        
        sb.append("Viele Grüße<br/>");
        sb.append("ABC Programming Team");
        
        MailAsyncronThread mail = new MailAsyncronThread(mailService, parent.getEmail(), "Neue Aufgabe - ABC Kinderkrippe", sb.toString());
        
        
        Thread thread = new Thread(mail);
        thread.start();
    }
    
    /**
     * Create email if a task is edited. The new information is then send to the assigned parent
     * with a new Thread.
     *
     */
    public void sendEditedMail(){
        
        StringBuilder sb = new StringBuilder();
        sb.append("<h4>Hallo " + parent.getFirstName()+"</h4><br/><br/>");
        sb.append("Eine Aufgabe von Ihnen wurde geändert. Die neuen Details sind:<br/><br/>");
        sb.append("<h5>Bezeichnung:</h5>");
        sb.append(task.getSubject() + "<br/>");
        sb.append("<h5>Beschreibung:</b5>");
        sb.append(task.getDescription() + "<br/><br/>");
        
        sb.append("Als Fälligkeitsdatum wurde das Datum: " + getDeadlineTo() + "festgelegt<br/>");
        sb.append("Um genauere Information zu bekommen besuchen Sie die unsere Website oder wenden Sie sich and das Kinderkrippenpersonal<br/><br/>");
        
        sb.append("Viele Grüße<br/>");
        sb.append("ABC Programming Team");
        
        MailAsyncronThread mail = new MailAsyncronThread(mailService, parent.getEmail(), "Neue Aufgabe - ABC Kinderkrippe", sb.toString());
        
        
        Thread thread = new Thread(mail);
        thread.start();
    }
    
    /**
     * Reset the current task and current parent
     *
     */
    public void abort(){
        this.task = null;
        this.parent = null;
    }
}

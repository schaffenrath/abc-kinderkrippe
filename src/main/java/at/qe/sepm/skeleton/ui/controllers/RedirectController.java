package at.qe.sepm.skeleton.ui.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

/**
 * Redirects the user to the correct page according to their user permissions.
 *
 */
@Controller
public class RedirectController implements AuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    /**
     * Redirects user to their homepage.
     *
     * @param request
     *            for redirection
     * @param response
     *            for redirection
     * @param authentication
     *            of the user that we want to redirect
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        HttpSession session = request.getSession();

        User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        session.setAttribute("uname", authUser.getUsername());
        session.setAttribute("authorities", authentication.getAuthorities());

        String targetUrl = getTargetUrl(authentication);
        redirectStrategy.sendRedirect(request, response, targetUrl);

    }

    /**
     * Helper method for onAuthenticationSuccess to get the site to which should be redirected.
     *
     * @param authentication
     *            of the user that we want to redirect
     * @return xhtml page to which the user will be redirected
     */
    protected String getTargetUrl(Authentication authentication) {
        Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
        if (roles.contains("ADMIN")) {
            return ("/employee/home.xhtml");
        } else if (roles.contains("EMPLOYEE")) {
            return ("/employee/home.xhtml");
        } else if (roles.contains("PARENT")) {
            return ("/parent/home.xhtml");
        } else {
            throw new IllegalStateException();
        }
    }

    public RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }

}

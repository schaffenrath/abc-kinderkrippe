package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.configs.DayPlanServiceConfig;
import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.CaregiverService;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.SiblingService;
import at.qe.sepm.skeleton.services.UserService;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller for the child creation.
 *
 */
@Component
@Scope("view")
public class ChildCreateController {

    @Autowired
    ChildService childService;

    @Autowired
    UserService userService;

    @Autowired
    SiblingService siblingService;

    @Autowired
    CaregiverService caregiverService;

    @Autowired
    DayPlanServiceConfig dayPlanServiceConfig;

    private Child child;

    private Gender gender;

    private User emergencyPerson;

    private String emergencyId;

    private String parentId = "";

    private Collection<Child> internals;

    private Child internal;

    private Collection<Sibling> externals;

    private Sibling sibling;

    private Set<Caregiver> caregivers;

    private Caregiver caregiver;

    private Collection<Caregiver> filteredCaregiver;
    
    private Date registrationDate;

    /**
     * Set current child to a new Child object.
     *
     */
    public void createNewChild() {
        this.child = new Child();
        this.child.setGender(gender);
    }

    /**
     * Return current child.
     *
     * @return current child.
     */
    public Child getChild() {
        if (this.child == null)
            createNewChild();
        return child;
    }

    /**
     * Set current child.
     *
     * @param child
     *            to set controller's child to
     */
    public void setChild(Child child) {
        this.child = child;
    }

    /**
     * Return first name of current child.
     *
     * @return first name of current child.
     */
    public String getFirstName() {
        if (this.child == null)
            createNewChild();
        return this.child.getFirstName();
    }

    /**
     * Set firstName of current child.
     *
     * @param firstName
     *            to set controller's child.firstName to
     */
    public void setFirstName(String firstName) {
        this.child.setFirstName(firstName);
    }

    /**
     * Return last name of current child.
     *
     * @return last name of current child.
     */
    public String getLastName() {
        return this.child.getLastName();
    }

    /**
     * Set lastName of current child.
     *
     * @param lastName
     *            to set controller's child.lastName to
     */
    public void setLastName(String lastName) {
        this.child.setLastName(lastName);
    }

    /**
     * Return current parentId.
     *
     * @return parentId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * Set the current parentId.
     *
     * @param parentId
     *            to set current parentId to
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
        buttonPress();
    }

    /**
     * Returns a boolean representing whether parentId is empty or not.
     *
     * @return whether parentId is empty or not
     */
    public boolean isParentSet() {
        return !this.parentId.isEmpty();
    }

    /**
     * Return current emergencyId.
     *
     * @return current emergencyId
     */
    public String getEmergencyId() {
        return emergencyId;
    }

    /**
     * Set the current emergencyId.
     *
     * @param emergencyId
     *            to set current emergencyId to
     */
    public void setEmergencyId(String emergencyId) {
        this.emergencyId = emergencyId;
    }

    /**
     * Set emergencyPerson based on emergencyId.
     *
     */
    public void loadEmergencyById() {
        if (!this.emergencyId.isEmpty()) {
            this.emergencyPerson = userService.loadUser(emergencyId);
        }
    }

    /**
     * Returns a boolean representing whether emergencyPerson is set or not.
     *
     * @return whether emergencyPerson is null or not
     */
    public boolean isEmergencyPersonSet() {
        return this.emergencyPerson != null;
    }

    /**
     * Returns the name of the current emergencyPerson
     *
     * @return name of current emergencyPerson or "Notfallkontakt" if no
     *         emergencyPerson is set.
     */
    public String getEmergencyPersonName() {
        if (this.emergencyPerson != null)
            return this.emergencyPerson.getFirstName() + " " + this.emergencyPerson.getLastName();
        return null;
    }

    /**
     * Returns the current emergencyPerson.
     *
     * @return current emergencyPerson.
     */
    public User getEmergencyPerson() {
        return emergencyPerson;
    }

    /**
     * Set the current emergencyPerson
     *
     * @param emergencyPerson
     *            to set current emergencyPerson to
     */
    public void setEmergencyPerson(User emergencyPerson) {
        if (emergencyPerson != null)
            this.emergencyPerson = emergencyPerson;
    }

    /**
     * Set the current emergencyPerson based on the id.
     *
     * @param id
     *            of the emergencyPerson.
     */
    public void setEmergencyPersonById(String id) {
        this.emergencyPerson = userService.loadUser(id);
    }

    /**
     * Set emergencyPerson to a new User object.
     *
     */
    public void createEmergencyPerson() {
        this.emergencyPerson = new User();
    }

    /**
     * Save current emergencyPerson in the database.
     *
     */
    public void saveEmergencyPerson() {
        if (this.emergencyPerson != null) {
            this.emergencyPerson = userService.saveUser(emergencyPerson);
            RequestContext.getCurrentInstance().execute("PF('create-emergency-person').hide()");
        }
    }

    /**
     * Returns the current internal siblings.
     *
     * @return current internal siblings.
     */
    public Collection<Child> getInternals() {
        return internals;
    }

    /**
     * Set the current internal siblings.
     *
     * @param internals
     *            to set current internals to.
     */
    public void setInternals(Set<Child> internals) {
        this.internals = internals;
    }

    /**
     * Returns the current internal sibling.
     *
     * @return current internal sibling.
     */
    public Child getInternal() {
        return internal;
    }

    /**
     * Set the current internal sibling.
     *
     * @param internal
     *            to set current internal to.
     */
    public void setInternal(Child internal) {
        this.internal = internal;
    }

    /**
     * Save the internal siblings in the database.
     *
     */
    public void saveInternalSibling() {
        if (internals == null)
            internals = new HashSet<>();
        internals.add(internal);
    }

    /**
     * Returns the current externals.
     *
     * @return current externals.
     */
    public Collection<Sibling> getExternals() {
        return externals;
    }

    /**
     * Set the current externals.
     *
     * @param externals
     *            to set current externals to
     */
    public void setExternals(Set<Sibling> externals) {
        this.externals = externals;
    }

    /**
     * Returns whether sibling is set.
     *
     * @return whether sibling is set.
     */
    public boolean isSiblingSet() {
        return this.sibling != null;
    }

    /**
     * Returns birthday of current sibling.
     *
     * @return birthday of current sibling.
     */
    public Date getSiblingBirthdate() {
        if (this.sibling == null || this.sibling.getBirthdate() == null)
            return null;
        return this.sibling.getBirthdate().getTime();
    }

    /**
     * Set current sibling's birthdate.
     *
     * @param date
     *            to set controller's current sibling's birthdate to.
     */
    public void setSiblingBirthdate(Date date) {
        Calendar birthday = Calendar.getInstance();
        birthday.setTime(date);
        if (this.sibling != null)
            this.sibling.setBirthdate(birthday);
    }

    /**
     * Returns current sibling.
     *
     * @return current sibling.
     */
    public Sibling getSibling() {
        return sibling;
    }

    /**
     * Set current sibling.
     *
     * @param sibling
     *            to set controller's current sibling to.
     */
    public void setSibling(Sibling sibling) {
        this.sibling = sibling;
    }
    
    /**
     * Set current sibling to new Sibling object.
     *
     */
    public void createSibling() {
        this.sibling = new Sibling();
    }

    /**
     * Save current sibling in the database.
     *
     */
    public void saveSibling() {
        if (sibling != null)
            siblingService.saveSibling(sibling);
        saveExternSibling();

    }

    /**
     * Save the current externals in the database.
     *
     */
    public void saveExternSibling() {
        if (externals == null)
            externals = new HashSet<>();
        externals.add(sibling);
        RequestContext.getCurrentInstance().execute("PF('create-sibling').hide()");
    }

    /**
     * Set current caregiver to a new Caregiver object.
     *
     */
    public void createCaregiver() {
        this.caregiver = new Caregiver();
    }

    /**
     * Returns whether caregiver is set or not.
     *
     * @return whether caregiver equals null
     */
    public boolean isCaregiverSet() {
        return this.caregiver != null;
    }

    /**
     * Returns current caregivers.
     *
     * @return current caregivers.
     */
    public Set<Caregiver> getCaregivers() {
        return caregivers;
    }

    /**
     * Set current caregivers.
     *
     * @param caregivers
     *            to set current caregivers to.
     */
    public void setCaregivers(Set<Caregiver> caregivers) {
        this.caregivers = caregivers;
    }

    /**
     * Returns current caregiver.
     *
     * @return current caregiver.
     */
    public Caregiver getCaregiver() {
        return caregiver;
    }

    /**
     * Set current caregiver.
     *
     * @param caregiver
     *            to set current caregiver to.
     */
    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    /**
     * Returns the Collection of the filtered caregivers in the datatable
     * 
     * @return filtered caregivers, depending on the input of the searfield of
     *         the datatable
     */
    public Collection<Caregiver> getFilteredCaregiver() {
        return filteredCaregiver;
    }

    /**
     * Sets the caregivers, who fulfill the requirements of the input in the
     * search field of the datatable.
     * 
     * @param filteredCaregiver
     */
    public void setFilteredCaregiver(Collection<Caregiver> filteredCaregiver) {
        this.filteredCaregiver = filteredCaregiver;
    }

    /**
     * Save current caregiver in the database.
     *
     */
    public void saveCaregiver() {
        if (this.caregiver == null)
            return;
        caregiverService.saveCaregiver(caregiver);
        addCaregiver();
        RequestContext.getCurrentInstance().execute("PF('create-caregiver').hide()");
    }

    /**
     * Add current caregiver to caregivers.
     *
     */
    public void addCaregiver() {
        if (this.caregivers == null)
            caregivers = new HashSet<>();
        if (this.caregiver != null)
            caregivers.add(caregiver);
    }

    /**
     * Sets child to a new Child object if it is empty.
     */
    public void buttonPress() {
        if (this.child == null)
            this.child = new Child();
    }

    /**
     * Returns the birthdate of the current child.
     *
     * @return birthdate of current child.
     */
    public Date getBirthdate() {
        if (this.child.getBirthdate() != null)
            return this.child.getBirthdate().getTime();
        return null;
    }

    /**
     * Set birthdate of current child.
     *
     * @param birthdate
     *            to set current child's birthdate to.
     */
    public void setBirthdate(Date birthdate) {
        Calendar birthday = Calendar.getInstance();
        birthday.setTime(birthdate);
        this.child.setBirthdate(birthday);
    }

    /**
     * Return current gender.
     *
     * @return current gender.
     */
    public String getGender() {
        if (this.gender != null)
            return this.gender.toString();
        return "";
    }

    /**
     * Set the current gender.
     *
     * @param gender
     *            to set the current gender to.
     */
    public void setGender(String gender) {
        switch (gender) {
        case "MALE":
            this.gender = Gender.MALE;
            break;
        case "FEMALE":
            this.gender = Gender.FEMALE;
            break;
        default:
            this.gender = Gender.OTHER;
            break;
        }
        this.child.setGender(this.gender);
    }

    /**
     * Returns current child's registration date.
     *
     * @return current child's registration date.
     */
    public Date getRegistrationDate() {
        if (this.registrationDate != null){
            return this.registrationDate;
        }
        return null;
    }

    /**
     * Set current child's registration date.
     *
     * @param date
     *            to set current child's registration date to.
     */
    public void setRegistrationDate(Date date) {
        Calendar dereg = Calendar.getInstance();
        dereg.setTime(date);
        this.child.setRegistrationDate(dereg);
        this.registrationDate = date;
    }

    /**
     * Returns current child's deregistration date.
     *
     * @return current child's deregistration date.
     */
    public Date getDeregistrationDate() {
        if (this.child.getDeregistrationDate() != null)
            return this.child.getDeregistrationDate().getTime();
        return null;
    }

    /**
     * Set the current child's deregistration date.
     *
     * @param date
     *            to set current child's deregistration date to.
     */
    public void setDeregistrationDate(Date date) {
        if (date != null) {
            Calendar dereg = Calendar.getInstance();
            dereg.setTime(date);
            this.child.setDeregistrationDate(dereg);
        }
    }

    /**
     * Returns current child's emergency contact's name.
     *
     * @return
     */
    public String getEmergencyContact() {
        if (this.child.getEmergencyContact() == null)
            return "Es wurde kein Notfallkontakt festgelegt";
        return this.child.getEmergencyContact().getFirstName() + this.child.getEmergencyContact().getLastName();
    }

    /**
     * Add the in parentId specified person as parent to createdChild.
     *
     * @param createdChild
     *            that gets the new parent.
     */
    private void addParent(Child createdChild) {
        User parent = userService.loadUser(parentId);
        if (!parent.isEnabled()) {
            parent.setEnabled(true);
            parent = userService.saveUser(parent);
        }
        childService.addParent(createdChild, parent);

    }

    /**
     * Add emergencyPerson as emergency person of createdChild.
     *
     * @param createdChild
     *            that gets the new emergency person.
     */
    private void addEmergencyPerson(Child createdChild) {
        if (createdChild != null)
            createdChild.setEmergencyContact(emergencyPerson);
        childService.saveChild(createdChild);
    }

    /**
     * Add internals to createdChild
     *
     */
    public void addInternalSiblings() {
        if (this.internal != null){
           if(this.internals == null) this.internals = new HashSet<>();
            this.internals.add(this.internal);
        }
    }

    /**
     * Add externals to createdChild
     *
     */
    public void addExternalSiblings() {
        if (this.sibling != null){
            if(this.externals == null) this.externals = new HashSet<>();
            this.externals.add(this.sibling);
        }
         RequestContext.getCurrentInstance().execute("PF('create-sibling').hide()");   
    }

    /**
     * Add internals to createdChild
     *
     * @param createdChild
     *            that gets the new caregiver.
     */
    private void addCaregivers(Child createdChild) {
        if (createdChild != null)
            System.out.println("not ready yet");
    }
    
    /**
     * Redirects the page to child-settings
     * @return redirection string
     */
    public String redirect(){
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("./child-settings.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ChildCreateController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "./child-settings?faces-redirect=true";
    }

    /**
     * Saves current created child and add all additional information to it.
     *
     * @throws IOException
     */
    public void saveChild() throws IOException {
        Calendar creation = Calendar.getInstance();
        child.setCreationDate(creation);
        Child createdChild = childService.saveChild(child);
        if (!parentId.isEmpty()) addParent(createdChild);
        if (emergencyPerson != null) addEmergencyPerson(createdChild);
        if (internals != null && !internals.isEmpty()) {
           childService.addInternalSiblingRelation(this.child, internals.iterator().next());
        }
        
        if (externals != null && !externals.isEmpty()) {
            for(Sibling external : externals){
            siblingService.saveSibling(external);
            childService.addExternalSiblingRelation(child, external);
            }
        }
        
        if (caregivers != null && !caregivers.isEmpty()) addCaregivers(createdChild);
        createdChild = childService.saveChild(createdChild);
        dayPlanServiceConfig.initAttendancesForChild(createdChild);
        FacesContext.getCurrentInstance().getExternalContext().redirect("./child-settings.xhtml");
    }
}
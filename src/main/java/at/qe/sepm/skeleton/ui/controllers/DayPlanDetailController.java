package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.services.DayPlanService;
import at.qe.sepm.skeleton.ui.beans.LunchConfigurationBean;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.ScheduleEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Controller for the dayplan detail view and modification.
 *
 */
@Component
@Scope("view")
public class DayPlanDetailController {

    @Autowired
    private DayPlanService dayPlanService;

    @Autowired
    private LunchConfigurationBean lunchConfigurationBean;

    private DayPlan dayPlan;

    private Calendar day;
    
    private Calendar year;

    public void setDayPlan(DayPlan dayPlan) {
        this.dayPlan = dayPlan;
    }

    public DayPlan getDayPlan() {
        return this.dayPlan;
    }

    /**
     * Set DayPlan by parameter and if no dayplan with the given time is found create a new one with the given time.
     *
     * @param time
     *            of DayPlan
     */
    public void setDayPlan(Calendar time) {        
        this.dayPlan = dayPlanService.loadDayPlan(time);
        if (dayPlan == null) this.dayPlan = dayPlanService.createDayPlan(time);        
    }
    
    public void setDayPlanFromSchedule(SelectEvent selectEvent){
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.setTime((Date) selectEvent.getObject());
        this.dayPlan = dayPlanService.loadDayPlan(selectedDate);
    }
    
    public void setDayPlanFromSchedule2(SelectEvent selectEvent){
        ScheduleEvent event = (ScheduleEvent) selectEvent.getObject();
        DayPlan obj = (DayPlan) event.getData();
        this.dayPlan = obj;
    }
    
    /**
     * Arrival time frame for the current day
     *
     * @return hh:mm-hh:mm display for today's arrival time
     */
    public String getArrivalTimeOfToday() {
        DayPlan currentDay = getCurrentDayPlan();
        if (currentDay != null && currentDay.getEarliestArrivalDate() != null) {
            if(!currentDay.isOpen()) {
                return "Heute geschlossen!";
            } else {
                StringBuilder result = new StringBuilder();
                result.append(currentDay.getEarliestArrivalDate().get(Calendar.HOUR_OF_DAY));
                result.append(":");
                result.append(currentDay.getEarliestArrivalDate().get(Calendar.MINUTE));
                if(currentDay.getEarliestArrivalDate().get(Calendar.MINUTE) == 0) {
                    result.append("0");
                }

                result.append(" - ");

                result.append(currentDay.getLatestArrivalDate().get(Calendar.HOUR_OF_DAY));
                result.append(":");
                result.append(currentDay.getLatestArrivalDate().get(Calendar.MINUTE));
                if(currentDay.getLatestArrivalDate().get(Calendar.MINUTE) == 0) {
                    result.append("0");
                }
                return result.toString();
            }
        }
        return "Heute geschlossen!";
    }

    /**
     * Departure time frame for the current day
     *
     * @return hh:mm-hh:mm display for today's departure time
     */
    public String getDepartureTimeOfToday() {
        DayPlan currentDay = getCurrentDayPlan();
        if (currentDay != null && currentDay.getEarliestDepartureDate() != null) {
            if(!currentDay.isOpen()) {
                return "";
            } else {
                StringBuilder result = new StringBuilder();
                result.append(currentDay.getEarliestDepartureDate().get(Calendar.HOUR_OF_DAY));
                result.append(":");
                result.append(currentDay.getEarliestDepartureDate().get(Calendar.MINUTE));
                if(currentDay.getEarliestDepartureDate().get(Calendar.MINUTE) == 0) {
                    result.append("0");
                }

                result.append(" - ");

                result.append(currentDay.getLatestDepartureDate().get(Calendar.HOUR_OF_DAY));
                result.append(":");
                result.append(currentDay.getLatestDepartureDate().get(Calendar.MINUTE));
                if(currentDay.getLatestDepartureDate().get(Calendar.MINUTE) == 0) {
                    result.append("0");
                }
                return result.toString();
            }
        }
        return "";
    }
    
    public Calendar getYear(){      
        if(this.year == null) this.year = Calendar.getInstance();
        return this.year;
    }

    public void getThisYear(){
        this.year = Calendar.getInstance();
    }
    
    public void getPrevYear(){
        if(this.year == null) this.year = Calendar.getInstance();
        this.year.set(Calendar.YEAR, -1);   
    }
    
    public void getNextYear(){        
        if(this.year == null) this.year = Calendar.getInstance();
        this.year.set(Calendar.YEAR, -1);       
    }
    
    public Calendar getCurrentWeeksMonday() {
        Calendar c = Calendar.getInstance();
        int currentDay = c.get(Calendar.DAY_OF_WEEK); //Sunday = 1

        c.add(Calendar.DATE, -currentDay + 2); //Monday of current week
        return c;
    }

    public Calendar getCurrentWeeksSunday() {
        Calendar c = Calendar.getInstance();
        int currentDay = c.get(Calendar.DAY_OF_WEEK); //Sunday = 1

        c.add(Calendar.DATE, (8 - currentDay) ); //Sunday of current week
        return c;
    }
    
    public void setEarliestArrivalTime(Date time){
        if(time == null) return;
        if(this.dayPlan == null) return;
            Calendar garbageCalendar = Calendar.getInstance();
            garbageCalendar.setTime(time);
            Calendar calendarTime = Calendar.getInstance();    
            calendarTime.setTime(dayPlan.getId().getTime());
            calendarTime.set(Calendar.HOUR_OF_DAY, garbageCalendar.get(Calendar.HOUR_OF_DAY));
            calendarTime.set(Calendar.MINUTE, garbageCalendar.get(Calendar.MINUTE));
            this.dayPlan.setEarliestArrivalDate(calendarTime);     
    }
    
    public Date getEarliestArrivalTime(){
        if(this.dayPlan != null && this.dayPlan.getLatestArrivalDate() != null)
            return this.dayPlan.getEarliestArrivalDate().getTime();
        return null;
    }
    
    public void setLatestArrivalTime(Date time){
        if(time == null) return;
        if(this.dayPlan == null) return;
        Calendar garbageCalendar = Calendar.getInstance();
            garbageCalendar.setTime(time);
            Calendar calendarTime = Calendar.getInstance();    
            calendarTime.setTime(dayPlan.getId().getTime());
            calendarTime.set(Calendar.HOUR_OF_DAY, garbageCalendar.get(Calendar.HOUR_OF_DAY));
            calendarTime.set(Calendar.MINUTE, garbageCalendar.get(Calendar.MINUTE));
        this.dayPlan.setLatestArrivalDate(calendarTime);
        
    }
    
    public Date getLatestArrivalTime(){
        if(this.dayPlan != null && this.dayPlan.getLatestDepartureDate() != null)
            return this.dayPlan.getLatestArrivalDate().getTime();
        return null;
    }
    
    public void setEarliestDepartureTime(Date time){
        if(time == null) return;
        if(this.dayPlan == null) return;
        Calendar garbageCalendar = Calendar.getInstance();
            garbageCalendar.setTime(time);
            Calendar calendarTime = Calendar.getInstance();    
            calendarTime.setTime(dayPlan.getId().getTime());
            calendarTime.set(Calendar.HOUR_OF_DAY, garbageCalendar.get(Calendar.HOUR_OF_DAY));
            calendarTime.set(Calendar.MINUTE, garbageCalendar.get(Calendar.MINUTE));
        this.dayPlan.setEarliestDepartureDate(calendarTime);
    }
    
    public Date getEarliestDepartureTime(){
        if(this.dayPlan != null && this.dayPlan.getEarliestDepartureDate() != null) 
            return this.dayPlan.getEarliestDepartureDate().getTime();
        return null;
    }
    
    public void setLatestDepartureTime(Date time){
        if(time == null) return;
        if(this.dayPlan == null) return;
            Calendar garbageCalendar = Calendar.getInstance();
            garbageCalendar.setTime(time);
            Calendar calendarTime = Calendar.getInstance();    
            calendarTime.setTime(dayPlan.getId().getTime());
            calendarTime.set(Calendar.HOUR_OF_DAY, garbageCalendar.get(Calendar.HOUR_OF_DAY));
            calendarTime.set(Calendar.MINUTE, garbageCalendar.get(Calendar.MINUTE));
        this.dayPlan.setLatestDepartureDate(calendarTime);
    }
    
    public Date getLatestDepartureTime(){
        if(this.dayPlan != null && this.dayPlan.getLatestDepartureDate() != null)
            return this.dayPlan.getLatestDepartureDate().getTime();
        return null;
    }

    
    public int getNumberOfChildren(){
        if(this.dayPlan != null && !this.dayPlan.getAttendances().isEmpty())
            return this.dayPlan.getAttendances().size();
        return 0;
    }

    /**
     * Number of children that are registered for selected day.
     *
     * @return number of registrations for the day or zero if no day was selected
     */
    public String getNumberOfChildrenToday() {
        if (getCurrentDayPlan() != null && !getCurrentDayPlan().getAttendances().isEmpty()){
            if (getCurrentDayPlan().getAttendances().size() == 1)
                return "1 Kind";
            else
                return getCurrentDayPlan().getAttendances().size() + " Kinder";
        }
        return "Keine";

    }

    /**
     * Get DayPlan object for specific day.
     *
     * @param time
     * @return
     */
    public DayPlan getDayPlan(Calendar time) {
        return dayPlanService.loadDayPlan(time);
    }

    /**
     * Loads DayPlan of the day when the method is executed.
     *
     * @return DayPlan of current day
     */
    public DayPlan getCurrentDayPlan() {
        Calendar calendar = Calendar.getInstance();
        return dayPlanService.loadDayPlan(calendar);
    }

    /**
     * Loads DayPlan of the next day when the method is executed.
     *
     * @return DayPlan of current day
     */
    public DayPlan getNextDayPlan() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        return dayPlanService.loadDayPlan(calendar);
    }
    
    /**
     * Loads all children that have birthday on a specified day.
     *
     * @param dTime
     * @return set of all children that have birthday
     */
    public Set<Child> getBirthdayChildren(Date dTime) {
        Calendar time = Calendar.getInstance();
        time.setTime(dTime);
        Set<Child> birthdayChildren = new HashSet<>();
        SimpleDateFormat birthdayFormat = new SimpleDateFormat("dd/MM");


        String today = birthdayFormat.format(time.getTime());
        if(dayPlanService.loadDayPlan(time) == null)
            return null;
        for (Attendance attendance : dayPlanService.loadDayPlan(time).getAttendances()) {
            if (today.equals(birthdayFormat.format(attendance.getChild().getBirthdate().getTime()))) {
                birthdayChildren.add(attendance.getChild());
            }
        }
        return birthdayChildren;
       }

    /**
     * Used for calculating how old a child becomes on its current birthday.
     *
     * @param birthdayChild
     *            child whose age is wanted
     * @return age of the child
     */
    public int getAge(Child birthdayChild) {
        Calendar calendar = Calendar.getInstance();
        Calendar birthYear = birthdayChild.getBirthdate();
        SimpleDateFormat ageFormat = new SimpleDateFormat("yyyy");
        Integer thisYear = Integer.parseInt(ageFormat.format(calendar.getTime()));
        Integer childYear = Integer.parseInt(ageFormat.format(birthYear.getTime()));
        return (thisYear - childYear);
    }

    public void setClosed(boolean status){
        if(this.dayPlan != null) this.dayPlan.setOpen(status);
    }
    
    public boolean getClosed(){
        if(this.dayPlan != null) return this.dayPlan.isOpen();
        return false;
    }
    
    public Calendar getSavedDay() {
        return day;
    }
    
    public boolean isSelected(){
        return this.dayPlan != null;
    }

    public void setSavedDay(int index) {
        Calendar gotDay = lunchConfigurationBean.getDayOfWeek(index);
        this.day = gotDay;
    }

    public String getSavedDayAsString() {
        SimpleDateFormat converter = new SimpleDateFormat("dd.MM.yyyy");
        if (day == null) return "Es wurde kein Tag ausgewählt";
        return converter.format(day.getTime());
    }

    public void createDayPlan(Calendar selectedDay) {
        this.dayPlan = dayPlanService.createDayPlan(selectedDay);
    }

    public void save() {
        if (dayPlan != null)
            dayPlanService.saveDayPlan(dayPlan);
            RequestContext.getCurrentInstance().execute("PF('eventDialog').hide()");
    }
}

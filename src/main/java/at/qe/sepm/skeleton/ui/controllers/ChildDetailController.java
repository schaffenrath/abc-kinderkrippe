package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import java.io.IOException;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 * Controller for the child detail view and modification.
 *
 */
@Component
@Scope("view")
public class ChildDetailController {

    @Autowired
    private UserService userService;

    @Autowired
    private ChildService childService;
    
    @Autowired
    private AttendanceService attendanceService;

    @Autowired
    private CaregiverService caregiverService;
 
    @Autowired
    private FileUploadController fileUpload;
    
    private Child child;

    private User parent;

    private Caregiver caregiver;

    private User emergency;

    /**
     * Sets child by id. Espacily used for loading children when site is loaded
     *
     * @param id of the child
     */
    public void setChildById(String id) {
        if(id.equals("")) return;
        child = childService.loadChild(id);
        fileUpload.setId(id);
        
        //Updates the form on the current site
        RequestContext cont = RequestContext.getCurrentInstance();
        cont.update("form");
    }

    /**
     * Gets the child object of the controller.
     *
     * @return current child
     */
    public Child getChild() {
        return child;
    }

    /**
     * Sets the child of the controller.
     *
     * @param child
     *            to set the controller's child to
     */
    public void setChild(Child child) {
        this.child = child;
    }

    /**
     * Returns current caregiver.
     *
     * @return current caregiver.
     */
    public Caregiver getCaregiver() {
        return caregiver;
    }

    /**
     * Sets current caregiver.
     *
     * @param caregiver
     *            to set current caregiver to.
     */
    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }
    
    /**
     * Sets the current caregiver via the id
     * 
     * @param id of caregiver as String
     */
    public void setCaregiverById(String id){
        this.caregiver = caregiverService.loadCaregiver(id);
    }

    /**
     * Sets current caregiver to a new Caregiver object.
     *
     */
    public void createCaregiver(){
        this.caregiver = new Caregiver();
    }

    /**
     * Adds the current caregiver to the current child.
     *
     */
    public void saveCaregiver(){
        childService.addCaregiver(child, caregiver);
        RequestContext.getCurrentInstance().execute("PF('create-caregiver').hide()");
    }

    /**
     * Removes current caregiver from the current child.
     *
     */
    public void removeCaregiver() {
        if (this.caregiver != null)
            childService.removeCaregiver(child, caregiver);
    }

    /**
     * Returns the current emergency.
     *
     * @return current emergency.
     */
    public User getEmergency() {
        return emergency;
    }

    /**
     * Sets the current emergency.
     *
     * @param emergency
     *            to set current emergency to.
     */
    public void setEmergency(User emergency) {
        this.emergency = emergency;
    }

    /**
     * Sets the current emergency by id.
     *
     * @param id
     *            of the emergency to which the current one will be set to.
     */
    public void setEmergencyById(String id){
        this.emergency = userService.loadUser(id);
    }

    /**
     * Returns the current emergency.
     *
     * @return current emeregency.
     */
    public User getEmergencyById(){
        return this.emergency;
    }

    /**
     * Sets the current emergency to a new User object.
     *
     */
    public void createEmergencyContact(){
        this.emergency = new User();
    }

    /**
     * Saves the current emergency in the current child.
     *
     */
    public void saveNewEmergencyContact(){
        this.child.setEmergencyContact(emergency);
        RequestContext.getCurrentInstance().execute("PF('create-emergency').hide()");
    }

    /**
     * Returns boolean that indicates whether the current parent is set.
     *
     * @return whether parent equals null.
     */
    public boolean isParentSelected() {
        return this.parent != null;
    }

    /**
     * Returns current parent.
     *
     * @return current parent.
     */
    public User getParent() {
        return parent;
    }

    /**
     * Sets current parent.
     *
     * @param parent
     *            to set current parent to.
     */
    public void setParent(User parent) {
        this.parent = parent;
    }
    
    /**
     * Sets the current parent via the id
     * 
     * @param id of the parent as String
     */
    public void setParentById(String id){
        this.parent = userService.loadUser(id);
    }

    /**
     * Assigns a existing parent to the current child
     * 
     */
    public void assignParent(){
        if(this.child != null && this.parent != null);
        childService.addParent(this.child, this.parent);
    }
    
//    /**
//     * Sets current parent by id.
//     *
//     * @param id
//     *            of the user that will be set as current parent
//     */
//    public void addParrent(String id) {
//        this.parent = userService.loadUser(id);
//    }

    /**
     * Removes current parent from current child.
     *
     */
    public void removeParent() {
        if (this.parent != null){
            childService.removeParent(child, parent);
            childService.saveChild(child);
        }
    }
    
    /**
     * Method iterates over every parent, which is related to the child. 
     * It is checked if the parents have any children left, after removing the
     * current one. After deleting the child from all related parents, the 
     * enabled value of the child is set to false
     */
    public void disableChild() {
        for (User parentOfChild : child.getParents()) {
            parentOfChild.removeChild(child);
            if(parentOfChild.getChildren().isEmpty()) {
                parentOfChild.setEnabled(false);
            }
            userService.saveUser(parentOfChild);
        }
        reload();
        child.setEnabled(false);
        childService.saveChild(child);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("./child-settings.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ChildCreateController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Returns the birthdate of the current child.
     *
     * @return birthdate of the current child.
     */
    public Date getBirthdate() {
        if (this.child.getBirthdate() != null) return this.child.getBirthdate().getTime();
        return null;
    }

    /**
     * Sets current child's birthdate.
     *
     * @param date
     *            to set the current child's birthdate to.
     */
    public void setBirthdate(Date date) {
        Calendar birthday = Calendar.getInstance();
        birthday.setTime(date);
        this.child.setBirthdate(birthday);
    }

    /**
     * Returns current child's gender.
     *
     * @return current child's gender.
     */
    public String getGender() {
        if (this.child.getGender() != null) return this.child.getGender().toString();
        return "";
    }

    /**
     * Sets gender of current child.
     *
     * @param gender
     *            to set current child's gender to.
     */
    public void setGender(String gender) {
        Gender selectedGender;
        switch (gender) {
            case "MALE":
                selectedGender = Gender.MALE;
                break;
            case "FEMALE":
                selectedGender = Gender.FEMALE;
                break;
            default:
                selectedGender = Gender.OTHER;
                break;
        }
        this.child.setGender(selectedGender);
    }

    /**
     * Returns current child's deregistration date.
     *
     * @return current child's deregistration date.
     */
    public Date getRegistrationDate() {
        if (this.child == null) return null;      
        if (this.child.getRegistrationDate() == null) return null;
        return this.child.getRegistrationDate().getTime();
    }

    /**
     * Sets current child's registration date.
     *
     * @param date
     *            to set the current child's registration date to.
     */
    public void setRegistrationDate(Date date) {
        Calendar dereg = Calendar.getInstance();
        dereg.setTime(date);
        this.child.setRegistrationDate(dereg);
    }

    /**
     * Returns current child's deregistration date.
     *
     * @return current child's deregistration date.
     */
    public Date getDeregistrationDate() {
        if (this.child == null) return null;
        if (this.child.getDeregistrationDate() == null) return null; 
        return this.child.getDeregistrationDate().getTime();
    }

    /**
     * Sets current child's deregistratin date.
     *
     * @param date
     *            to set the current child's deregistration date to.
     */
    public void setDeregistrationDate(Date date) {
        if (date != null) {
            Calendar dereg = Calendar.getInstance();
            dereg.setTime(date);
            this.child.setDeregistrationDate(dereg);
        }
    }

    /**
     * Returns current child's emergency contact's id.
     *
     * @return id of current child's emergency contact.
     */
    public String getEmergencyContactById() {
        if(this.child.getEmergencyContact() != null) {
            return this.child.getEmergencyContact().getId();
        }
        return "";
    }

    /**
     * Sets current child's emergency contact by id.
     *
     * @param emergencyContact
     *            id of the emergency contact.
     */
    public void setEmergencyContactById(String emergencyContact) {
        if (caregiverService.loadCaregiver(emergencyContact) != null)
            this.child.setEmergencyContact(caregiverService.loadCaregiver(emergencyContact));
        else this.child.setEmergencyContact(userService.loadUser(emergencyContact));
    }

    /**
     * Returns lunch registrations for the current week and the current child.
     *
     * @return Collection of lunches for the current week.
     */
    public Collection<Lunch> getLunchForCurrentWeek(){
        if(this.child == null){
            return null;
        }
        Set<Lunch> lunchOfWeek = new HashSet<>();
        for(Attendance attendance : attendanceService.loadAttendancesByChild(this.child)) {
            int attendWeek = attendance.getDayPlan().getId().get(Calendar.WEEK_OF_YEAR);
            int currentWeek = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
            if(attendWeek == currentWeek)
                lunchOfWeek.add(attendance.getLunch());
            if(attendWeek > currentWeek)
                break;
        }
        return lunchOfWeek;
    }
    
    /**
     * Save the child object.
     *
     */
    public void save() {
        child.setCreationDate(Calendar.getInstance());
        child.setRegistrationDate(Calendar.getInstance());
        if(child.getGender() == null)
            child.setGender(Gender.OTHER);
        if (child == null) return;
        Child newChild = childService.saveChild(child);

        if (this.parent != null) {
            newChild.addParent(parent);
            childService.saveChild(newChild);
        }
    }
    
    public void saveChanges(){
        childService.saveChild(child);
        RequestContext.getCurrentInstance().execute("PF('config-child').hide()");
    }

    /**
     * Reload child object.
     *
     */
    public void reload() {
        if (child == null) return;
        childService.saveChild(child);
    }

    /**
     * Delete object.
     *
     */
    public void delete() {
        if (child == null) return;
        childService.deleteChild(child);
    }

    /**
     * Returns boolean that indicated if a child object
     * is set.
     *
     * @return whether child is null
     */
    public boolean isChildSelected() {
        return child != null;
    }

    /**
     * Sets child to null.
     *
     */
    public void reset() {
        this.child = null;
    }

    /**
     * Sets current child to a new Child object.
     *
     */
    public void createNewChild() {
        this.child = new Child();
    }
}
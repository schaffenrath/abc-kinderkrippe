package abcselenium;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

public class SeleniumTesting {
	private static WebDriver driver;
	private static WebDriverWait driverWait;
	
	private static String id_login_button = "login-button"; // id of login form
	private static String id_email = "username"; // id of email input field
	private static String id_password = "password"; // id of password input field
	
	private static String email = "admin@gmx.at"; // login username
	private static String password ="passwd"; // login password
	
	private static String email_parent = "maria@werth.at";
	private static String password_parent = "passwd";
	
	@BeforeClass
	public static void Init() {
		driver = new ChromeDriver();
		driverWait = new WebDriverWait(driver, 10);
	}
	
	@AfterClass
	public static void Dest() {
		driver.quit();
	}
	
	/**
	 * Print URL so we know where we stopped.
	 */
	private void logInfo() {
		System.out.println("SELENIUM TESTING: url   " + driver.getCurrentUrl());
	}
	
	/**
	 * Tests if all web elements required for login can be found
	 * and if the user can login.
	 * @throws Exception
	 */
    private void login(String username, String password) throws Exception {
        driver.get("http://localhost:8080/login.xhtml");
        
        logInfo();
        
        // write email
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id_email)));
        driver.findElement(By.id(id_email)).sendKeys(username);
        
        // write password
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id_password)));
        driver.findElement(By.id(id_password)).sendKeys(password);
        
        // submit
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id_login_button)));
        driver.findElement(By.id(id_login_button)).submit();
        
        logInfo();
    }
    
    /**
     * Tests logout page.
     * @throws Exception
     */
    private void logout() throws Exception {
    	driver.get("http://localhost:8080/logout");
    }
	
    /**
     * Testing parent login.
     * @throws Exception
     */
    @Test
    public void seleniumTestingParent() throws Exception {
    	login(email_parent, password_parent);
    	parentMenuHome();
    	parentMenuAttendance();
    	parentMenuTask();
    	parentMenuLunch();
    	parentMenuSettings();
		logout();
    }
    
    private void parentMenuHome() throws Exception {
    	// click on home-attendance
    	driver.get("http://localhost:8080/parent/home.xhtml");
    	driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home-attendance")));
    	driver.findElement(ById.id("home-attendance")).sendKeys(Keys.ENTER);
    	
    	// click on home-caregiver
    	driver.get("http://localhost:8080/parent/home.xhtml");
    	driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home-caregiver")));
    	driver.findElement(ById.id("home-caregiver")).sendKeys(Keys.ENTER);
    	
    	// click on home-lunch-overview
    	driver.get("http://localhost:8080/parent/home.xhtml");
    	driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home-lunch-overview")));
    	driver.findElement(ById.id("home-lunch-overview")).sendKeys(Keys.ENTER);
    	
    	// click on home-lunch-overview
    	driver.get("http://localhost:8080/parent/home.xhtml");
    	driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home-lunch-registration")));
    	driver.findElement(ById.id("home-lunch-registration")).sendKeys(Keys.ENTER);
    	
    	// click on home-task
    	driver.get("http://localhost:8080/parent/home.xhtml");
    	driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home-task")));
    	driver.findElement(ById.id("home-lunch-overview")).sendKeys(Keys.ENTER);
    }
    
    private void parentMenuAttendance() throws Exception {
    	driver.get("http://localhost:8080/parent/attendance.xhtml");
    }
    
    private void parentMenuTask() throws Exception {
    	driver.get("http://localhost:8080/parent/task.xhtml");
    }
    
    private void parentMenuLunch() throws Exception {
    	driver.get("http://localhost:8080/parent/lunch-overview.xhtml");
    	driver.get("http://localhost:8080/parent/lunch-registration.xhtml");
    }
    
    private void parentMenuSettings() throws Exception {
    	driver.get("http://localhost:8080/parent/child-settings.xhtml");
    	driver.get("http://localhost:8080/parent/caregiver-settings.xhtml");
    	driver.get("http://localhost:8080/parent/parent-settings.xhtml");
    }
    
	/**
	 * Junit does not support proper testing with order, so this
	 * is how we do it.
	 * Tests parent site
	 * @throws Exception 
	 */
	@Test
	public void seleniumTestingEmployee() throws Exception {
		login(email, password); // test login
		employeeMenuHome();
		employeeMenuTasks();
		employeeMenuDayPlan();
		employeeMenuLunch();
		employeeMenuChildren();
		employeeMenuParents();
		employeeMenuEmployees();
		logout(); // test logout
	}
	
	/**
	 *  Goto home.
	 */
	public void employeeMenuHome() {
		// click on show-task
		driver.get("http://localhost:8080/employee/home.xhtml");
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("show-task")));
    	driver.findElement(ById.id("show-task")).sendKeys(Keys.ENTER);
		
		// click show_time
    	driver.get("http://localhost:8080/employee/home.xhtml");
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("show-time")));
    	driver.findElement(ById.id("show-time")).sendKeys(Keys.ENTER);
		
		// click show-lunch
    	driver.get("http://localhost:8080/employee/home.xhtml");
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("show-lunch")));
    	driver.findElement(ById.id("show-lunch")).sendKeys(Keys.ENTER);
	}
	
	/**
	 * Goto tasks.
	 * @throws Exception 
	 */
	private void employeeMenuTasks() throws Exception {
		driver.get("http://localhost:8080/employee/parent-tasks.xhtml");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("form:create-task")));
		driver.findElement(By.id("form:create-task")).sendKeys(Keys.ENTER);
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("form:new-task-subject")));
		driver.findElement(By.id("form:new-task-subject")).sendKeys("Test");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("form:new-task-description")));
		driver.findElement(By.id("form:new-task-description")).sendKeys("Test");
		
		// deadline from
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("form:new-task-deadlineFrom_input")));
		driver.findElement(By.id("form:new-task-deadlineFrom_input")).click();
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[5]/td[1]/a")));
		driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[5]/td[1]/a")).sendKeys(Keys.ENTER);
		
		// deadline to
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("form:new-task-deadlineTo_input")));
		driver.findElement(By.id("form:new-task-deadlineTo_input")).sendKeys(Keys.ENTER);
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[5]/td[1]/a")));
		driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[5]/td[1]/a")).sendKeys(Keys.ENTER);
		
		// info date
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:new-task-infoDate_input\"]")));
		driver.findElement(By.xpath("//*[@id=\"form:new-task-infoDate_input\"]")).sendKeys("20.04.2019");
		
		// pick parent
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:parentTable_data\"]/tr[1]")));
		driver.findElement(By.xpath("//*[@id=\"form:parentTable_data\"]/tr[1]")).click();
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:j_idt74\"]/span")));
		driver.findElement(By.xpath("//*[@id=\"form:j_idt74\"]/span")).click();
		
		driver.get("http://localhost:8080/employee/parent-tasks.xhtml");
	}
	
	/**
	 * Goto DayPlan.
	 */
	private void employeeMenuDayPlan() {
		driver.get("http://localhost:8080/employee/dayplan-select.xhtml");
		driver.get("http://localhost:8080/employee/dayplan-overview.xhtml");
		driver.get("http://localhost:8080/employee/dayplan-calendar.xhtml");
		driver.get("http://localhost:8080/employee/dayplan-year.xhtml");
	}
	
	/**
	 * Goto Lunch.
	 */
	private void employeeMenuLunch() {
		driver.get("http://localhost:8080/employee/lunch.xhtml");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:j_idt44\"]")));
		driver.findElement(By.xpath("//*[@id=\"form:j_idt44\"]")).click();
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:menuDescription\"]")));
		driver.findElement(By.xpath("//*[@id=\"form:menuDescription\"]")).sendKeys("Test");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:menuPrice_input\"]")));
		driver.findElement(By.xpath("//*[@id=\"form:menuPrice_input\"]")).sendKeys("23");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:saveButton\"]")));
		driver.findElement(By.xpath("//*[@id=\"form:saveButton\"]")).click();
	}
	
	/**
	 * Goto Children.
	 */
	private void employeeMenuChildren() {
		driver.get("http://localhost:8080/employee/child-settings.xhtml");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"registrat-child\"]/div")));
		driver.findElement(By.xpath("//*[@id=\"registrat-child\"]/div")).click();
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:parent-table_data\"]/tr[1]")));
		driver.findElement(By.xpath("//*[@id=\"form:parent-table_data\"]/tr[1]")).click();
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:j_idt44\"]")));
		driver.findElement(By.xpath("//*[@id=\"form:j_idt44\"]")).click();
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:gender-select\"]")));
		//driver.findElement(By.xpath("//*[@id=\"form:gender-select\"]")).click();

		//driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:gender-select_1\"]")));
		//driver.findElement(By.xpath("//*[@id=\"form:gender-select_1\"]")).click();
		
		//driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:child-firstname\"]")));
		//driver.findElement(By.xpath("//*[@id=\"form:child-firstname\"]")).sendKeys("Test");
		
		/*
		//*[@id="form:child-lastname"]
		
		test
		
		//*[@id="form:child-birthdate_input"]
		click
		
		//*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[4]/a
		click
		
		//*[@id="ui-datepicker-div"]
		click
		
		//*[@id="ui-datepicker-div"]
		click*/
		
		//driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:j_isdafsdt44\"]")));
	}
	
	/**
	 * Goto Parents.
	 */
	private void employeeMenuParents() {
		driver.get("http://localhost:8080/employee/parent-settings.xhtml");
		
		// create parent
		driver.get("http://localhost:8080/employee/parent-registration.xhtml");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("form:guard-firstname")));
		driver.findElement(By.id("form:guard-firstname")).sendKeys("test");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("form:guard-lastname")));
		driver.findElement(By.id("form:guard-lastname")).sendKeys("test");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("form:guard-mail")));
		driver.findElement(By.id("form:guard-mail")).sendKeys("test@test.com");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("form:guard-number-private")));
		driver.findElement(By.id("form:guard-number-private")).sendKeys("000000000000000");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:j_idt37\"]")));
		driver.findElement(By.xpath("//*[@id=\"form:j_idt37\"]")).sendKeys(Keys.ENTER);	
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form:j_idt68\"]")));
		driver.findElement(By.xpath("//*[@id=\"form:j_idt68\"]")).click();
	}
	
	/**
	 * Goto Employees.
	 */
	private void employeeMenuEmployees() {
		driver.get("http://localhost:8080/employee/employee-settings.xhtml");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"add-employee\"]")));
		driver.findElement(By.xpath("//*[@id=\"add-employee\"]")).click();
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"j_idt29:user-firstname\"]")));
		driver.findElement(By.xpath("//*[@id=\"j_idt29:user-firstname\"]")).sendKeys("test");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"j_idt29:user-lastname\"]")));
		driver.findElement(By.xpath("//*[@id=\"j_idt29:user-lastname\"]")).sendKeys("test");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"j_idt29:user-mail\"]")));
		driver.findElement(By.xpath("//*[@id=\"j_idt29:user-mail\"]")).sendKeys("test@gmail.com");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"j_idt29:employee-number-private\"]")));
		driver.findElement(By.xpath("//*[@id=\"j_idt29:employee-number-private\"]")).sendKeys("000000000000000");
		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"j_idt29:j_idt36\"]")));
		driver.findElement(By.xpath("//*[@id=\"j_idt29:j_idt36\"]")).click();
	}
}
